﻿using System.Xml.Serialization;
using System.IO;
using System.Text;

namespace TravelApi
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.insurefor.com/ws2/0")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.insurefor.com/ws2/0", IsNullable = false)]
    public class GetQuote
    {

        private cLoginData loginDataField;

        private cQuoteApplication[] quoteApplicationsField;

        public cLoginData LoginData
        {
            get
            {
                return this.loginDataField;
            }
            set
            {
                this.loginDataField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("QuoteApplication", IsNullable = false)]
        public cQuoteApplication[] QuoteApplications
        {
            get
            {
                return this.quoteApplicationsField;
            }
            set
            {
                this.quoteApplicationsField = value;
            }
        }

        [XmlIgnore()]
        public string Xml
        {
            get
            {
                // Prepare XML serializer
                XmlSerializer serializer = new XmlSerializer(typeof(GetQuote));

                // Serialize into Stringbuilder
                StringBuilder bodyBuilder = new StringBuilder();
                StringWriter bodyWriter = new StringWriter(bodyBuilder);
                serializer.Serialize(bodyWriter, this);
                // Replace UTF-16 encoding with UTF-8 encoding
                string body = bodyBuilder.ToString().Replace("utf-16", "utf-8");
                return body;
            }
        }
    }

    [System.Serializable()]
    public class cLoginData
    {

        private int agentIdField;

        private string loginNameField;

        private string passwordField;

        /// <remarks/>
        public int AgentId
        {
            get
            {
                return this.agentIdField;
            }
            set
            {
                this.agentIdField = value;
            }
        }


        /// <remarks/>
        public string LoginName
        {
            get
            {
                return this.loginNameField;
            }
            set
            {
                this.loginNameField = value;
            }
        }

        /// <remarks/>
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }
    }

    [System.Serializable()]
    public class cTraveller
    {

        private string titleField;

        private string firstNameField;

        private string lastNameField;

        private System.DateTime dateOfBirthField;

        private int ageField;

        /// <remarks/>
        public string Title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string LastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        public int Age
        {
            get
            {
                return this.ageField;
            }
            set
            {
                this.ageField = value;
            }
        }
    }

    [System.Serializable()]
    public class cPromotion
    {

        private string promotionCodeField;

        private string nameField;

        /// <remarks/>
        public string PromotionCode
        {
            get
            {
                return this.promotionCodeField;
            }
            set
            {
                this.promotionCodeField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }

    [System.Serializable()]
    public class cEndorsement
    {

        private string endorsementCodeField;

        private bool selectedField;


        private string nameField;

        /// <remarks/>
        public string EndorsementCode
        {
            get
            {
                return this.endorsementCodeField;
            }
            set
            {
                this.endorsementCodeField = value;
            }
        }

        /// <remarks/>
        public bool Selected
        {
            get
            {
                return this.selectedField;
            }
            set
            {
                this.selectedField = value;
            }
        }


        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }

    [System.Serializable()]
    public class cProduct
    {

        private int productIdField;

        private string nameField;

        /// <remarks/>
        public int ProductId
        {
            get
            {
                return this.productIdField;
            }
            set
            {
                this.productIdField = value;
            }
        }


        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }

    [System.Serializable()]
    public class cMainData
    {

        private System.DateTime startDateField;

        private System.DateTime returnDateField;

        private eTripType tripTypeField;

        private eArea areaField;

        private eRelationship relationshipField;

        private cProduct productField;

        private cEndorsement[] endorsementsField;

        private cPromotion promotionField;

        private cTraveller[] travellersField;

        private string reference;

        public System.DateTime StartDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        public System.DateTime ReturnDate
        {
            get
            {
                return this.returnDateField;
            }
            set
            {
                this.returnDateField = value;
            }
        }

        public eTripType TripType
        {
            get
            {
                return this.tripTypeField;
            }
            set
            {
                this.tripTypeField = value;
            }
        }

        public eArea Area
        {
            get
            {
                return this.areaField;
            }
            set
            {
                this.areaField = value;
            }
        }

        public eRelationship Relationship
        {
            get
            {
                return this.relationshipField;
            }
            set
            {
                this.relationshipField = value;
            }
        }

        public cProduct Product
        {
            get
            {
                return this.productField;
            }
            set
            {
                this.productField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("Endorsement", IsNullable = false)]
        public cEndorsement[] Endorsements
        {
            get
            {
                return this.endorsementsField;
            }
            set
            {
                this.endorsementsField = value;
            }
        }

        public cPromotion Promotion
        {
            get
            {
                return this.promotionField;
            }
            set
            {
                this.promotionField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("Traveller", IsNullable = false)]
        public cTraveller[] Travellers
        {
            get
            {
                return this.travellersField;
            }
            set
            {
                this.travellersField = value;
            }
        }

        public string Reference
        {
            get
            {
                return this.reference;
            }
            set
            {
                this.reference = value;
            }
        }
    }

    [System.Serializable()]
    public enum eTripType
    {
        SingleTrip,
        Annual,
        LongStay,
    }

    [System.Serializable()]
    public enum eArea
    {
        UK,
        Europe,
        ANZO,
        Worldwide,
        USA,
    }

    [System.Serializable()]
    public enum eRelationship
    {
        Individual,
        Couple,
        SingleParentFamily,
        Family,
    }

    [System.Serializable()]
    public class cQuoteApplication
    {

        private cMainData mainDataField;

        private bool storeQuoteField;

        /// <remarks/>
        public cMainData MainData
        {
            get
            {
                return this.mainDataField;
            }
            set
            {
                this.mainDataField = value;
            }
        }

        /// <remarks/>
        public bool StoreQuote
        {
            get
            {
                return this.storeQuoteField;
            }
            set
            {
                this.storeQuoteField = value;
            }
        }
    }
}