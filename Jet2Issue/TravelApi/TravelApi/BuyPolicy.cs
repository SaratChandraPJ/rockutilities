﻿using System.Xml.Serialization;
using System.IO;
using System.Text;

namespace TravelApi
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.insurefor.com/ws2/0")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.insurefor.com/ws2/0", IsNullable = false)]
    public partial class BuyPolicy
    {

        private cLoginData loginDataField;

        private string policyHolderIdField;

        private cPolicyHolderData policyHolderDataField;

        private cQuoteApplication quoteApplicationField;

        private bool agreeTermsField;

        private bool agreeTermsFieldSpecified;

        private bool agreePolicyField;

        private bool agreePolicyFieldSpecified;

        private bool agreeDisclosureField;

        private bool agreeDisclosureFieldSpecified;

        private bool wantsEmailCertificateField;

        private bool wantsEmailCertificateFieldSpecified;

        private cSurcharge[] surchargesField;

        private decimal paymentTotalPriceField;

        private bool paymentTotalPriceFieldSpecified;

        private bool paymentOutstandingField;

        private bool paymentOutstandingFieldSpecified;

        private string paymentReferenceField;

        private string referenceField;

        private cPolicy[] linkedPoliciesField;

        /// <remarks/>
        public cLoginData LoginData
        {
            get
            {
                return this.loginDataField;
            }
            set
            {
                this.loginDataField = value;
            }
        }

        /// <remarks/>
        public string PolicyHolderId
        {
            get
            {
                return this.policyHolderIdField;
            }
            set
            {
                this.policyHolderIdField = value;
            }
        }

        /// <remarks/>
        public cPolicyHolderData PolicyHolderData
        {
            get
            {
                return this.policyHolderDataField;
            }
            set
            {
                this.policyHolderDataField = value;
            }
        }

        /// <remarks/>
        public cQuoteApplication QuoteApplication
        {
            get
            {
                return this.quoteApplicationField;
            }
            set
            {
                this.quoteApplicationField = value;
            }
        }

        /// <remarks/>
        public bool AgreeTerms
        {
            get
            {
                return this.agreeTermsField;
            }
            set
            {
                this.agreeTermsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AgreeTermsSpecified
        {
            get
            {
                return this.agreeTermsFieldSpecified;
            }
            set
            {
                this.agreeTermsFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool AgreePolicy
        {
            get
            {
                return this.agreePolicyField;
            }
            set
            {
                this.agreePolicyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AgreePolicySpecified
        {
            get
            {
                return this.agreePolicyFieldSpecified;
            }
            set
            {
                this.agreePolicyFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool AgreeDisclosure
        {
            get
            {
                return this.agreeDisclosureField;
            }
            set
            {
                this.agreeDisclosureField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AgreeDisclosureSpecified
        {
            get
            {
                return this.agreeDisclosureFieldSpecified;
            }
            set
            {
                this.agreeDisclosureFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool WantsEmailCertificate
        {
            get
            {
                return this.wantsEmailCertificateField;
            }
            set
            {
                this.wantsEmailCertificateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool WantsEmailCertificateSpecified
        {
            get
            {
                return this.wantsEmailCertificateFieldSpecified;
            }
            set
            {
                this.wantsEmailCertificateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Surcharge", IsNullable = false)]
        public cSurcharge[] Surcharges
        {
            get
            {
                return this.surchargesField;
            }
            set
            {
                this.surchargesField = value;
            }
        }

        /// <remarks/>
        public decimal PaymentTotalPrice
        {
            get
            {
                return this.paymentTotalPriceField;
            }
            set
            {
                this.paymentTotalPriceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PaymentTotalPriceSpecified
        {
            get
            {
                return this.paymentTotalPriceFieldSpecified;
            }
            set
            {
                this.paymentTotalPriceFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool PaymentOutstanding
        {
            get
            {
                return this.paymentOutstandingField;
            }
            set
            {
                this.paymentOutstandingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PaymentOutstandingSpecified
        {
            get
            {
                return this.paymentOutstandingFieldSpecified;
            }
            set
            {
                this.paymentOutstandingFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string PaymentReference
        {
            get
            {
                return this.paymentReferenceField;
            }
            set
            {
                this.paymentReferenceField = value;
            }
        }

        /// <remarks/>
        public string Reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Policy", IsNullable = false)]
        public cPolicy[] LinkedPolicies
        {
            get
            {
                return this.linkedPoliciesField;
            }
            set
            {
                this.linkedPoliciesField = value;
            }
        }


        [XmlIgnore()]
        public string Xml
        {
            get
            {
                // Prepare XML serializer
                XmlSerializer serializer = new XmlSerializer(typeof(BuyPolicy));

                // Serialize into Stringbuilder
                StringBuilder bodyBuilder = new StringBuilder();
                StringWriter bodyWriter = new StringWriter(bodyBuilder);
                serializer.Serialize(bodyWriter, this);
                // Replace UTF-16 encoding with UTF-8 encoding
                string body = bodyBuilder.ToString().Replace("utf-16", "utf-8");
                return body;
            }
        }
    }
}