﻿using System.Xml.Serialization;
using System.IO;
using System.Text;

namespace TravelApi
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.insurefor.com/ws2/0")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.insurefor.com/ws2/0", IsNullable = false)]
    public partial class CreatePolicyHolder
    {

        private cLoginData loginDataField;

        private cPolicyHolderData policyHolderDataField;

        /// <remarks/>
        public cLoginData LoginData
        {
            get
            {
                return this.loginDataField;
            }
            set
            {
                this.loginDataField = value;
            }
        }

        /// <remarks/>
        public cPolicyHolderData PolicyHolderData
        {
            get
            {
                return this.policyHolderDataField;
            }
            set
            {
                this.policyHolderDataField = value;
            }
        }

        [XmlIgnore()]
        public string Xml
        {
            get
            {
                // Prepare XML serializer
                XmlSerializer serializer = new XmlSerializer(typeof(CreatePolicyHolder));

                // Serialize into Stringbuilder
                StringBuilder bodyBuilder = new StringBuilder();
                StringWriter bodyWriter = new StringWriter(bodyBuilder);
                serializer.Serialize(bodyWriter, this);
                // Replace UTF-16 encoding with UTF-8 encoding
                string body = bodyBuilder.ToString().Replace("utf-16", "utf-8");
                return body;
            }
        }
    }
}