﻿using System.Xml.Serialization;
using System.IO;
using System.Text;

namespace TravelApi
{
    [System.Serializable()]
    [XmlRoot(Namespace = "http://www.insurefor.com/ws2/0")]
    public partial class BuyPolicy1
    {

        private cLoginData loginDataField;

        private string policyHolderIdField;

        private cPolicyHolderData policyHolderDataField;

        private cQuoteApplication quoteApplicationField;

        private bool agreeTermsField;

        private bool agreeTermsFieldSpecified;

        private bool agreePolicyField;

        private bool agreePolicyFieldSpecified;

        private bool agreeDisclosureField;

        private bool agreeDisclosureFieldSpecified;

        private bool wantsEmailCertificateField;

        private bool wantsEmailCertificateFieldSpecified;

        private cSurcharge[] surchargesField;

        private decimal paymentTotalPriceField;

        private bool paymentTotalPriceFieldSpecified;

        /// <remarks/>
        public cLoginData LoginData
        {
            get
            {
                return this.loginDataField;
            }
            set
            {
                this.loginDataField = value;
            }
        }

        /// <remarks/>
        public string PolicyHolderId
        {
            get
            {
                return this.policyHolderIdField;
            }
            set
            {
                this.policyHolderIdField = value;
            }
        }

        /// <remarks/>
        public cPolicyHolderData PolicyHolderData
        {
            get
            {
                return this.policyHolderDataField;
            }
            set
            {
                this.policyHolderDataField = value;
            }
        }

        /// <remarks/>
        public cQuoteApplication QuoteApplication
        {
            get
            {
                return this.quoteApplicationField;
            }
            set
            {
                this.quoteApplicationField = value;
            }
        }

        /// <remarks/>
        public bool AgreeTerms
        {
            get
            {
                return this.agreeTermsField;
            }
            set
            {
                this.agreeTermsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AgreeTermsSpecified
        {
            get
            {
                return this.agreeTermsFieldSpecified;
            }
            set
            {
                this.agreeTermsFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool AgreePolicy
        {
            get
            {
                return this.agreePolicyField;
            }
            set
            {
                this.agreePolicyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AgreePolicySpecified
        {
            get
            {
                return this.agreePolicyFieldSpecified;
            }
            set
            {
                this.agreePolicyFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool AgreeDisclosure
        {
            get
            {
                return this.agreeDisclosureField;
            }
            set
            {
                this.agreeDisclosureField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AgreeDisclosureSpecified
        {
            get
            {
                return this.agreeDisclosureFieldSpecified;
            }
            set
            {
                this.agreeDisclosureFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool WantsEmailCertificate
        {
            get
            {
                return this.wantsEmailCertificateField;
            }
            set
            {
                this.wantsEmailCertificateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool WantsEmailCertificateSpecified
        {
            get
            {
                return this.wantsEmailCertificateFieldSpecified;
            }
            set
            {
                this.wantsEmailCertificateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Surcharge", IsNullable = false)]
        public cSurcharge[] Surcharges
        {
            get
            {
                return this.surchargesField;
            }
            set
            {
                this.surchargesField = value;
            }
        }

        /// <remarks/>
        public decimal PaymentTotalPrice
        {
            get
            {
                return this.paymentTotalPriceField;
            }
            set
            {
                this.paymentTotalPriceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PaymentTotalPriceSpecified
        {
            get
            {
                return this.paymentTotalPriceFieldSpecified;
            }
            set
            {
                this.paymentTotalPriceFieldSpecified = value;
            }
        }

        [XmlIgnore()]
        public string Xml
        {
            get
            {
                // Prepare XML serializer
                XmlSerializer serializer = new XmlSerializer(typeof(BuyPolicy));

                // Serialize into Stringbuilder
                StringBuilder bodyBuilder = new StringBuilder();
                StringWriter bodyWriter = new StringWriter(bodyBuilder);
                serializer.Serialize(bodyWriter, this);
                // Replace UTF-16 encoding with UTF-8 encoding
                string body = bodyBuilder.ToString().Replace("utf-16","utf-8");
                return body;
            }
        }
    }
   
}