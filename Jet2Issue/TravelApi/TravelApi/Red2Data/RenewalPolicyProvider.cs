﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml;
using System.Data;
using System.Reflection;
using System.IO;

namespace TravelApi.Red2Data
{
    public class RenewalPolicyProvider
    {
        RenewalDataContext db = DataContextManager.GetMirrorDataContext();

        //public List<Endorse> getEndorse()
        //{
        //    var lAutoModel = new List<AutoRenewalModel>();
        //    var red2Policy = new List<Policy>();
        //    var lEndorse = new List<Endorse>();

        //    var rPolicies = db.GetTable<AutoRenewal>().Where(a => a.QuoteActive == true).ToList();// Where(a => a.Active == false && a.QuoteDate > Convert.ToDateTime("2012-09-25")).ToList().OrderByDescending(a => a.QuoteDate);
        //    //var rPolicies = db.GetTable<AutoRenewal>().Where(a => a.Policyid == 595816).ToList();

        //    foreach (var d in rPolicies)
        //    {
        //        if (d.AgentId == 0)
        //            continue;

        //        var pProvider = new PolicyProvider();
        //        red2Policy.Add(pProvider.getPolicyByPolicyNumber(d.Policyid));
        //    }

        //    foreach (var c in rPolicies)
        //    {
        //        if (c.AgentId == 0)
        //            continue;

        //        var provider = new PolicyProvider();
        //        var red2 = red2Policy.Single(a => a.PolicyId == c.Policyid);

        //        var Holder = red2.PolicyHolders.SingleOrDefault(a => a.PolicyHolderId == red2.PolicyHolderId);
        //        if (Holder == null)
        //            continue;

        //        var quote = red2.Quotes.SingleOrDefault(a => a.QuoteId == red2.QuoteId);
        //        if (quote == null)
        //            continue;

        //        var policyTravellers = quote.PolicyTravellers.Where(a => a.QuoteId == red2.QuoteId);
        //        if (policyTravellers == null)
        //            continue;


        //        var Endorsements = !string.IsNullOrEmpty(quote.EndorsementCodes) ? quote.EndorsementCodes.Split(new char[] { ',' }).ToList() : new List<string>() { };
        //        var d = new Endorse();
        //        d.PolicyId = c.Policyid;

        //        if (Endorsements != null && Endorsements.Count > 1)
        //        {
        //            d.Endors = quote.EndorsementCodes;
        //        }


        //        //if (!Endorsements.Any(a => a.Contains("LB1")))
        //        //{
        //        //    //if (Endorsements.Count > 1)
        //        //    //{
        //        //    //    Endorsements.RemoveAt(Endorsements.Count);
        //        //    //    Endorsements.Add("LB1");
        //        //    //}
        //        //    //else
        //        //    //{
        //        //    //    Endorsements.Add("LB1");
        //        //    //}

        //        //    //Endorsements.Add("LB1");
        //        //    d.Endors.Add("LB1");

        //        //}
        //        if (Endorsements.Count > 1)
        //            lEndorse.Add(d);
        //    }

        //    var dt = Dt.ToDataTable(lEndorse.ToList());
        //    var ds = new DataSet();
        //    ds.Tables.Add(dt);

        //    ExcelLibrary.DataSetHelper.CreateWorkbook(@"\\serv02\\IT\\AutoRenew\\lEndorse[" + DateTime.Now.ToString("dd-MM-yyyy-hh-MM-ss") + "].xls", ds);

        //    return lEndorse;

        //}

        /// <summary>
        /// For Auto Exe
        /// </summary>
        /// <returns></returns>
        public List<AutoRenewalModel> getRenewalPolicies()
        {

            var lAutoModel = new List<AutoRenewalModel>();
            var red2Policy = new List<Policy>();
            var pProvider = new PolicyProvider();
            var rPolicies = pProvider.getPolicyByPolicyNumber("JEV14999190");
    
            red2Policy.Add(pProvider.getPolicyByPolicyNumber(rPolicies.PolicyId));
            
                    var provider = new PolicyProvider();
                    var red2 = red2Policy.Single(a => a.PolicyId == rPolicies.PolicyId);

                    var Holder = red2.PolicyHolders.SingleOrDefault(a => a.PolicyHolderId == red2.PolicyHolderId);
                    if (Holder == null)
                    {
                        Console.WriteLine("No PolicyHolder Found");
                    }

                    var quote = red2.Quotes.SingleOrDefault(a => a.QuoteId == red2.QuoteId);
                    if (quote == null)
                    {
                        Console.WriteLine("No Quote Found");
                    }

                    var policyTravellers = quote.PolicyTravellers.Where(a => a.QuoteId == red2.QuoteId);
                    if (policyTravellers == null)
                    {
                        Console.WriteLine("No Travellers Found");
                    }

                    var d = Holder.EmailAddress.Split(new Char[] { '@' }).Last();
                    bool Email = false;
                    //Email Validation 
                    //foreach (var dd in db.Emails)
                    //{
                    //    if (Holder.EmailAddress.Split(new Char[] { '@' }).Last().Trim().ToLower() == dd.F1.Trim().ToLower())
                    //    {
                    //        Email = true;
                    //    }
                    //}

                    var Transaction = red2.Payments.SingleOrDefault(a => a.PolicyId == red2.PolicyId && ((a.NameOnCard == null && a.CardType == null && a.PaymentValid == false) || (a.PaymentValid == true)));

                    if (Transaction == null)// && (c.AgentId == 715 || c.AgentId == 716))
                    {
                        Transaction = new Payment();
                        Transaction.TransactionId = Convert.ToString(quote.QuoteId);
                    }
                    //Get Agent Branding from Quote

                    var agentbranding = pProvider.GetAgentBrandingByQuoteId(quote.QuoteId);
                    var Promocodes = quote.PromotionalCode.ToString();

                    //For Linked Policies 
                    if (!string.IsNullOrEmpty(red2.LinkedPolicyIds))
                    {
                        var red2PolicyData = red2.LinkedPolicyIds.Split(new char[] { ',' }).ToArray();

                        for (int j = 0; j < red2PolicyData.Count(); j++)
                        {
                            var red2Linked = provider.getPolicyByPolicyNumber(Convert.ToInt32(red2PolicyData[j]));
                            if ((bool)red2Linked.IsCancelled.HasValue && (bool)red2.IsCancelled.Value == false)
                            {
                                var pHolder = red2Linked.PolicyHolders.Where(a => a.PolicyHolderId == red2Linked.PolicyHolderId).First();
                                var Quote = red2Linked.Quotes.Where(a => a.QuoteId == red2Linked.QuoteId).First();
                                var PolicyTravellers = Quote.PolicyTravellers.Where(a => a.QuoteId == red2Linked.QuoteId);
                                //Rule
                                if (red2Linked.PolicyNumber.StartsWith("IPP"))
                                    continue;
                                lAutoModel.Add(new AutoRenewalModel()
                                {
                                    UserId = 825,//getAgentId(Quote.ProductId),
                                    UserName = agentbranding.Subdomain, //HardCode
                                    Password = agentbranding.Password, //HardCode
                                    FirstName = pHolder.FirstName,
                                    LastName = pHolder.LastName,
                                    TripType = (ApiCall.TripType)Enum.Parse(typeof(ApiCall.TripType), red2.TripTypeId.ToString(), true),
                                    DestinationType = (TravelApi.ApiCall.DestinationType)Enum.Parse(typeof(TravelApi.ApiCall.DestinationType), getTripType(Quote), true),
                                    PolicyStartDate = rPolicies.PolicyStartDate.Value,
                                    PolicyEndDate = rPolicies.PolicyEndDate.Value,
                                    //Products = new string[] { Convert.ToString(getProductId(Quote.ProductId)) },
                                    Products = new string[] { Convert.ToString(1100) }, // change it here
                                    TotalTravellers = PolicyTravellers.Count(),
                                    Group = (TravelApi.ApiCall.GroupType)Enum.Parse(typeof(TravelApi.ApiCall.GroupType), getRelationship(Quote), true),
                                    //  Travellers = new cTraveller[] { new cTraveller() { Age = pHolder.Age.HasValue ? (int)pHolder.Age.Value + 1 : 0, DateOfBirth = pHolder.DateOfBirth.HasValue ? (DateTime)pHolder.DateOfBirth.Value : new DateTime(), FirstName = pHolder.FirstName, LastName = pHolder.LastName, Title = pHolder.Title } },
                                    Travellers = getTravellers(policyTravellers.ToArray(), policyTravellers.Count()),
                                    PromoCode = string.Empty, // retriveing PromoCode from Quote
                                    Endorsements = new string[] { },
                                    Policyid = rPolicies.PolicyId,
                                    IsLinked = true,
                                    OldPrice = rPolicies.PricePaid,
                                    OrderReference = Transaction != null ? Transaction.TransactionId : string.Empty,
                                    Postcode = pHolder.PostcodeZip,
                                    Email = Email,
                                    IsPolicyJub = Quote.UnderwriterId == 165 || Quote.UnderwriterId == 219 ||Quote.UnderwriterId ==350 ? true : false, //Hard Code
                                });
                            }
                        }
                    }

                    var Endorsements = !string.IsNullOrEmpty(quote.EndorsementCodes) ? quote.EndorsementCodes.Split(new char[] { ',' }).ToList() : new List<string>() { };


                    if (Endorsements.Any(a => a.Contains("LB2"))) //Rule or Hard Code
                    {
                        Endorsements.Remove("LB2");
                    }
                    if (Endorsements.Any(a => a.Contains("LB1"))) //Rule or Hard Code
                    {
                        Endorsements.Remove("LB1");
                    }


                    lAutoModel.Add(new AutoRenewalModel()
                    {
                        //UserId = c.AgentId,
                        UserId = 825,//getAgentId(quote.ProductId),
                        UserName = agentbranding.Subdomain,
                        Password = agentbranding.Password,
                        FirstName = Holder.FirstName,
                        LastName = Holder.LastName,
                        TripType = (ApiCall.TripType)Enum.Parse(typeof(ApiCall.TripType), red2.TripTypeId.ToString(), true),
                        DestinationType = (TravelApi.ApiCall.DestinationType)Enum.Parse(typeof(TravelApi.ApiCall.DestinationType), getTripType(quote), true),
                        PolicyStartDate = rPolicies.PolicyStartDate.Value,
                        PolicyEndDate = rPolicies.PolicyEndDate.Value,
                        //Products = new string[] { Convert.ToString(c.ProductId) },
                        Products = new string[] { Convert.ToString(1100) },
                        TotalTravellers = policyTravellers.Count(),
                        Group = (TravelApi.ApiCall.GroupType)Enum.Parse(typeof(TravelApi.ApiCall.GroupType), getRelationship(quote), true),
                        Travellers = getTravellers(policyTravellers.ToArray(), policyTravellers.Count()),
                        PromoCode = getPromocode((int)rPolicies.AgentId,(int)quote.ProductId, Promocodes,quote.ProductId), //Retriving PromoCode from the PromotionalCode Table
                        Endorsements = Endorsements.ToArray(),
                        Policyid = rPolicies.PolicyId,
                        IsLinked = false,
                        OldPrice = rPolicies.PricePaid,
                        OrderReference = Transaction != null ? Transaction.TransactionId : string.Empty,
                        Postcode = Holder.PostcodeZip,
                        Email = Email
                    });
            return lAutoModel;
        }

        public List<AutoRenewalModel> getRenewalPoliciesNew()
        {
            var lAutoModel = new List<AutoRenewalModel>();
            var red2Policy = new List<Policy>();

            var rPolicies = db.GetTable<AutoRenewalNew>().Where(a => a.QuoteActive == true && a.Active == false).ToList();
            //var rPolicies = db.GetTable<AutoRenewal>().Where(a => a.Policyid == 640953).ToList();

            foreach (var d in rPolicies)
            {
                if (d.AgentId == 0)
                    continue;

                var pProvider = new PolicyProvider();
                red2Policy.Add(pProvider.getPolicyByPolicyNumber(d.Policyid));
            }

            foreach (var c in rPolicies)
            {
                if (c.AgentId == 0)
                    continue;

                var provider = new PolicyProvider();
                var red2 = red2Policy.Single(a => a.PolicyId == c.Policyid);

                var Holder = red2.PolicyHolders.SingleOrDefault(a => a.PolicyHolderId == red2.PolicyHolderId);
                if (Holder == null)
                    continue;

                var quote = red2.Quotes.SingleOrDefault(a => a.QuoteId == red2.QuoteId);
                if (quote == null)
                    continue;

                var policyTravellers = quote.PolicyTravellers.Where(a => a.QuoteId == red2.QuoteId);
                if (policyTravellers == null)
                    continue;

                var d = Holder.EmailAddress.Split(new Char[] { '@' }).Last();
                bool Email = false;
                foreach (var dd in db.Emails)
                {
                    if (Holder.EmailAddress.Split(new Char[] { '@' }).Last().Trim().ToLower() == dd.F1.Trim().ToLower())
                    {
                        Email = true;
                    }
                }

                var Transaction = red2.Payments.SingleOrDefault(a => a.PolicyId == red2.PolicyId && ((a.NameOnCard == null && a.CardType == null && a.PaymentValid == false) || (a.PaymentValid == true)));

                if (Transaction == null)// && (c.AgentId == 715 || c.AgentId == 716))
                {
                    Transaction = new Payment();
                    Transaction.TransactionId = Convert.ToString(quote.QuoteId);
                }

                if (!string.IsNullOrEmpty(red2.LinkedPolicyIds))
                {
                    var red2PolicyData = red2.LinkedPolicyIds.Split(new char[] { ',' }).ToArray();

                    for (int j = 0; j < red2PolicyData.Count(); j++)
                    {
                        var red2Linked = provider.getPolicyByPolicyNumber(Convert.ToInt32(red2PolicyData[j]));
                        if ((bool)red2Linked.IsCancelled.HasValue && (bool)red2.IsCancelled.Value == false)
                        {
                            var pHolder = red2Linked.PolicyHolders.Where(a => a.PolicyHolderId == red2Linked.PolicyHolderId).First();
                            var Quote = red2Linked.Quotes.Where(a => a.QuoteId == red2Linked.QuoteId).First();
                            var PolicyTravellers = Quote.PolicyTravellers.Where(a => a.QuoteId == red2Linked.QuoteId);

                            if (red2Linked.PolicyNumber.StartsWith("IPP"))
                                continue;
                            lAutoModel.Add(new AutoRenewalModel()
                            {
                                UserId = getAgentId(quote.ProductId),
                                UserName = "agent14",
                                Password = "agent14",
                                FirstName = pHolder.FirstName,
                                LastName = pHolder.LastName,
                                TripType = (ApiCall.TripType)Enum.Parse(typeof(ApiCall.TripType), red2.TripTypeId.ToString(), true),
                                DestinationType = (TravelApi.ApiCall.DestinationType)Enum.Parse(typeof(TravelApi.ApiCall.DestinationType), getTripType(Quote), true),
                                PolicyStartDate = c.PolicyStartDate.Value.AddYears(1),
                                PolicyEndDate = c.Expirydate.Value.AddYears(1),
                                Products = new string[] { Convert.ToString(getProductId(Quote.ProductId)) },
                                TotalTravellers = PolicyTravellers.Count(),
                                Group = (TravelApi.ApiCall.GroupType)Enum.Parse(typeof(TravelApi.ApiCall.GroupType), getRelationship(Quote), true),
                                Travellers = new cTraveller[] { new cTraveller() { Age = pHolder.Age.HasValue ? (int)pHolder.Age.Value + 1 : 0, DateOfBirth = pHolder.DateOfBirth.HasValue ? (DateTime)pHolder.DateOfBirth.Value : new DateTime(), FirstName = pHolder.FirstName, LastName = pHolder.LastName, Title = pHolder.Title } },
                                PromoCode = string.Empty,
                                Endorsements = new string[] { },
                                Policyid = c.Policyid,
                                IsLinked = true,
                                OldPrice = c.OldPrice,
                                OrderReference = Transaction != null ? Transaction.TransactionId : string.Empty,
                                Postcode = pHolder.PostcodeZip,
                                Email = Email,
                                IsPolicyJub = Quote.UnderwriterId == 165 ? true : false,
                            });
                        }
                    }
                }

                var Endorsements = !string.IsNullOrEmpty(quote.EndorsementCodes) ? quote.EndorsementCodes.Split(new char[] { ',' }).ToList() : new List<string>() { };
                if (!Endorsements.Any(a => a.Contains("LB1")))
                {
                    Endorsements.Add("LB1");
                }

                lAutoModel.Add(new AutoRenewalModel()
                {
                    UserId = c.AgentId,
                    UserName = "agent14",
                    Password = "agent14",
                    FirstName = Holder.FirstName,
                    LastName = Holder.LastName,
                    TripType = (ApiCall.TripType)Enum.Parse(typeof(ApiCall.TripType), red2.TripTypeId.ToString(), true),
                    DestinationType = (TravelApi.ApiCall.DestinationType)Enum.Parse(typeof(TravelApi.ApiCall.DestinationType), getTripType(quote), true),
                    PolicyStartDate = c.PolicyStartDate.Value.AddYears(1),
                    PolicyEndDate = c.Expirydate.Value.AddYears(1),
                    Products = new string[] { Convert.ToString(c.ProductId) },
                    TotalTravellers = policyTravellers.Count(),
                    Group = (TravelApi.ApiCall.GroupType)Enum.Parse(typeof(TravelApi.ApiCall.GroupType), getRelationship(quote), true),
                    Travellers = getTravellers(policyTravellers.ToArray(), policyTravellers.Count()),
                    PromoCode = string.Empty,
                    Endorsements = Endorsements.ToArray(),
                    Policyid = c.Policyid,
                    IsLinked = false,
                    OldPrice = c.OldPrice,
                    OrderReference = Transaction != null ? Transaction.TransactionId : string.Empty,
                    Postcode = Holder.PostcodeZip,
                    Email = Email
                });
            }
            return lAutoModel;
        }

        public List<AutoRenewalModel> getRenewalPolicy(string policyNumber)
        {
            var lAutoModel = new List<AutoRenewalModel>();
            var red2Policy = new List<Policy>();

            var pProvider = new PolicyProvider();

            var red2 = pProvider.getPolicyByPolicyNumber(policyNumber);

            var Holder = red2.PolicyHolders.Where(a => a.PolicyHolderId == red2.PolicyHolderId).First();
            var quote = red2.Quotes.Where(a => a.QuoteId == red2.QuoteId).First();
            var policyTravellers = quote.PolicyTravellers.Where(a => a.QuoteId == red2.QuoteId);
            var TransactionId = red2.Payments.Single(a => a.PolicyId == red2.PolicyId).TransactionId;
            #region old
            //lAutoModel.Add(new AutoRenewalModel()
            //{
            //    UserId = quote.AgentId,
            //    UserName = "autorenew",
            //    Password = "autorenew",
            //    FirstName = Holder.FirstName,
            //    LastName = Holder.LastName,
            //    TripType = (ApiCall.TripType)Enum.Parse(typeof(ApiCall.TripType), red2.TripTypeId.ToString(), true),
            //    DestinationType = (TravelApi.ApiCall.DestinationType)Enum.Parse(typeof(TravelApi.ApiCall.DestinationType), getTripType(quote), true),
            //    PolicyStartDate = red2.PolicyStartDate.Value.AddYears(1),
            //    PolicyEndDate = red2.PolicyEndDate.Value.AddYears(1),
            //    Products = new string[] { Convert.ToString(quote.ProductId) },
            //    TotalTravellers = policyTravellers.Count(),
            //    Group = (TravelApi.ApiCall.GroupType)Enum.Parse(typeof(TravelApi.ApiCall.GroupType), getRelationship(quote), true),
            //    Travellers = getTravellers(policyTravellers.ToArray(), policyTravellers.Count()),
            //    PromoCode = string.Empty,
            //    Endorsements = !string.IsNullOrEmpty(quote.EndorsementCodes) ? quote.EndorsementCodes.Split(new char[] { ',' }) : new string[] { },
            //    Policyid = red2.PolicyId,
            //    IsLinked = false,
            //    Policy = red2,
            //    OrderReference = TransactionId
            //});
            #endregion old
            var provider = new PolicyProvider();
            if (TransactionId == null)// && (c.AgentId == 715 || c.AgentId == 716))
            {

                TransactionId = Convert.ToString(red2.QuoteId);
            }

            if (!string.IsNullOrEmpty(red2.LinkedPolicyIds))
            {
                var red2PolicyData = red2.LinkedPolicyIds.Split(new char[] { ',' }).ToArray();

                for (int j = 0; j < red2PolicyData.Count(); j++)
                {
                    var red2Linked = provider.getPolicyByPolicyNumber(Convert.ToInt32(red2PolicyData[j]));
                    if ((bool)red2Linked.IsCancelled.HasValue && (bool)red2.IsCancelled.Value == false)
                    {
                        var pHolder = red2Linked.PolicyHolders.Where(a => a.PolicyHolderId == red2Linked.PolicyHolderId).First();
                        var Quote = red2Linked.Quotes.Where(a => a.QuoteId == red2Linked.QuoteId).First();
                        var PolicyTravellers = Quote.PolicyTravellers.Where(a => a.QuoteId == red2Linked.QuoteId);
                        //Rule
                        if (red2Linked.PolicyNumber.StartsWith("IPP"))
                            continue;
                        lAutoModel.Add(new AutoRenewalModel()
                        {
                            UserId = getAgentId(Quote.ProductId),
                            UserName = "autorenew", //HardCode
                            Password = "autorenew", //HardCode
                            FirstName = pHolder.FirstName,
                            LastName = pHolder.LastName,
                            TripType = (ApiCall.TripType)Enum.Parse(typeof(ApiCall.TripType), red2Linked.TripTypeId.ToString(), true),
                            DestinationType = (TravelApi.ApiCall.DestinationType)Enum.Parse(typeof(TravelApi.ApiCall.DestinationType), getTripType(Quote), true),
                            PolicyStartDate = red2Linked.PolicyStartDate.Value.AddYears(1),
                            PolicyEndDate = red2Linked.PolicyEndDate.Value.AddYears(1),
                            Products = new string[] { Convert.ToString(getProductId(Quote.ProductId)) },
                            TotalTravellers = PolicyTravellers.Count(),
                            Group = (TravelApi.ApiCall.GroupType)Enum.Parse(typeof(TravelApi.ApiCall.GroupType), getRelationship(Quote), true),
                            //  Travellers = new cTraveller[] { new cTraveller() { Age = pHolder.Age.HasValue ? (int)pHolder.Age.Value + 1 : 0, DateOfBirth = pHolder.DateOfBirth.HasValue ? (DateTime)pHolder.DateOfBirth.Value : new DateTime(), FirstName = pHolder.FirstName, LastName = pHolder.LastName, Title = pHolder.Title } },
                            Travellers = getTravellers(policyTravellers.ToArray(), policyTravellers.Count()),
                            PromoCode = string.Empty,
                            Endorsements = new string[] { },
                            Policyid = red2Linked.PolicyId,
                            IsLinked = true,
                            OldPrice = red2Linked.PricePaid,
                            Policy = red2Linked,
                            OrderReference = TransactionId != null ? TransactionId : string.Empty,
                            Postcode = pHolder.PostcodeZip,
                            IsPolicyJub = Quote.UnderwriterId == 165 || Quote.UnderwriterId == 219 ? true : false, //Hard Code
                        });
                    }
                }
            }

            var Endorsements = !string.IsNullOrEmpty(quote.EndorsementCodes) ? quote.EndorsementCodes.Split(new char[] { ',' }).ToList() : new List<string>() { };


            if (Endorsements.Any(a => a.Contains("LB2"))) //Rule or Hard Code
            {
                Endorsements.Remove("LB2");
            }
            if (Endorsements.Any(a => a.Contains("LB1"))) //Rule or Hard Code
            {
                Endorsements.Remove("LB1");
            }

            lAutoModel.Add(new AutoRenewalModel()
            {
                UserId = getAgentId(quote.ProductId),
                UserName = "autorenew",
                Password = "autorenew",
                FirstName = Holder.FirstName,
                LastName = Holder.LastName,
                TripType = (ApiCall.TripType)Enum.Parse(typeof(ApiCall.TripType), red2.TripTypeId.ToString(), true),
                DestinationType = (TravelApi.ApiCall.DestinationType)Enum.Parse(typeof(TravelApi.ApiCall.DestinationType), getTripType(quote), true),
                PolicyStartDate = red2.PolicyStartDate.Value.AddYears(1),
                PolicyEndDate = red2.PolicyEndDate.Value.AddYears(1),
                Products = new string[] { Convert.ToString(getProductId(quote.ProductId)) },
                TotalTravellers = policyTravellers.Count(),
                Group = (TravelApi.ApiCall.GroupType)Enum.Parse(typeof(TravelApi.ApiCall.GroupType), getRelationship(quote), true),
                Travellers = getTravellers(policyTravellers.ToArray(), policyTravellers.Count()),
                PromoCode = string.Empty,
                Endorsements = Endorsements.ToArray(),
                Policyid = red2.PolicyId,
                IsLinked = false,
                OldPrice = red2.PricePaid,
                Policy=red2,
                OrderReference = TransactionId != null ? TransactionId : string.Empty,
                Postcode = Holder.PostcodeZip,
            });

            return lAutoModel;
        }

        public List<AutoRenewalModel> CreateTestRenewalPolicy(Quote quote)
        {
            var lAutoModel = new List<AutoRenewalModel>();
            var red2Policy = new List<Policy>();

            var pProvider = new PolicyProvider();

            lAutoModel.Add(new AutoRenewalModel()
            {
                UserId = getAgentId(quote.ProductId),
                UserName = "autorenew",
                Password = "autorenew",
                FirstName = "Test",
                LastName = "Test",
                TripType = (ApiCall.TripType)Enum.Parse(typeof(ApiCall.TripType), "2", true),
                DestinationType = (TravelApi.ApiCall.DestinationType)Enum.Parse(typeof(TravelApi.ApiCall.DestinationType), getTripType(quote), true),
                PolicyStartDate = DateTime.Now.AddYears(-1),
                PolicyEndDate = DateTime.Now,
                Products = new string[] { Convert.ToString(getProductId(quote.ProductId)) },
                TotalTravellers = quote.NumberOfTravellers,
                Group = (TravelApi.ApiCall.GroupType)Enum.Parse(typeof(TravelApi.ApiCall.GroupType), getRelationship(quote), true),
                Travellers = getTravellers(quote.PolicyTravellers.ToArray(), quote.NumberOfTravellers),
                PromoCode = string.Empty,
                Endorsements = null,
                IsLinked = false,
                OrderReference =  null
            });

            return lAutoModel;
        }

        /// <summary>
        /// For Transfer EXE
        /// </summary>
        /// <returns>List of Policy Request for Taking the payment</returns>
        public List<AutoRenewalModel> getRenewalPoliciesForAuto()
        {
            var lAutoModel = new List<AutoRenewalModel>();
            var red2Policy = new List<Policy>();
            var rPolicies = db.GetTable<Transaction>().Where(a => a.PolicyActive == true).ToList();
            var provider = new PolicyProvider();

            foreach (var d in rPolicies)
            {
                if (d.AgentId == 0)
                    continue;
                var policy = provider.getPolicyByPolicyNumber((int)d.PolicyId);
                if (policy == null)
                    continue;
                if (policy.IsCancelled == true)
                {
                    var cTransaction = new RenewalPolicyProvider();
                    var cTravelPolicy = cTransaction.getTransaction(policy.PolicyId);
                    //var cPolicies = db.GetTable<Transaction>().Where(a => a.PolicyNumber == policy.PolicyNumber && a.PolicyId == policy.PolicyId).SingleOrDefault();
                    cTravelPolicy.ExceptionDesc = "Policy Cannot be Renewed due to Policy Cancellation";
                    cTravelPolicy.ExceptionId = 6;
                    cTravelPolicy.PolicyActive = false;
                    cTransaction.Update(cTravelPolicy);
                    continue;
                }
                red2Policy.Add(policy);
            }

            foreach (var c in rPolicies)
            {
                if (c.AgentId == 0)
                    continue;

                var red2 = red2Policy.SingleOrDefault(a => a.PolicyId == c.PolicyId);
                if (red2 == null)
                    continue;

                var Holder = red2.PolicyHolders.SingleOrDefault(a => a.PolicyHolderId == red2.PolicyHolderId);
                if (Holder == null)
                    continue;

                var quote = red2.Quotes.SingleOrDefault(a => a.QuoteId == red2.QuoteId);
                if (quote == null)
                    continue;

                var policyTravellers = quote.PolicyTravellers.Where(a => a.QuoteId == red2.QuoteId);
                if (policyTravellers == null)
                    continue;

                var Transaction = red2.Payments.SingleOrDefault(a => a.PolicyId == red2.PolicyId && red2.PolicyId != 2989700 && ((a.NameOnCard == null && a.CardType == null && a.PaymentValid == false) || (a.PaymentValid == true)));

                if (Transaction == null)//&& (c.AgentId == 716 || c.AgentId == 715))
                {
                    Transaction = new Payment();
                    Transaction.TransactionId = Convert.ToString(quote.QuoteId);
                }


                var Endorsements = !string.IsNullOrEmpty(quote.EndorsementCodes) ? quote.EndorsementCodes.Split(new char[] { ',' }).ToList() : new List<string>() { };

                if (Endorsements.Any(a => a.Contains("LB2")))
                {
                    Endorsements.Remove("LB2");
                }
                if (Endorsements.Any(a => a.Contains("LB1")))
                {
                    Endorsements.Remove("LB1");
                }

                if (!string.IsNullOrEmpty(red2.LinkedPolicyIds))
                {
                    var linked = new List<AutoRenewalModel>();
                    var red2PolicyData = red2.LinkedPolicyIds.Split(new char[] { ',' }).ToArray();

                    for (int j = 0; j < red2PolicyData.Count(); j++)
                    {
                        var red2Linked = provider.getPolicyByPolicyNumber(Convert.ToInt32(red2PolicyData[j]));
                        if (red2Linked == null)
                            continue;

                        if (red2Linked.IsCancelled.HasValue && (bool)red2.IsCancelled.Value == false)
                        {
                            var pHolder = red2Linked.PolicyHolders.SingleOrDefault(a => a.PolicyHolderId == red2Linked.PolicyHolderId);
                            if (pHolder == null)
                                continue;

                            var Quote = red2Linked.Quotes.SingleOrDefault(a => a.QuoteId == red2Linked.QuoteId);
                            if (Quote == null)
                                continue;

                            var PolicyTravellers = Quote.PolicyTravellers.Where(a => a.QuoteId == red2Linked.QuoteId);
                            if (PolicyTravellers == null)
                                continue;

                            if (red2Linked.PolicyNumber.StartsWith("IPP"))
                                continue;

                            linked.Add(new AutoRenewalModel()
                            {
                                UserId = getAgentId(Quote.ProductId),
                                UserName = "autorenew",
                                Password = "autorenew",
                                FirstName = pHolder.FirstName,
                                LastName = pHolder.LastName,
                                TripType = (ApiCall.TripType)Enum.Parse(typeof(ApiCall.TripType), red2.TripTypeId.ToString(), true),
                                DestinationType = (TravelApi.ApiCall.DestinationType)Enum.Parse(typeof(TravelApi.ApiCall.DestinationType), getTripType(Quote), true),
                                PolicyStartDate = c.PolicyStartDate.Value.AddYears(1),
                                PolicyEndDate = c.PolicyEndDate.Value.AddYears(1),
                                Products = new string[] { Convert.ToString(getProductId(Quote.ProductId)) },
                                TotalTravellers = PolicyTravellers.Count(),
                                Group = (TravelApi.ApiCall.GroupType)Enum.Parse(typeof(TravelApi.ApiCall.GroupType), getRelationship(Quote), true),
                                Travellers = new cTraveller[] { new cTraveller() { Age = pHolder.Age.HasValue ? (int)pHolder.Age.Value + 1 : 0, DateOfBirth = pHolder.DateOfBirth.HasValue ? (DateTime)pHolder.DateOfBirth.Value : new DateTime(), FirstName = pHolder.FirstName, LastName = pHolder.LastName, Title = pHolder.Title } },
                                PromoCode = string.Empty,
                                Endorsements = new string[] { },
                                //!string.IsNullOrEmpty(Quote.EndorsementCodes) ? Quote.EndorsementCodes.Split(new char[] { ',' }) : 
                                Policyid = (int)c.PolicyId,
                                IsLinked = true,
                                OldPrice = Quote.GrossPriceWithSurchargesAndMedicalScreenings,
                                IsTravel = false,
                                IsPolicyJub = Quote.UnderwriterId == 165 || Quote.UnderwriterId == 219 || Quote.UnderwriterId == 350 ? true : false,
                            });
                        }
                    }

                    lAutoModel.Add(new AutoRenewalModel()
                    {
                        UserId = c.AgentId,
                        UserName = "autorenew",
                        Password = "autorenew",
                        FirstName = Holder.FirstName,
                        LastName = Holder.LastName,
                        TripType = (ApiCall.TripType)Enum.Parse(typeof(ApiCall.TripType), red2.TripTypeId.ToString(), true),
                        DestinationType = (TravelApi.ApiCall.DestinationType)Enum.Parse(typeof(TravelApi.ApiCall.DestinationType), getTripType(quote), true),
                        PolicyStartDate = c.PolicyStartDate.Value.AddYears(1),
                        PolicyEndDate = c.PolicyEndDate.Value.AddYears(1),
                        Products = new string[] { Convert.ToString(c.ProductId) },
                        TotalTravellers = policyTravellers.Count(),
                        Group = (TravelApi.ApiCall.GroupType)Enum.Parse(typeof(TravelApi.ApiCall.GroupType), getRelationship(quote), true),
                        Travellers = getTravellers(policyTravellers.ToArray(), policyTravellers.Count()),
                        PromoCode = string.Empty,
                        Endorsements = Endorsements.ToArray(),
                        Policyid = (int)c.PolicyId,
                        IsLinked = true,
                        IsTravel = true,
                        OrderReference = Transaction == null ? null : Transaction.TransactionId,
                        LinkedPolicies = linked,
                        Policy = red2,
                        QuotedPrice = c.CustomerQuotedPrice.HasValue ? (decimal)c.CustomerQuotedPrice : 0
                    });
                    continue;
                }


                lAutoModel.Add(new AutoRenewalModel()
                {
                    //UserId = c.AgentId,
                    UserId = getAgentId(quote.ProductId),
                    UserName = "autorenew",
                    Password = "autorenew",
                    FirstName = Holder.FirstName,
                    LastName = Holder.LastName,
                    TripType = (ApiCall.TripType)Enum.Parse(typeof(ApiCall.TripType), red2.TripTypeId.ToString(), true),
                    DestinationType = (TravelApi.ApiCall.DestinationType)Enum.Parse(typeof(TravelApi.ApiCall.DestinationType), getTripType(quote), true),
                    PolicyStartDate = c.PolicyStartDate.Value.AddYears(1),
                    PolicyEndDate = c.PolicyEndDate.Value.AddYears(1),
                    //Products = new string[] { Convert.ToString(c.ProductId) },
                    Products = new string[] { Convert.ToString(getProductId(quote.ProductId)) },
                    TotalTravellers = policyTravellers.Count(),
                    Group = (TravelApi.ApiCall.GroupType)Enum.Parse(typeof(TravelApi.ApiCall.GroupType), getRelationship(quote), true),
                    Travellers = getTravellers(policyTravellers.ToArray(), policyTravellers.Count()),
                    PromoCode = string.Empty,
                    Endorsements = Endorsements.ToArray(),
                    Policyid = (int)c.PolicyId,
                    IsLinked = false,
                    IsTravel = true,
                    OrderReference = Transaction == null ? null : Transaction.TransactionId,
                    Policy = red2,
                    QuotedPrice = c.CustomerQuotedPrice.HasValue ? (decimal)c.CustomerQuotedPrice : 0
                });
            }
            return lAutoModel;
        }

        public cTraveller[] getTravellers(PolicyTraveller[] Travellers, int Total)
        {
            var travellers = new cTraveller[Total];
            int i = 0;
            foreach (var member in Travellers)
            {
                travellers[i] = new TravelApi.cTraveller();
                travellers[i].Title = member.Title;
                travellers[i].FirstName = member.FirstName;
                travellers[i].LastName = member.LastName;
                travellers[i].Age = (int)member.Age;
                i++;
            }
            return travellers.OrderByDescending(a => a.Age).ToArray();
        }

        public cTraveller[] getTravellersDebenhams(PolicyTraveller[] Travellers, int Total)
        {
            var travellers = new cTraveller[Total];
            int i = 0;
            foreach (var member in Travellers)
            {
                travellers[i] = new TravelApi.cTraveller();
                travellers[i].Title = member.Title;
                travellers[i].FirstName = member.FirstName;
                travellers[i].LastName = member.LastName;
                travellers[i].Age = (int)member.Age+1;
                i++;
            }
            return travellers.OrderByDescending(a => a.Age).ToArray();
        }

        public cTraveller[] getTravellersMSM(PolicyTraveller[] Travellers, int Total)
        {
            var travellers = new cTraveller[Total];
            int i = 0;
            foreach (var member in Travellers)
            {
                travellers[i] = new TravelApi.cTraveller();
                travellers[i].Title = member.Title;
                travellers[i].FirstName = member.FirstName;
                travellers[i].LastName = member.LastName;
                travellers[i].Age = (int)member.Age;
                i++;
            }
            return travellers.OrderByDescending(a => a.Age).ToArray();
        }

        //Hard Code
        public string getRelationship(Quote quote)
        {
            string GroupType = string.Empty;
            switch (quote.RelationshipType)
            {
                case "Individual":
                    {
                        GroupType = "1";
                        break;
                    }
                case "Couple":
                    {
                        GroupType = "2";
                        break;
                    }
                case "Family":
                    {
                        GroupType = "3";
                        break;
                    }
                case "SingleParentFamily":
                    {
                        GroupType = "4";
                        break;
                    }
            }
            return GroupType;
        }

        //Hard Code
        public string getTripType(Quote quote)
        {
            string DCode = string.Empty;
            switch (quote.DestinationCode)
            {
                case "AreaOne":
                    {
                        DCode = "2";
                        break;
                    }
                case "AreaTwo":
                    {
                        DCode = "2";
                        break;
                    }
                case "AreaThree":
                    {
                        DCode = "3";
                        break;
                    }
                case "AreaFour":
                    {
                        DCode = "4";
                        break;
                    }
                case "AreaFive":
                    {
                        DCode = "5";
                        break;
                    }
            }
                #region old
                //    if (quote.AgentId == 740 || quote.AgentId == 613)
                //    {
                //        switch (quote.DestinationCode)
                //        {
                //            case "AreaOne":
                //                {
                //                    DCode = "2";
                //                    break;
                //                }
                //            case "AreaTwo":
                //                {
                //                    DCode = "2";
                //                    break;
                //                }
                //            case "AreaThree":
                //                {
                //                    DCode = "3";
                //                    break;
                //                }
                //            case "AreaFour":
                //                {
                //                    DCode = "4";
                //                    break;
                //                }
                //            case "AreaFive":
                //                {
                //                    DCode = "5";
                //                    break;
                //                }
                //        }
                //    }
                //    else if (quote.AgentId == 888 || quote.AgentId == 889)
                //    {
                //        switch (quote.DestinationCode)
                //        {
                //            case "AreaOne":
                //                {
                //                    DCode = "1";
                //                    break;
                //                }
                //            case "AreaTwo":
                //                {
                //                    DCode = "2";
                //                    break;
                //                }
                //            case "AreaThree":
                //                {
                //                    DCode = "3";
                //                    break;
                //                }
                //            case "AreaFour":
                //                {
                //                    DCode = "4";
                //                    break;
                //                }
                //            case "AreaFive":
                //                {
                //                    DCode = "5";
                //                    break;
                //                }
                //        }

                //    }


                //    else if (quote.AgentId == 880 || quote.AgentId == 882 || quote.AgentId == 883 || quote.AgentId == 884 || quote.AgentId == 885 || quote.AgentId == 887)
                //    {
                //        switch (quote.DestinationCode)
                //        {
                //            case "AreaOne":
                //                {
                //                    DCode = "1";
                //                    break;
                //                }
                //            case "AreaTwo":
                //                {
                //                    DCode = "2";
                //                    break;
                //                }
                //            case "AreaThree":
                //                {
                //                    DCode = "3";
                //                    break;
                //                }
                //            case "AreaFour":
                //                {
                //                    DCode = "4";
                //                    break;
                //                }
                //            case "AreaFive":
                //                {
                //                    DCode = "5";
                //                    break;
                //                }
                //        }

                //    }

                //    else
                //    {
                //        switch (quote.DestinationCode)
                //        {
                //            case "AreaOne":
                //                {
                //                    DCode = "2";
                //                    break;
                //                }
                //            case "AreaTwo":
                //                {
                //                    DCode = "2";
                //                    break;
                //                }
                //            case "AreaThree":
                //                {
                //                    DCode = "3";
                //                    break;
                //                }
                //            case "AreaFour":
                //                {
                //                    DCode = "4";
                //                    break;
                //                }
                //            case "AreaFive":
                //                {
                //                    DCode = "5";
                //                    break;
                //                }
                //        }
                //    }
                //    return DCode;
                //}
                #endregion
                return DCode;
        }

            //Hard Code
        public string getTripTypeDebenhams(Quote quote)
        {
            string DCode = string.Empty;

            switch (quote.DestinationCode)
            {
                case "AreaOne":
                    {
                        DCode = "2";
                        break;
                    }
                case "AreaTwo":
                    {
                        DCode = "2";
                        break;
                    }
                case "AreaThree":
                    {
                        DCode = "3";
                        break;
                    }
                case "AreaFour":
                    {
                        DCode = "4";
                        break;
                    }
                case "AreaFive":
                    {
                        DCode = "5";
                        break;
                    }
            }




            return DCode;
        }

        public AutoRenewal getRenewalPolicies(string policyNumber)
        {
            var rPolicies = db.GetTable<AutoRenewal>().Single(a => a.PolicyNumber == policyNumber);//.Where(a => a.policystartdate.Value.Date== DateTime.Today.Date.AddDays(23).AddYears(-1)).ToList();
            return rPolicies;
        }

        public AutoRenewalDebenham getRenewalPoliciesDebenhams(string policyNumber)
        {
            var rPolicies = db.GetTable<AutoRenewalDebenham>().Single(a => a.PolicyNumber == policyNumber);//.Where(a => a.policystartdate.Value.Date== DateTime.Today.Date.AddDays(23).AddYears(-1)).ToList();
            return rPolicies;
        }

        public bool Update(AutoRenewal auto)
        {

            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public bool UpdateDebenhams(AutoRenewalDebenham auto)
        {

            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public bool UpdateAuto(AutoRenewalNew auto)
        {

            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public bool Update(Quotes auto)
        {

            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public AutoRenewal getRenewalPolicies(int policyid)
        {
            var rPolicies = db.GetTable<AutoRenewal>().SingleOrDefault(a => a.Policyid == policyid && a.Active == false);//.Where(a => a.policystartdate.Value.Date== DateTime.Today.Date.AddDays(23).AddYears(-1)).ToList();
            return rPolicies;
        }

        public AutoRenewalDebenham getRenewalPoliciesDebenhams(int policyid)
        {
            var rPolicies = db.GetTable<AutoRenewalDebenham>().SingleOrDefault(a => a.Policyid == policyid && a.Active == false);//.Where(a => a.policystartdate.Value.Date== DateTime.Today.Date.AddDays(23).AddYears(-1)).ToList();
            return rPolicies;
        }

        public AutoRenewalNew getRenewalPolicy(int policyid)
        {
            var rPolicies = db.GetTable<AutoRenewalNew>().SingleOrDefault(a => a.Policyid == policyid && a.Active == false);//.Where(a => a.policystartdate.Value.Date== DateTime.Today.Date.AddDays(23).AddYears(-1)).ToList();
            return rPolicies;
        }

        public AutoRenewalTest getRenewalPoliciesTest(int policyid)
        {
            var rPolicies = db.GetTable<AutoRenewalTest>().SingleOrDefault(a => a.Policyid == policyid && a.Active == false);//.Where(a => a.policystartdate.Value.Date== DateTime.Today.Date.AddDays(23).AddYears(-1)).ToList();
            return rPolicies;
        }

        public bool UpdateTest(AutoRenewalTest auto)
        {

            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public Quotes getRenewalPoliciesQuotes(int policyid)
        {
            var rPolicies = db.GetTable<Quotes>().SingleOrDefault(a => a.PolicyId == policyid);//.Where(a => a.policystartdate.Value.Date== DateTime.Today.Date.AddDays(23).AddYears(-1)).ToList();
            return rPolicies;
        }

        public Transaction getTransaction(int policyid)
        {
            var rPolicies = db.GetTable<Transaction>().SingleOrDefault(a => a.PolicyId == policyid);//.Where(a => a.policystartdate.Value.Date== DateTime.Today.Date.AddDays(23).AddYears(-1)).ToList();
            return rPolicies;
        }

        public List<Transaction> getTransaction()
        {
            var rPolicies = db.GetTable<Transaction>().Where(a=>a.PolicyCreatedDate >= DateTime.Now.Date).ToList();//.Where(a => a.policystartdate.Value.Date== DateTime.Today.Date.AddDays(23).AddYears(-1)).ToList();
            return rPolicies;
        }

        public List<AutoRenewal> getAutoRenewalPolicies()
        {
            var rPolicies = db.GetTable<AutoRenewal>().Where(a => a.ProcessedDate >= DateTime.Now.Date).ToList();//.Where(a => a.policystartdate.Value.Date== DateTime.Today.Date.AddDays(23).AddYears(-1)).ToList();
            return rPolicies;
        }

        public bool Update(Transaction transaction)
        {

            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public bool Update(AutoRenewActivityLog AutoRenewActivityLog)
        {

            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public AutoRenewActivityLog Insert(AutoRenewActivityLog item)
        {
            try
            {
                db.AutoRenewActivityLogs.InsertOnSubmit(item);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                return new AutoRenewActivityLog();
            }
            return item;
        }

        public AutoRenewActivityLog getActivityLog(int PolicyId)
        {
            var rPolicies = db.GetTable<AutoRenewActivityLog>().SingleOrDefault(a => a.PolicyId == PolicyId);//.Where(a => a.policystartdate.Value.Date== DateTime.Today.Date.AddDays(23).AddYears(-1)).ToList();
            if (rPolicies == null)
                return null;

            return rPolicies;
        }

        public void SecUpdate()
        {
            var d = db.GetTable<AutoRenewActivityLog>().Where(a => a.getPaymentRequest != null && a.getPaymentResponse != null);

            foreach (var c in d)
            {
                var secRes = new SecPayResponse();
                secRes.PolicyId = Convert.ToString(c.PolicyId);
                secRes.VPNAuthorisationCode = noticia.TryGetElementValue(c.getPaymentResponse, "VPNAuthorisationCode");
                secRes.VPNStatus = Convert.ToBoolean(c.getPaymentResponse.Element("VPNValid").Value);
                secRes.VPNStatusCode = c.getPaymentResponse.Element("VPNStatusCode").Value;
                secRes.Amount = Convert.ToDecimal(c.getPaymentResponse.Element("Amount").Value);
                secRes.OldOrderReference = c.getPaymentResponse.Element("OrderReference").Value;
                secRes.NewOrderReference = noticia.TryGetElementValue(c.getPaymentResponse, "VPNTransactionID");
                secRes.VPNMessage = noticia.TryGetElementValue(c.getPaymentResponse, "VPNMessage");
                secRes.CreatedDate = DateTime.Now;
                secRes.ActivityDate = c.CreatedDate;
                db.SecPayResponses.InsertOnSubmit(secRes);
                db.SubmitChanges();
            }
        }

        public int getProductId(int ProductId)
        {
            try
            {
                var d = db.GetTable<Autorenewalmapping>().SingleOrDefault(a => Convert.ToInt32(a.ProductId) == Convert.ToInt32(ProductId) && a.IsActive == true);
                if (d == null)
                    return 0;

                return (int)d.NewProductID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public string getPromocode(int AgentId,int productid, string promoCode, int oldproductId)
        {
            var c  = db.GetTable<Autorenewalmapping>().SingleOrDefault(a => Convert.ToInt32(a.NewProductID) == Convert.ToInt32(productid) && a.NewAgentid == AgentId && a.ProductId == oldproductId);
            if (c != null)
            {
                int AgentID = (int)c.AgentId;
                var promocode = db.GetTable<PromotionalCode>().Where(a => a.AgentId == AgentID && a.IsForAutoRenewal == true).ToList();
                if (promocode != null)
                {
                    if (promocode.Any(a=>a.PromotionalCodeName.Contains(promoCode)))
                    {
                        return promoCode;
                    }
                    else
                    {
                        return null;
                    } 
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }

        }

        public int getAgentId(int ProductId)
        {
            //   var ff = from f in db.AutorenewalMappings where Convert.ToInt32(f.ProductId.Value) == ProductId select f;
            try
            {
                //Need to add is active in the condition
                var d = db.GetTable<Autorenewalmapping>().SingleOrDefault(a => Convert.ToInt32(a.ProductId) == Convert.ToInt32(ProductId) && a.IsActive == true);
                if (d == null)
                    return 0;
                //var t = (int)d.NewAgentid;
                return (int)d.NewAgentid;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public List<AutoRenewalModel> getQuotes()
        {
            var lAutoModel = new List<AutoRenewalModel>();
            var red2Policy = new List<Policy>();

            var rPolicies = db.GetTable<Quotes>();
            //var rPolicies = db.GetTable<AutoRenewal>().Where(a => a.Policyid == 449480).ToList();

            foreach (var d in rPolicies)
            {
                if (d.AgentId == 0)
                    continue;

                var pProvider = new PolicyProvider();
                red2Policy.Add(pProvider.getPolicyByPolicyNumber(Convert.ToInt32(d.PolicyId)));
            }

            foreach (var c in rPolicies)
            {
                if (c.AgentId == 0)
                    continue;

                var provider = new PolicyProvider();
                var red2 = red2Policy.Single(a => a.PolicyId == Convert.ToInt32(c.PolicyId));

                var Holder = red2.PolicyHolders.SingleOrDefault(a => a.PolicyHolderId == red2.PolicyHolderId);
                if (Holder == null)
                    continue;

                var quote = red2.Quotes.SingleOrDefault(a => a.QuoteId == red2.QuoteId);
                if (quote == null)
                    continue;

                var policyTravellers = quote.PolicyTravellers.Where(a => a.QuoteId == red2.QuoteId);
                if (policyTravellers == null)
                    continue;

                var Transaction = red2.Payments.SingleOrDefault(a => a.PolicyId == red2.PolicyId && ((a.NameOnCard == null && a.CardType == null && a.PaymentValid == false) || (a.PaymentValid == true)));

                lAutoModel.Add(new AutoRenewalModel()
                {
                    UserId = Convert.ToInt32(c.AgentId),
                    UserName = "agent14",
                    Password = "agent14",
                    FirstName = Holder.FirstName,
                    LastName = Holder.LastName,
                    TripType = (ApiCall.TripType)Enum.Parse(typeof(ApiCall.TripType), red2.TripTypeId.ToString(), true),
                    DestinationType = (TravelApi.ApiCall.DestinationType)Enum.Parse(typeof(TravelApi.ApiCall.DestinationType), getTripType(quote), true),
                    PolicyStartDate = c.PolicyStartDate.Value.AddYears(1),
                    PolicyEndDate = c.PolicyEndDate.Value.AddYears(1),
                    Products = new string[] { Convert.ToString(Convert.ToInt32(c.New_PI)) },
                    TotalTravellers = policyTravellers.Count(),
                    Group = (TravelApi.ApiCall.GroupType)Enum.Parse(typeof(TravelApi.ApiCall.GroupType), getRelationship(quote), true),
                    Travellers = getTravellers(policyTravellers.ToArray(), policyTravellers.Count()),
                    PromoCode = string.Empty,
                    Endorsements = !string.IsNullOrEmpty(quote.EndorsementCodes) ? quote.EndorsementCodes.Split(new char[] { ',' }) : new string[] { },
                    Policyid = Convert.ToInt32(c.PolicyId),
                    IsLinked = false,
                    OldPrice = Convert.ToDecimal(c.PricePaid),
                    OrderReference = Transaction != null ? Transaction.TransactionId : string.Empty
                });
            }

            return lAutoModel;
        }

        //public void FailedTransaction()
        //{
        //    var d = from c in db.AutoRenewActivityLogs
        //            join e in db.Transactions on c.PolicyId equals e.PolicyId
        //            where c.CreatedDate.Value.Date == DateTime.Now.Date && c.getPaymentRequest != null && c.getPaymentResponse != null && c.bQuoteRequest == null
        //            select new
        //            {
        //                AgentId = e.AgentId,
        //                ProductId = e.ProductId,
        //                OldPolicyId = e.PolicyId,
        //                OldPolicyNumber = e.PolicyNumber,
        //                QuoteId = Convert.ToString(c.gQuoteResponse).Substring(Convert.ToString(c.gQuoteResponse).IndexOf("<QuoteId>") + 9, (Convert.ToString(c.gQuoteResponse).IndexOf("</QuoteId>") - Convert.ToString(c.gQuoteResponse).IndexOf("<QuoteId>")) - 9),
        //                pStartDate = e.PolicyStartDate,
        //                pEndDate = e.PolicyEndDate,
        //                EmailAddress = e.Emailaddress,
        //                CustomerQuotedPrice = e.CustomerQuotedPrice,
        //                SecPayMessage = c.getPaymentResponse.Element("VPNMessage").Value
        //            };

        //    var dt = Dt.ToDataTable(d.ToList());
        //    var ds = new DataSet();
        //    ds.Tables.Add(dt);

        //    ExcelLibrary.DataSetHelper.CreateWorkbook(@"\\serv02\\IT\\AutoRenew\\FailedTransaction[" + DateTime.Now.ToString("dd-MM-yyyy-hh-MM-ss") + "].xls", ds);
        //}

        public AutoRenewalModel getMSM(int PolicyId, out string pId, out string reference)
        {

            var lAutoModel = new AutoRenewalModel();
            var red2Policy = new List<Policy>();
            pId = string.Empty;
            reference = string.Empty;


            var pProvider = new PolicyProvider();
            red2Policy.Add(pProvider.getPolicyByPolicyNumber(PolicyId));

            try
            {


                var provider = new PolicyProvider();
                var red2 = red2Policy.Single(a => a.PolicyId == PolicyId);

                var Holder = red2.PolicyHolders.SingleOrDefault(a => a.PolicyHolderId == red2.PolicyHolderId);


                var quote = red2.Quotes.SingleOrDefault(a => a.QuoteId == red2.QuoteId);


                var policyTravellers = quote.PolicyTravellers.Where(a => a.QuoteId == red2.QuoteId);


                var d = Holder.EmailAddress.Split(new Char[] { '@' }).Last();
                bool Email = false;
                foreach (var dd in db.Emails)
                {
                    if (Holder.EmailAddress.Split(new Char[] { '@' }).Last().Trim().ToLower() == dd.F1.Trim().ToLower())
                    {
                        Email = true;
                    }
                }

                var Transaction = red2.Payments.SingleOrDefault(a => a.PolicyId == red2.PolicyId && ((a.NameOnCard == null && a.CardType == null && a.PaymentValid == false) || (a.PaymentValid == true)));

                if (Transaction == null)// && (c.AgentId == 715 || c.AgentId == 716))
                {
                    Transaction = new Payment();
                    Transaction.TransactionId = Convert.ToString(quote.QuoteId);
                }



                var Endorsements = !string.IsNullOrEmpty(quote.EndorsementCodes) ? quote.EndorsementCodes.Split(new char[] { ',' }).ToList() : new List<string>() { };
                if (Endorsements.Contains("WDG") || Endorsements.Contains("WED") || Endorsements.Contains("MSM131"))
                {
                    Endorsements.Remove("WDG");
                    Endorsements.Remove("WED");
                    Endorsements.Remove("MSM131");
                }
                pId = Convert.ToString(Holder.PolicyHolderId);
                reference = quote.Reference;

                lAutoModel = (new AutoRenewalModel()
                {
                    UserId = 903,
                    UserName = "msmi4",
                    Password = "msmi4",
                    FirstName = Holder.FirstName,
                    LastName = Holder.LastName,
                    TripType = (ApiCall.TripType)Enum.Parse(typeof(ApiCall.TripType), red2.TripTypeId.ToString(), true),
                    DestinationType = (TravelApi.ApiCall.DestinationType)Enum.Parse(typeof(TravelApi.ApiCall.DestinationType), getTripType(quote), true),
                    PolicyStartDate = red2.PolicyStartDate,
                    PolicyEndDate = red2.PolicyEndDate,
                    Products = new string[] { Convert.ToString(quote.ProductId) },
                    TotalTravellers = policyTravellers.Count(),
                    Group = (TravelApi.ApiCall.GroupType)Enum.Parse(typeof(TravelApi.ApiCall.GroupType), getRelationship(quote), true),
                    Travellers = getTravellersMSM(policyTravellers.ToArray(), policyTravellers.Count()),
                    PromoCode = string.Empty,
                    Endorsements = Endorsements.ToArray(),
                    Policyid = PolicyId,
                    IsLinked = false,
                    // OldPrice = c.OldPrice,
                    OrderReference = Transaction != null ? Transaction.TransactionId : string.Empty,
                    Postcode = Holder.PostcodeZip,
                    Email = Email
                });

            }
            catch (Exception ex)
            {

            }
            // }
            return lAutoModel;
        }
    }


    public static class noticia
    {
        public static string TryGetElementValue(this XElement parentEl, string elementName, string defaultValue = null)
        {
            var foundEl = parentEl.Element(elementName);
            if (foundEl != null)
            {
                return foundEl.Value;
            }
            else
            {
                return defaultValue;
            }
        }

        public static DataTable ToDataTable<T>(this IEnumerable<T> collection)
        {
            DataTable dt = new DataTable("DataTable");
            Type t = typeof(T);
            PropertyInfo[] pia = t.GetProperties();

            //Inspect the properties and create the columns in the DataTable
            foreach (PropertyInfo pi in pia)
            {
                Type ColumnType = pi.PropertyType;
                if ((ColumnType.IsGenericType))
                {
                    ColumnType = ColumnType.GetGenericArguments()[0];
                }
                dt.Columns.Add(pi.Name, ColumnType);
            }

            //Populate the data table
            foreach (T item in collection)
            {
                DataRow dr = dt.NewRow();
                dr.BeginEdit();
                foreach (PropertyInfo pi in pia)
                {
                    if (pi.GetValue(item, null) != null)
                    {
                        dr[pi.Name] = pi.GetValue(item, null);
                    }
                }
                dr.EndEdit();
                dt.Rows.Add(dr);
            }
            return dt;
        }
    }

    public class Endorse
    {
        public int PolicyId { get; set; }

        public string Endors { get; set; }
    }
}
