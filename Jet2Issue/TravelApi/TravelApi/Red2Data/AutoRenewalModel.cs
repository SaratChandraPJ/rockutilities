﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TravelApi.Red2Data
{
    public class AutoRenewalModel
    {
        public AutoRenewalModel() { }

        public int Policyid { get; set; }

        public string PolicyNumber { get; set; }

        public int QuoteId { get; set; }

        public System.Nullable<System.DateTime> PolicyStartDate { get; set; }

        public System.Nullable<System.DateTime> PolicyEndDate { get; set; }

        public string EmailAddress { get; set; }

        public System.Nullable<int> UserId { get; set; }

        public string AgentName { get; set; }

        public System.DateTime ProcessedDate { get; set; }

        public System.Nullable<bool> Active { get; set; }

        public System.Nullable<decimal> TravelPrice { get; set; }

        public System.Nullable<decimal> GadgetPrice { get; set; }

        public System.Nullable<decimal> TotalPrice { get; set; }

        public string Exception { get; set; }

        public System.Nullable<bool> QuoteActive { get; set; }

        public System.Nullable<bool> IsAged74 { get; set; }

        public System.Nullable<System.DateTime> QuoteDate { get; set; }

        public System.Nullable<System.DateTime> RenewalDate { get; set; }

        public System.Nullable<System.DateTime> InitialemailDate { get; set; }

        public System.Nullable<System.DateTime> PaymentDate { get; set; }

        public System.Nullable<System.DateTime> Finaloptoutdate { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Title { get; set; }

        public System.Nullable<int> ProductId { get; set; }

        public System.Nullable<decimal> OldPrice { get; set; }

        public Quote Quote { get; set; }

        public TravelApi.ApiCall.TripType TripType { get; set; }

        public TravelApi.ApiCall.DestinationType DestinationType { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string[] Products { get; set; }

        public int TotalTravellers { get; set; }

        public TravelApi.ApiCall.GroupType Group { get; set; }

        public cTraveller[] Travellers { get; set; }

        public string PromoCode { get; set; }

        public string[] Endorsements { get; set; }

        public bool IsLinked { get; set; }

        public Policy Policy { get; set; }

        public string OrderReference { get; set; }

        public List<AutoRenewalModel> LinkedPolicies { get; set; }

        public bool IsTravel { get; set; }

        public bool IsPolicyJub { get; set; }

        public decimal QuotedPrice { get; set; }

        public string Postcode { get; set; }

        public bool Email { get; set; }

    }

    public class RemovePolicyModel
    {
        public RemovePolicyModel() { }

        public string PolicyNumber { get; set; }

        public string EmailId { get; set; }

        public string PolicyholderName { get; set; }

        public int OptOutReasonId { get; set; }

        public string Notes { get; set; }

        public string Month { get; set; }

        public DateTime? ExpiryDate { get; set; }
    }

    public class SecPayResponseModel
    {
        public SecPayResponseModel() { }
        public string OrderReference { get; set; }
        public decimal Amount { get; set; }
        public bool VPNValid { get; set; }
        public string VPNStatusCode { get; set; }
        public string VPNAuthorisationCode { get; set; }
        public string VPNTransactionID { get; set; }
    }
}
