﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TravelApi.Red2Data
{
    public class PolicyProvider
    {
        Red2DataContext dbContext = DataContextManager.GetRed2DataContext();
        public Policy getPolicyByPolicyNumber(int PolicyId)
        {
            var policy = dbContext.GetTable<Policy>().SingleOrDefault(a => a.PolicyId == PolicyId);
            if (policy == null)
                return null;
            return policy;
        }

        public List<Policy> getPolicyByPolicyNumber()
        {
            var policy = dbContext.GetTable<Policy>().ToList();
            return policy;
        }

        public cPolicyHolderData getPolicyHolderByPolicyId (int policyId)
        {
            cPolicyHolderData cp = new cPolicyHolderData();
            var policy = dbContext.GetTable<Policy>().Where(b => b.PolicyId == policyId).First();
            var p = dbContext.GetTable<PolicyHolder>().Where(a => a.PolicyHolderId == policy.PolicyHolderId);
            if(p != null)
            {
                cp.Address1 = p.First().AddressLineOne;
                cp.Address2 = p.First().AddressLineTwo;
                cp.Address3 = p.First().AddressLineThree;
                cp.Address4= p.First().CountyState;
                cp.Age = p.First().Age.GetValueOrDefault();
                cp.Country = p.First().Country;
                cp.DateOfBirth = p.First().DateOfBirth.GetValueOrDefault();
                cp.DateOfBirthSpecified = p.First().DateOfBirth != null ? true : false;
                cp.Email = p.First().EmailAddress;
                cp.FirstName = p.First().FirstName;
                cp.LastName = p.First().LastName;
                cp.PhoneDaytime = p.First().PhoneDaytime;
                cp.PhoneEvening = p.First().PhoneEvening;
                cp.Postcode = p.First().PostcodeZip;
                cp.Title = p.First().Title;
                cp.WantsEmailUpdates = p.First().WantsEmailUpdates.GetValueOrDefault();
                cp.WantsEmailUpdatesSpecified = p.First().WantsEmailUpdates == true ? true : false;
                return cp;
            }
            else
            {
                return cp;
            }
        }

        public AgentBranding GetAgentBrandingByQuoteId (int quoteId)
        {
            var quote = dbContext.GetTable<Quote>().Where(q => q.QuoteId == quoteId).First();
            var ab = dbContext.GetTable<AgentBranding>().Where(a => a.AgentBrandingId == quote.AgentBrandingId).First();
            return ab;
        }

        public Policy getPolicyByPolicyNumber(string policynumber)
        {
            var policy = dbContext.GetTable<Policy>().Single(a => a.PolicyNumber == policynumber);
            return policy;
        }

        public bool UpdatePolicyNumber(string policynumber, string OldPolicyNumber)
        {
            try
            {
                var policy = dbContext.GetTable<Policy>().Single(a => a.PolicyNumber == OldPolicyNumber);
                policy.Comments = "Current Policy is Renewed and New Policy Number is :" + policynumber;
                policy.IsRenewed = true;
                dbContext.SubmitChanges();
                return true;
            }
            catch (Exception E)
            {
                SendEmail S = new SendEmail();
                S.send("Exception while updating the Policy Number: " + E.ToString(), "Exception while updating the Policy Number");
                Console.WriteLine("Exception while updating the Policy Number :" + E.ToString());
                return false;
            }
        }

        public Agent getAgent(int AgentID)
        {
            var policy = dbContext.GetTable<Agent>().Single(a => a.AgentId == AgentID);
            return policy;
        }

        public bool getPolicy(string policyNumber, string EmailAddress, ref RemovePolicyModel rePolicyModel)
        {
            try
            {
                if (string.IsNullOrEmpty(policyNumber) || string.IsNullOrEmpty(EmailAddress))
                {
                    return false;
                }

                var policy = dbContext.GetTable<Policy>().SingleOrDefault(a => a.PolicyNumber.Trim().ToLower() == policyNumber.Trim().ToLower() && (a.IsCancelled == false || a.IsCancelled == null) && a.PolicyEndDate > DateTime.Today.AddDays(-8));

                if (policy == null)
                    return false;

                rePolicyModel.PolicyNumber = policy.PolicyNumber;
                var pHolder = policy.PolicyHolders[0];//.SingleOrDefault(a => a.EmailAddress.ToLower().Trim() == EmailAddress.ToLower().Trim());

                if (pHolder == null)
                    return false;

                rePolicyModel.EmailId = pHolder.EmailAddress;
                rePolicyModel.PolicyholderName = pHolder.Title + " " + pHolder.FirstName + " " + pHolder.LastName;
                rePolicyModel.ExpiryDate = policy.PolicyEndDate;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Insert(RemovePolicyModel item)
        {
            try
            {
                var dbContext = DataContextManager.GetAutotDataContext();

                var unModel = new AutoRenew();
                unModel.CreatredDate = DateTime.Now;
                unModel.EmailAddress = item.EmailId;
                unModel.PolicyNumber = item.PolicyNumber;
                unModel.OptOutReasonId = item.OptOutReasonId;
                unModel.Notes = item.Notes;
                unModel.Month = item.Month;
                dbContext.AutoRenews.InsertOnSubmit(unModel);
                dbContext.SubmitChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public IQueryable<OptOutReason> GetOptOutReasons()
        {
            List<string> Reasons = new List<string>();
            var dbContext = DataContextManager.GetAutotDataContext();
            var query = from r in dbContext.OptOutReasons
                        select r;
            return query;
        }
    }
}
