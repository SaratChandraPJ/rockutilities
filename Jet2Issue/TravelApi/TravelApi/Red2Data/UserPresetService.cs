﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace TravelApi.Red2Data
{
   public  class UserPresetService
    {
        readonly string NamePrefix = "getqp";
        Red2DataContext db = DataContextManager.GetRed2DataContext();
        //Set Data to user information controls.
        public string CreateUserPreset(AutoRenewalModel UP, int quoteid)
        {
            int UPId = 0;
            UserPreset PresetUser = new UserPreset();

            PresetUser.CustomerEmail = UP.Policy.PolicyHolders[0].EmailAddress;
            PresetUser.CustomerLastName = UP.LastName;
            PresetUser.CustomerName = UP.FirstName;
            PresetUser.CustomerPhone = UP.Policy.PolicyHolders[0].PhoneDaytime;
            PresetUser.DateCreated = DateTime.Now;
            PresetUser.PolicyNumber = UP.PolicyNumber;
            PresetUser.QuotaStartDate = UP.PolicyStartDate;
            PresetUser.QuotaFinishDate = (UP.PolicyEndDate == null) ?
                             Convert.ToDateTime(UP.PolicyStartDate).AddYears(1) : UP.PolicyEndDate;
            PresetUser.ServiceAgentName = "AutoRenew";

            PresetUser.PresetValue = CreatePresetValue(UP,quoteid);
            UPId = Save(PresetUser);
            if (UPId > 0)
                return "QT" + UPId;
            else
                return null;
        }

        public int Save(UserPreset UP)
        {
            int UPId = 0;
            try
            {
                if (UP.Id == 0)
                {
                    UP.DateCreated = DateTime.Now;
                    db.UserPresets.InsertOnSubmit(UP);
                    db.SubmitChanges();
                    UPId = UP.Id;
                }
                else
                {
                    db.SubmitChanges();
                    UPId = (UP.Id);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message.ToString());
            }
            return UPId;
        }

        public byte[] CreatePresetValue (AutoRenewalModel UP,int quoteid)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add(NamePrefix + "travellers", SerializationExtensions.ToJson(UP.Travellers));
            //dic.Add("getqptravellers", "age:" + UP.Policy.PolicyHolders[0].Age);
            dic.Add("getqpLoginEmailAddress", "");
            dic.Add("getqpLoginPassword", "");
            dic.Add("getqpQuoteAgent", UP.Policy.AgentId.ToString());
            dic.Add("getqpQuoteBranding", UP.Policy.Agents[0].AgentBrandings[0].AgentBrandingId.ToString());
            dic.Add("getqpQuoteArea", UP.DestinationType.ToString());
            dic.Add("getqpQuoteTripType", UP.TripType.ToString());
            dic.Add("getqpQuoteStartDate", UP.PolicyStartDate.ToString());
            dic.Add("getqpQuoteReturnDate", "");
            dic.Add("getqpQuoteTotalCount", UP.TotalTravellers.ToString());
            TravellerUpdate(ref dic, UP);
            ChildUpdate(ref dic, UP);
            dic.Add("getqpQuoteRelationship", UP.Group.ToString());
            dic.Add("getqpQuotePromotionCodeDropDown", "");
            dic.Add("getqpQuoteRetrieveQuote", "");
            EndorsementUpdate(ref dic, UP,quoteid);
           // ProductUpdate(ref dic, UP);
            dic.Add("getqpPresetReference", "");
            dic.Add("getqpQuotePromotionCode", UP.PromoCode);

            var memorystream = new MemoryStream();
            var bf = new BinaryFormatter();
            bf.Serialize(memorystream, dic);
            return memorystream.ToArray();
        }
        public void ProductUpdate (ref Dictionary<string,string> dic,AutoRenewalModel UP)
        {
            if (UP.Products.Contains("Level 1 Gadet"))
            {
                dic.Add("getqpOptionalProduct0", "True");
            }
            else
            {
                dic.Add("getqpOptionalProduct0", "False");
            }
            if (UP.Products.Contains("Level 2 Gadet"))
            {
                dic.Add("getqpOptionalProduct1", "True");
            }
            else
            {
                dic.Add("getqpOptionalProduct1", "False");
            }
            if (UP.Products.Contains("Level 3 Gadet"))
            {
                dic.Add("getqpOptionalProduct2", "True");
            }
            else
            {
                dic.Add("getqpOptionalProduct2", "False");
            }
            if (UP.Products.Contains("Car Hire Excess Insurance"))
            {
                dic.Add("getqpOptionalProduct3", "True");
            }
            else
            {
                dic.Add("getqpOptionalProduct3", "False");
            }
            if (UP.Products.Contains(""))
            {
                dic.Add("getqpOptionalProduct4", "True");
            }
            else
            {
                dic.Add("getqpOptionalProduct4", "False");
            }
            if (UP.Products.Contains(""))
            {
                dic.Add("getqpOptionalProduct5", "True");
            }
            else
            {
                dic.Add("getqpOptionalProduct5", "False");
            }
        }


        public void EndorsementUpdate (ref Dictionary<string,string> dic,AutoRenewalModel UP,int quoteid)
        {
            dic.Add(NamePrefix + "QuoteEndorsement0", "False");
            dic.Add(NamePrefix + "QuoteEndorsement1", "False");
            dic.Add(NamePrefix + "QuoteEndorsement2", "False");
            dic.Add(NamePrefix + "QuoteEndorsement3", "False");
            dic.Add(NamePrefix + "QuoteEndorsement4", "False");
            dic.Add(NamePrefix + "QuoteEndorsement5", "False");
            dic.Add(NamePrefix + "QuoteEndorsement6", "False");
            dic.Add(NamePrefix + "QuoteEndorsement7", "False");
            dic.Add(NamePrefix + "QuoteEndorsement8", "False");
            dic.Add(NamePrefix + "QuoteEndorsement9", "False");
            dic.Add(NamePrefix + "QuoteEndorsement10", "False");
            dic.Add(NamePrefix + "QuoteEndorsement11", "False");
            dic.Add(NamePrefix + "QuoteEndorsement12", "False");
            dic.Add(NamePrefix + "QuoteEndorsement13", "False");
            dic.Add(NamePrefix + "QuoteEndorsement14", "False");
            dic.Add(NamePrefix + "QuoteEndorsement15", "False");
            dic.Add(NamePrefix + "QuoteEndorsement16", "False");
            dic.Add(NamePrefix + "QuoteEndorsement17", "False");
            dic.Add(NamePrefix + "QuoteEndorsement18", "False");
            dic.Add(NamePrefix + "QuoteEndorsement19", "False");
            dic.Add(NamePrefix + "OptionalProduct1", "False");
            dic.Add(NamePrefix + "OptionalProduct2", "False");
            dic.Add(NamePrefix + "OptionalProduct3", "False");
            dic.Add(NamePrefix + "OptionalProduct4", "False");
            dic.Add(NamePrefix + "OptionalProduct5", "False");

            //int l = 0;
            //int i = 0, j = 20, k = 0;

            int AgentBrandingId = UP.Policy.Agents[0].AgentBrandings[0].AgentBrandingId;

            var r = (from n in db.AgentBrandings
                    where n.AgentBrandingId == AgentBrandingId && n.AgentId == UP.Policy.AgentId
                    select n).FirstOrDefault();

            var productAgentBrandingId = (from p in db.Quotes
                                       where p.QuoteId == quoteid
                                       select p.AgentBrandingId).FirstOrDefault();

            var ProductAgentBranding = (from PA in db.AgentBrandings
                                       where PA.AgentBrandingId == productAgentBrandingId
                                       select PA).FirstOrDefault();

            if (UP.Endorsements != null && UP.Endorsements.Any())
            {
                foreach (var e in UP.Endorsements)
                {
                    if (e.Equals(r.EndorsementOneCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement0"] = "True";
                    }
                    if (e.Equals(r.EndorsementTwoCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement1"] = "True";
                    }
                    if (e.Equals(r.EndorsementThreeCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement2"] = "True";
                    }
                    if (e.Equals(r.EndorsementFourCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement3"] = "True";
                    }
                    if (e.Equals(r.EndorsementFiveCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement4"] = "True";
                    }
                    if (e.Equals(r.EndorsementSixCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement5"] = "True";
                    }
                    if (e.Equals(r.EndorsementSevenCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement6"] = "True";
                    }
                    if (e.Equals(r.EndorsementEightCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement7"] = "True";
                    }
                    if (e.Equals(r.EndorsementNineCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement8"] = "True";
                    }
                    if (e.Equals(r.EndorsementTenCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement9"] = "True";
                    }
                    if (e.Equals(r.EndorsementElevenCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement10"] = "True";
                    }
                    if (e.Equals(r.EndorsementTwelveCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement11"] = "True";
                    }
                    if (e.Equals(r.EndorsementThirteenCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement12"] = "True";
                    }
                    if (e.Equals(r.EndorsementFourteenCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement13"] = "True";
                    }
                    if (e.Equals(r.EndorsementFifteenCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement14"] = "True";
                    }
                    if (e.Equals(r.EndorsementSixteenCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement15"] = "True";
                    }
                    if (e.Equals(r.EndorsementSeventeenCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement16"] = "True";
                    }
                    if (e.Equals(r.EndorsementEighteenCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement17"] = "True";
                    }
                    if (e.Equals(r.EndorsementNineteenCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement18"] = "True";
                    }
                    if (e.Equals(r.EndorsementTwentyCode))
                    {
                        dic[NamePrefix + "QuoteEndorsement19"] = "True";
                    }
                }
            }
            if (UP.LinkedPolicies != null)
            {
                foreach (var p in UP.LinkedPolicies)
                {
                    if (Convert.ToInt32(p.Products[0]).Equals(ProductAgentBranding.OptionalProductOneId))
                    {
                        dic[NamePrefix + "OptionalProduct0"] = "True";
                    }
                    if (Convert.ToInt32(p.Products[0]).Equals(ProductAgentBranding.OptionalProductTwoId))
                    {
                        dic[NamePrefix + "OptionalProduct1"] = "True";
                    }
                    if (Convert.ToInt32(p.Products[0]).Equals(ProductAgentBranding.OptionalProductThreeId))
                    {
                        dic[NamePrefix + "OptionalProduct2"] = "True";
                    }
                    if (Convert.ToInt32(p.Products[0]).Equals(ProductAgentBranding.OptionalProductFourId))
                    {
                        dic[NamePrefix + "OptionalProduct3"] = "True";
                    }
                    if (Convert.ToInt32(p.Products[0]).Equals(ProductAgentBranding.OptionalProductFiveId))
                    {
                        dic[NamePrefix + "OptionalProduct4"] = "True";
                    }
                    if (Convert.ToInt32(p.Products[0]).Equals(ProductAgentBranding.OptionalProductSixId))
                    {
                        dic[NamePrefix + "OptionalProduct5"] = "True";
                    }
                }
            }

            //foreach (var e in UP.Endorsements)
            //{
            //    dic.Add(NamePrefix + "QuoteEndorsement" + i, "True");
            //    i = i+1;
            //}
            //for (k = i; k < j ; k++)
            //{
            //    dic.Add(NamePrefix + "QuoteEndorsement" + k, "False");
            //}
        }

        public void TravellerUpdate (ref Dictionary<string, string> dic, AutoRenewalModel UP)
        {
            switch (UP.TotalTravellers)
            {
                case 1:
                    dic.Add("getqpQuoteAdultOneCount", "1");
                    dic.Add("getqpQuoteAdultTwoCount", "0");
                    dic.Add("getqpQuoteAdultThreeCount", "0");
                    dic.Add("getqpQuoteAdultFourCount", "0");
                    dic.Add("getqpQuoteAdultFiveCount", "0");
                    dic.Add("getqpQuoteAdultSixCount", "0");
                    dic.Add("getqpQuoteAdultSevenCount", "0");
                    break;
                case 2:
                    dic.Add("getqpQuoteAdultOneCount", "1");
                    dic.Add("getqpQuoteAdultTwoCount", "1");
                    dic.Add("getqpQuoteAdultThreeCount", "0");
                    dic.Add("getqpQuoteAdultFourCount", "0");
                    dic.Add("getqpQuoteAdultFiveCount", "0");
                    dic.Add("getqpQuoteAdultSixCount", "0");
                    dic.Add("getqpQuoteAdultSevenCount", "0");
                    break;
                case 3:
                    dic.Add("getqpQuoteAdultOneCount", "1");
                    dic.Add("getqpQuoteAdultTwoCount", "1");
                    dic.Add("getqpQuoteAdultThreeCount", "1");
                    dic.Add("getqpQuoteAdultFourCount", "0");
                    dic.Add("getqpQuoteAdultFiveCount", "0");
                    dic.Add("getqpQuoteAdultSixCount", "0");
                    dic.Add("getqpQuoteAdultSevenCount", "0");
                    break;
                case 4:
                    dic.Add("getqpQuoteAdultOneCount", "1");
                    dic.Add("getqpQuoteAdultTwoCount", "1");
                    dic.Add("getqpQuoteAdultThreeCount", "1");
                    dic.Add("getqpQuoteAdultFourCount", "1");
                    dic.Add("getqpQuoteAdultFiveCount", "0");
                    dic.Add("getqpQuoteAdultSixCount", "0");
                    dic.Add("getqpQuoteAdultSevenCount", "0");
                    break;
                case 5:
                    dic.Add("getqpQuoteAdultOneCount", "1");
                    dic.Add("getqpQuoteAdultTwoCount", "1");
                    dic.Add("getqpQuoteAdultThreeCount", "1");
                    dic.Add("getqpQuoteAdultFourCount", "1");
                    dic.Add("getqpQuoteAdultFiveCount", "1");
                    dic.Add("getqpQuoteAdultSixCount", "0");
                    dic.Add("getqpQuoteAdultSevenCount", "0");
                    break;
                case 6:
                    dic.Add("getqpQuoteAdultOneCount", "1");
                    dic.Add("getqpQuoteAdultTwoCount", "1");
                    dic.Add("getqpQuoteAdultThreeCount", "1");
                    dic.Add("getqpQuoteAdultFourCount", "1");
                    dic.Add("getqpQuoteAdultFiveCount", "1");
                    dic.Add("getqpQuoteAdultSixCount", "1");
                    dic.Add("getqpQuoteAdultSevenCount", "0");
                    break;
                case 7:
                    dic.Add("getqpQuoteAdultOneCount", "1");
                    dic.Add("getqpQuoteAdultTwoCount", "1");
                    dic.Add("getqpQuoteAdultThreeCount", "1");
                    dic.Add("getqpQuoteAdultFourCount", "1");
                    dic.Add("getqpQuoteAdultFiveCount", "1");
                    dic.Add("getqpQuoteAdultSixCount", "1");
                    dic.Add("getqpQuoteAdultSevenCount", "1");
                    break;
                default:
                    dic.Add("getqpQuoteAdultOneCount", "0");
                    dic.Add("getqpQuoteAdultTwoCount", "0");
                    dic.Add("getqpQuoteAdultThreeCount", "0");
                    dic.Add("getqpQuoteAdultFourCount", "0");
                    dic.Add("getqpQuoteAdultFiveCount", "0");
                    dic.Add("getqpQuoteAdultSixCount", "0");
                    dic.Add("getqpQuoteAdultSevenCount", "0");
                    break;
            }
        }

        public void ChildUpdate (ref Dictionary<string, string> dic, AutoRenewalModel UP)
        {
            if (UP.Travellers.Any(a => a.Age > 2 && a.Age < 10))
            {
                dic.Add("getqpQuoteChildCount", "1");
            }
            else
            {
                dic.Add("getqpQuoteChildCount", "0");
            }
            if (UP.Travellers.Any(a => a.Age < 1))
            {
                dic.Add("getqpQuoteInfantCount", "1");
            }
            else
            {
                dic.Add("getqpQuoteInfantCount", "0");
            }
        }

        public static class SerializationExtensions
        {
            private static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            public static string ToJson(object obj)
            {
                if (obj == null)
                    return null;
                var result = JsonConvert.SerializeObject(obj, Formatting.None, Settings);
                return result;
            }
        }
    }
}

