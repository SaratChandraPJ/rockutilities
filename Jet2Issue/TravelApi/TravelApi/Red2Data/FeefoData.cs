﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Reflection;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace TravelApi.Red2Data
{
    public class FeefoDataProvider
    {
        Red2DataContext dbContext = DataContextManager.GetRed2DataContext();

        public FeefoDataProvider() { }

        public void getFeefoData()
        {
            var data = dbContext.GetTable<Policy>().Where(a => (a.DateCreated.Date == DateTime.Today.AddDays(-1).Date) &&// && a.DateCreated.Date <= DateTime.Today.AddDays(-1).Date) && //a.DateCreated.Date <= Convert.ToDateTime(DateTime.Now.Date.AddDays(-1)).Date) &&
                 a.IsPaid == true &&
                 a.PolicyHolders.Single(c => c.PolicyHolderId == a.PolicyHolderId).FirstName != "test" &&

                 a.PolicyHolders.Single(c => c.PolicyHolderId == a.PolicyHolderId).LastName != "test" &&
                 a.IsCancelled == false &&
                 (a.Agents.Any(b => b.AgentName.StartsWith("insurefor"))) &&
                 a.Quotes.Single(f => f.QuoteId == a.QuoteId).Reference != "autorenew" &&
                 (!a.PolicyNumber.Contains("GAD")));

            var lFeefodata = new List<FeefoData>();
            foreach (var c in data)
            {
                try
                {
                    if (!IsValidEmail(c.PolicyHolders.Single(e => e.PolicyHolderId == c.PolicyHolderId).EmailAddress.Trim()))
                        continue;
                    lFeefodata.Add(new FeefoData()
                        {
                            Amount = Convert.ToString(c.PricePaid),
                            //  Category = c.Quotes.Single(a => a.QuoteId == c.QuoteId).TripTypeId == 1 ? "SingleTrip" : c.Quotes.Single(a => a.QuoteId == c.QuoteId).TripTypeId == 2 ? "Annual" : "LongStay",
                            Category = c.Quotes.Single(a => a.QuoteId == c.QuoteId).UserLoginId == 0 ? "Online" : "Phone/" + dbContext.GetTable<UserLoginTravel>().Single(a => a.UserLoginId == c.Quotes.Single(b => b.QuoteId == b.QuoteId).UserLoginId).Username,
                            CustomerRef = Convert.ToString(c.PolicyId),
                            Date = c.DateCreated.ToString("dd/MM/yyyy"),
                            Description = c.Quotes.Single(a => a.QuoteId == c.QuoteId).TripTypeId == 1 ? "Insurefor.com Single Trip travel insurance" : c.Quotes.Single(a => a.QuoteId == c.QuoteId).TripTypeId == 2 ? "insurefor.com Annual travel insurance" : c.Quotes.Single(a => a.QuoteId == c.QuoteId).TripTypeId == 3 ? "insurefor.com Single Trip travel insurance" : "N/A",
                            Email = c.PolicyHolders.Single(e => e.PolicyHolderId == c.PolicyHolderId).EmailAddress.Trim(),
                            Feedbackdate = "",
                            Logon = "www.rockinsurance.com/insurefor",
                            Name = c.PolicyHolders.Single(e => e.PolicyHolderId == c.PolicyHolderId).Title.Trim() + " " + c.PolicyHolders.Single(e => e.PolicyHolderId == c.PolicyHolderId).FirstName.Trim() + " " + c.PolicyHolders.Single(e => e.PolicyHolderId == c.PolicyHolderId).LastName.Trim(),
                            OrderRef = c.PolicyNumber.Trim(),
                            ProductLink = c.Quotes.Single(a => a.QuoteId == c.QuoteId).TripTypeId == 1 ? "http://www.insurefor.com/travel-insurance/Single-trip-travel-insurance" : c.Quotes.Single(a => a.QuoteId == c.QuoteId).TripTypeId == 2 ? "http://www.insurefor.com/travel-insurance/annual-travel-insurance" : c.Quotes.Single(a => a.QuoteId == c.QuoteId).TripTypeId == 3 ? "http://www.insurefor.com/travel-insurance/Single-trip-travel-insurance" : "N/A",
                            ProductSearchCode = c.AgentId.ToString()//c.Quotes.Single(a => a.QuoteId == c.QuoteId).TripTypeId == 1 ? "SingleTrip" : c.Quotes.Single(a => a.QuoteId == c.QuoteId).TripTypeId == 2 ? "Annual" : "LongStay",
                        });
                }
                catch (Exception ex)
                {
                    continue;
                }
            }

            SendFile(WriteTxt(lFeefodata));
        }

        public string WriteTxt(List<FeefoData> feefoData)
        {
            var builder = new StringBuilder();
            string header = "Name\t" +
                             "Email\t" +
                             "Date\t" +
                             "Description\t" +
                             "Logon\t" +
                             "Category\t" +
                             "Feedback Date\t" +
                             "Product search code\t" +
                             "Order Ref\t" +
                             "Product link\t" +
                             "Customer Ref\t" +
                             "Amount\t\r\n";

            builder.Append(header);

            foreach (var c in feefoData)
            {
                string s = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t\r\n", c.Name.Trim(), c.Email.Trim(), c.Date.Trim(), c.Description.Trim(), c.Logon.Trim(), c.Category.Trim(), c.Feedbackdate.Trim(), c.ProductSearchCode.Trim(), c.OrderRef.Trim(), c.ProductLink.Trim(), c.CustomerRef.Trim(), c.Amount.Trim());
                builder.Append(s);
            }
            return builder.ToString();
        }

        public void SendFile(string sourceStream)
        {
            // Get the object used to communicate with the server.
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://ftp.feefo.com/" + "Insurefor_" + DateTime.Now.Year + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Ticks + ".txt");
            request.Method = WebRequestMethods.Ftp.UploadFile;

            // This example assumes the FTP site uses anonymous logon.
            request.Credentials = new NetworkCredential("www-rockinsurance-com@feefo.com", "r0ckadm1n");

            MemoryStream memStr = new MemoryStream(UTF8Encoding.Default.GetBytes(sourceStream));

            Byte[] bytes = memStr.ToArray();
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();



            //   Gets the FtpWebResponse of the uploading operation
            //   var response = (FtpWebResponse)request.GetResponse();
            // Console.WriteLine(response.StatusDescription); //Display response
            //
            //          FtpWebResponse response = (FtpWebResponse)request.GetResponse();

            //  Console.WriteLine("Upload File Complete, status {0}", response.StatusDescription);

            // response.Close();
        }

        public bool IsValidEmail(string strIn)
        {
            if (String.IsNullOrEmpty(strIn))
                return false;



            // Return true if strIn is in valid e-mail format. 
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$");
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }

    public class FeefoData
    {
        public FeefoData() { }

        public string Name { get; set; }
        public string Email { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
        public string Logon { get; set; }
        public string Category { get; set; }
        public string Feedbackdate { get; set; }
        public string ProductSearchCode { get; set; }
        public string OrderRef { get; set; }
        public string ProductLink { get; set; }
        public string CustomerRef { get; set; }
        public string Amount { get; set; }
    }
}


