﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TravelApi.Red2Data
{
    public class PriceandAgeCaps
    {
        //RockEntitiesDataContext db = DataContextManager.getRockDataContext();
         RenewalDataContext db = DataContextManager.GetMirrorDataContext();

        //Step 1 : Fetch the condition by using agent id 
        //Step 2 : Check whether the condition is empty or not.if empty get the condition 
        public void GetPriceCaps(AutoRenewal policy,ref string List,ref string exception,ref bool endorsements,ref decimal TotalPrice, int Condition, int i, int NewAgentid)
        {
            string v = string.Empty;
            //string List = string.Empty;
            string expression = string.Empty;
            List<string> exp = new List<string>();
            bool listfill = false;
            AutorenewalPriceAgemapping u = null;
            try
            {
                //for recursive conditions if agent has to undergo more than one condition
                if (Condition > 0)
                {
                    if (NewAgentid == 0)
                    {
                        u = db.AutorenewalPriceAgemappings.Where(a => a.IsActive == true && a.NewAgentid == NewAgentid && a.AutorenewalPriceAgemappingId == Condition).FirstOrDefault();
                    }
                    else
                    {
                        u = db.AutorenewalPriceAgemappings.Where(a => a.IsActive == true && a.NewAgentid == policy.AgentId && a.AutorenewalPriceAgemappingId == Condition).FirstOrDefault();
                    }
                     
                }
                else
                {
                     u= db.AutorenewalPriceAgemappings.Where(a => a.IsActive == true && a.NewAgentid == policy.AgentId).FirstOrDefault();
                }
                if (u != null)
                {
                    expression = u.Condition;
                }
                #region for agents who are in the agent mapping table and conditions exits
                if (expression != null && u != null)
                {
                    var result = Eval(Convert.ToDecimal(policy.TotalPrice), Convert.ToDecimal(policy.OldPrice), Convert.ToDecimal(policy.NetToUnderwriterGadget), Convert.ToDecimal(policy.NetToUnderwriter), Convert.ToDecimal(policy.NetToUnderwriterCDW), expression, i);
                    if (Convert.ToBoolean(result))
                    {
                        //for recursive conditions if agent has to undergo more than one condition
                        if (u.TrueList == null && u.Condition2 != null)
                        {
                            GetPriceCaps(policy, ref List, ref exception, ref endorsements, ref TotalPrice, Convert.ToInt32(u.Condition2), i, Convert.ToInt32(policy.AgentId));
                        }
                        else
                        {
                            if (Convert.ToDecimal(result) > 1)
                            {
                                TotalPrice = Convert.ToDecimal(result);
                            }
                            List = u.TrueList;
                            if (u.Exception != null)
                                exception = u.Exception;
                            if (Convert.ToBoolean(u.Endorsements))
                            {
                                endorsements = Convert.ToBoolean(u.Endorsements);
                            }
                        }
                    }
                    else
                    {
                        //List = u.ErrorList;
                        //exception = u.Exception;
                        //if (Convert.ToBoolean(u.Endorsements))
                        if (u.ErrorList == null && u.Condition2 != null)
                        {
                            //endorsements = Convert.ToBoolean(u.Endorsements);
                            GetPriceCaps(policy, ref List, ref exception, ref endorsements, ref TotalPrice, Convert.ToInt32(u.Condition2), i, Convert.ToInt32(policy.AgentId));
                        }
                        else
                        {
                            List = u.ErrorList;
                            exception = u.Exception;
                            if (Convert.ToBoolean(u.Endorsements))
                            {
                                endorsements = Convert.ToBoolean(u.Endorsements);

                            }
                        }
                    }
                }
                #endregion
                #region for all other agents
                else
                {
                    //if no condition exists and yet they needs to assigned to specific Lists with out List number
                    //var truelist  = (from r in db.AutorenewalPriceAgemappings
                    //   where r.NewAgentid == policy.AgentId
                    //   select r.TrueList);
                    //List = truelist.FirstOrDefault();
                    if (u != null)
                        List = u.TrueList;
                    if (List == null)
                    {
                        var y = db.AutorenewalPriceAgemappings.Where(a => a.IsActive == true && a.NewAgentid == 0).Select(a => a.Condition);
                        exp = y.ToList();
                        if (exp.Count > 0)
                        {
                            foreach (var e in exp)
                            {
                                var result = Eval(Convert.ToDecimal(policy.TotalPrice), Convert.ToDecimal(policy.OldPrice), Convert.ToDecimal(policy.NetToUnderwriterGadget), Convert.ToDecimal(policy.NetToUnderwriter), Convert.ToDecimal(policy.NetToUnderwriterCDW), e, i);
                                if (Convert.ToBoolean(result))
                                {
                                    var temp = db.AutorenewalPriceAgemappings.Where(a => a.Condition == e && a.IsActive == true).FirstOrDefault();

                                    //for recursive conditions if agent has to undergo more than one condition
                                    if (temp.TrueList == null && temp.Condition2 != null)
                                    {
                                        GetPriceCaps(policy, ref List, ref exception, ref endorsements, ref TotalPrice, Convert.ToInt32(temp.Condition2), i, Convert.ToInt32(temp.NewAgentid));
                                        if (List != null)
                                        {
                                            listfill = true;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToDecimal(result) > 1)
                                        {
                                            TotalPrice = Convert.ToDecimal(result);
                                        }
                                        List = temp.TrueList;
                                        // if Total price needs to be updated with OldPrice or Any other Price 
                                        var totalpriceudpate = temp.TotalPrice;
                                        if (totalpriceudpate != null)
                                        {
                                            if (totalpriceudpate.Contains("OldPrice"))
                                                TotalPrice = Convert.ToDecimal(policy.OldPrice);
                                            if ((totalpriceudpate.Contains("TotalPrice")))
                                                TotalPrice = Convert.ToDecimal(policy.TotalPrice);
                                        }
                                        listfill = true;
                                        break;
                                    }
                                }

                            }
                            if (!listfill)
                            {
                                //Assigning Defualt List value if no condition matches the agent 
                                List = (from c in db.AutorenewalPriceAgemappings
                                        where c.NewAgentid == 0
                                        select c.ErrorList).FirstOrDefault();
                                if (List == "List1")
                                {
                                    exception = (from c in db.AutorenewalPriceAgemappings
                                                 where c.NewAgentid == 0
                                                 select c.Exception).FirstOrDefault();
                                }
                            }
                        }
                        else
                        {
                            //Assigning Defualt List value if no condition matches the agent 
                            List = (from c in db.AutorenewalPriceAgemappings
                                    where c.NewAgentid == 0
                                    select c.ErrorList).FirstOrDefault();
                            if (List == "List1")
                            {
                                exception = (from c in db.AutorenewalPriceAgemappings
                                             where c.NewAgentid == 0
                                             select c.Exception).FirstOrDefault();
                            }
                        }
                    }
                    else
                    {
                        if (List == "List1")
                        {
                            exception = u.Exception;
                        }
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                var s = new SendEmail();
                s.send("An Error Occcured in Price Cap Decision:" +ex.Message.ToString(),"Error While Creating Quote");
                //ErrorManager err = new ErrorManager(ex, "An error occurred whilst attempting to retrieve all Users.");
                //err.Store();
            }
        }

        public bool GetMaxAge(int agentId, string ProductId, cTraveller[] travellers)
        {
            string v = string.Empty;
            string List = string.Empty;
            bool result = false;
            try
            {
                var u = db.AutorenewalPriceAgemappings.Where(a=>a.NewAgentid == agentId && a.ProductId == Convert.ToInt32(ProductId) && a.IsActive == true);
                if (u.Count() == 0)
                {
                    var w = db.AutorenewalPriceAgemappings.Where(a=>a.NewAgentid == agentId && a.IsActive ==true);
                    if (w.Count() == 0)
                    {
                    }
                    else
                    {
                        foreach(var c in w)
                        {
                            if (travellers.Any(a => a.Age > c.MaxAge) && c.MaxAge >0)
                            {
                                result = true;
                            }
                        }
                        //foreach(cTraveller traveller in travellers)
                        //{
                        //    if(w.Any(a=> a.MaxAge > traveller.Age))
                        //    {
                        //        result = true;
                        //    }
                        //}
                        //if (travellers.Any(a => a.Age == w.FirstOrDefault().MaxAge))
                        //result =  true;
                        //rPolicy.IsAged64 = true;
                    }
                }
                else
                {
                    foreach (var c in u)
                    {
                        if (travellers.Any(a => a.Age > c.MaxAge) && c.MaxAge > 0)
                        {
                            result = true;
                        }
                    }
                    //foreach (cTraveller traveller in travellers)
                    //{
                    //    if (u.Any(a => a.MaxAge > traveller.Age))
                    //    {
                    //        result = true;
                    //    }
                    //}
                    //if (travellers.Any(a => a.Age == u.FirstOrDefault().MaxAge))
                    //    result = true;
                    //rPolicy.IsAged64 = true;
                }
                //rPolicy.Exception = "Can not create a policy";
                //rPolicy.QuoteDate = DateTime.Now;
                //rPolicy.QuoteActive = false;
                //rPolicy.Lists = "List1";
                //rProvider.Update(rPolicy);
                //return true;
            }
            catch (Exception ex)
            {
                var s = new SendEmail();
                s.send("An Error Occcured in Max Age Decision:" + ex.Message.ToString(), "Error While Creating Quote");
                //ErrorManager err = new ErrorManager(ex, "An error occurred whilst attempting to retrieve all Users.");
                //err.Store();
            }
            return result;
        }

        public object Eval(decimal TotalPrice, decimal OldPrice, decimal NetToUnderwriterGadget, decimal NetToUnderwriter, decimal NetToUnderwriterCDW, string exp, int i)
        {
            string expression = string.Empty;
            expression = exp;
            MSScriptControl.ScriptControl sc = new MSScriptControl.ScriptControl();
            sc.Language = "VBScript";
            if (expression.Contains("TotalPrice"))
            {
                expression = expression.Replace("TotalPrice", TotalPrice.ToString());
            }
            if (expression.Contains("OldPrice"))
            {
                expression = expression.Replace("OldPrice", OldPrice.ToString());
            }
            if (expression.Contains("NetToGadgetUnderwriter"))
            {
                expression = expression.Replace("NetToGadgetUnderwriter", NetToUnderwriterGadget.ToString());
            }
            if (expression.Contains("NetToUnderwriter"))
            {
                expression = expression.Replace("NetToUnderwriter", NetToUnderwriter.ToString());
            }
            if (expression.Contains("NetToCDWUnderwriter"))
            {
                expression = expression.Replace("NetToCDWUnderwriter", NetToUnderwriterCDW.ToString());
            }
            if (expression.Contains("i"))
            {
                expression = expression.Replace("i", i.ToString());
            }
            object obj = sc.Eval(expression);
            return obj;
            
        }
    }
}
