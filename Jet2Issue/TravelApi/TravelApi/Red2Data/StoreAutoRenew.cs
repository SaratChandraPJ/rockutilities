﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.ComponentModel;
using System.Reflection;

namespace TravelApi.Red2Data
{
    public class StoreAutoRenew
    {
        public StoreAutoRenew() { }
        RenewalDataContext db = DataContextManager.GetMirrorDataContext();

        //public void Store()
        //{
        //    var d = from cc in db.AutoRenewals.AsEnumerable()
        //            where cc.QuoteDate.Value.Date == DateTime.Now.Date

        //            select new
        //            {
        //                AgentId = cc.AgentId,
        //                AgentName = cc.AgentName,
        //                pStartDate = cc.PolicyStartDate,
        //                pEndDate = cc.PolicyEndDate,
        //                TravelPrice = cc.TravelPrice ?? 0,
        //                GadgetPrice = cc.GadgetPrice ?? 0,
        //                NetToUnderwriter = cc.NetToUnderwriter ?? 0,
        //                NetToUnderwriterGadget = cc.NetToUnderwriterGadget ?? 0,
        //                TotalPrice = cc.TotalPrice ?? 0,
        //                Lists = cc.Lists ?? "--"
        //            };

        //    var dt = Dt.ToDataTable(d.ToList());
        //    var ds = new DataSet();
        //    ds.Tables.Add(dt);

        //    ExcelLibrary.DataSetHelper.CreateWorkbook(@"\\serv02\\IT\\AutoRenew\\GetQuote[" + DateTime.Now.ToString("dd-MM-yyyy-hh-MM-ss") + "].xls", ds);

        //}
    }

    public static class Dt
    {
        public static DataTable ToDataTable<T>(this IEnumerable<T> collection)
        {
            DataTable dt = new DataTable("DataTable");
            Type t = typeof(T);
            PropertyInfo[] pia = t.GetProperties();

            //Inspect the properties and create the columns in the DataTable
            foreach (PropertyInfo pi in pia)
            {
                Type ColumnType = pi.PropertyType;
                if ((ColumnType.IsGenericType))
                {
                    ColumnType = ColumnType.GetGenericArguments()[0];
                }
                dt.Columns.Add(pi.Name, ColumnType);
            }

            //Populate the data table
            foreach (T item in collection)
            {
                DataRow dr = dt.NewRow();
                dr.BeginEdit();
                foreach (PropertyInfo pi in pia)
                {
                    if (pi.GetValue(item, null) != null)
                    {
                        dr[pi.Name] = pi.GetValue(item, null);
                    }
                }
                dr.EndEdit();
                dt.Rows.Add(dr);
            }
            return dt;
        }
    }
}
