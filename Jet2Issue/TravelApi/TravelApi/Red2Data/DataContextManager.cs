﻿using System.Configuration;
using TravelApi.Red2Data;
namespace TravelApi.Red2Data
{
    public class DataContextManager
    {
        public static RenewalDataContext GetMirrorDataContext()
        {
            string conn = ConfigurationManager.ConnectionStrings["MirrorConnectionString"].ToString();

            RenewalDataContext datacontext = new RenewalDataContext(conn);

            //datacontext.CommandTimeout = 180;
            return datacontext;
        }

        public static Red2DataContext GetRed2DataContext()
        {
            string conn = ConfigurationManager.ConnectionStrings["RockLiveReportConnectionString"].ToString();

            Red2DataContext datacontext = new Red2DataContext(conn);

            return datacontext;
        }

        public static UnsubscribeDataContext GetAutotDataContext()
        {
            string conn = ConfigurationManager.ConnectionStrings["AutoRenewConnectionString"].ToString();

            UnsubscribeDataContext datacontext = new UnsubscribeDataContext(conn);

            return datacontext;
        }


        public static GadgetDataDataContext GetGadgetDataContext()
        {
            string conn = ConfigurationManager.ConnectionStrings["GadgetBuddyConnectionString"].ToString();

            GadgetDataDataContext datacontext = new GadgetDataDataContext(conn);

            return datacontext;
        }

        public static MSDataContext GetMSDataContext()
        {
            string conn = ConfigurationManager.ConnectionStrings["MSConnectionString"].ToString();

            MSDataContext datacontext = new MSDataContext(conn);

            return datacontext;
        }

    }
}
