﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;

namespace TravelApi.Red2Data
{

    public class FeefoGadgetProvider
    {
        GadgetDataDataContext dbContext = DataContextManager.GetGadgetDataContext();

        public FeefoGadgetProvider() { }

        public void getFeefoData()
        {
            var data = dbContext.PolicyGadgets.Where(a => a.IsPaid == true && (a.IsCancelled == false || a.IsCancelled == null) &&
                 (a.IsAmended == false || a.IsAmended == null) &&
                   a.PolicyHolderGadget.EmailAddress != "stephen@gilkes.me.uk" &&
                 a.PolicyHolderGadget.FirstName != "test" &&
                 a.PolicyHolderGadget.LastName != "test"
                 &&
                 (a.DateCreated.Date == DateTime.Now.AddDays(-1).Date)
                 && (a.PolicyNumber != null));

            var lFeefodata = new List<FeefoData>();
            foreach (var c in data)
            {
                lFeefodata.Add(new FeefoData()
                {
                    Amount = Convert.ToString(c.PricePaid),
                    Category = "Gadget",
                    CustomerRef = Convert.ToString(c.PolicyId),
                    Date = c.DateCreated.ToString("dd/MM/yyyy"),
                    Description = "gadgetbuddy.com Gadget Insurance",
                    Email = c.PolicyHolderGadget.EmailAddress.Trim(),
                    Feedbackdate = "",
                    Logon = "www.rockinsurance.com/mygadgetbuddy",
                    Name = c.PolicyHolderGadget.Title.Trim() + " " + c.PolicyHolderGadget.FirstName.Trim() + " " + c.PolicyHolderGadget.LastName.Trim(),
                    OrderRef = c.PolicyNumber.Trim(),
                    ProductLink = "https://www.gadgetbuddy.com",
                    ProductSearchCode = Convert.ToString(c.QuoteId)
                });
            }

            SendFile(WriteTxt(lFeefodata));
        }

        public string WriteTxt(List<FeefoData> feefoData)
        {
            var builder = new StringBuilder();
            //  StreamWriter outputFile = new StreamWriter(@"c:\Feefo\MyGadgetBuddy_" + DateTime.Now.Year + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Ticks + ".txt");

            string header = "Name\t" +
                             "Email\t" +
                             "Date\t" +
                             "Description\t" +
                             "Logon\t" +
                             "Category\t" +
                             "Feedback Date\t" +
                             "Product search code\t" +
                             "Order Ref\t" +
                             "Product link\t" +
                             "Customer Ref\t" +
                             "Amount\t\r\n";

            builder.Append(header);

            foreach (var c in feefoData)
            {
                string s = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t\r\n", c.Name.Trim(), c.Email.Trim(), c.Date.Trim(), c.Description.Trim(), c.Logon.Trim(), c.Category.Trim(), c.Feedbackdate.Trim(), c.ProductSearchCode.Trim(), c.OrderRef.Trim(), c.ProductLink.Trim(), c.CustomerRef.Trim(), c.Amount.Trim());
                builder.Append(s);
            }
            //  outputFile.WriteLine(builder);
            // outputFile.Close();
            return builder.ToString();

        }

        public void SendFile(string sourceStream)
        {
            // Get the object used to communicate with the server.
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://ftp.feefo.com/" + "GadgetBuddy" + DateTime.Now.Year + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Ticks + ".txt");
            request.Method = WebRequestMethods.Ftp.UploadFile;

            // This example assumes the FTP site uses anonymous logon.
            request.Credentials = new NetworkCredential("www-rockinsurance-com", "r0ckadm1n");

            MemoryStream memStr = new MemoryStream(UTF8Encoding.Default.GetBytes(sourceStream));

            Byte[] bytes = memStr.ToArray();
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();



            //   Gets the FtpWebResponse of the uploading operation
            //   var response = (FtpWebResponse)request.GetResponse();
            // Console.WriteLine(response.StatusDescription); //Display response
            //
            //          FtpWebResponse response = (FtpWebResponse)request.GetResponse();

            //  Console.WriteLine("Upload File Complete, status {0}", response.StatusDescription);

            // response.Close();
        }
    }
}
