﻿using System.Xml.Serialization;
using System.IO;
using System.Text;

namespace TravelApi
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.insurefor.com/ws2/0")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.insurefor.com/ws2/0", IsNullable = false)]
    public partial class BuyPolicyResponse
    {

        private cOperationResult operationResultField;

        private cPrice priceField;

        private string policyHolderIdField;

        private string policyIdField;

        private string policyNumberField;

        /// <remarks/>
        public cOperationResult OperationResult
        {
            get
            {
                return this.operationResultField;
            }
            set
            {
                this.operationResultField = value;
            }
        }

        /// <remarks/>
        public cPrice Price
        {
            get
            {
                return this.priceField;
            }
            set
            {
                this.priceField = value;
            }
        }

        /// <remarks/>
        public string PolicyHolderId
        {
            get
            {
                return this.policyHolderIdField;
            }
            set
            {
                this.policyHolderIdField = value;
            }
        }

        /// <remarks/>
        public string PolicyId
        {
            get
            {
                return this.policyIdField;
            }
            set
            {
                this.policyIdField = value;
            }
        }

        /// <remarks/>
        public string PolicyNumber
        {
            get
            {
                return this.policyNumberField;
            }
            set
            {
                this.policyNumberField = value;
            }
        }

        public BuyPolicyResponse GetResponse(string xmlResponse)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(BuyPolicyResponse));
            StringReader responseReader = new StringReader(xmlResponse);
            GetQuoteResponse quoteResponse = new GetQuoteResponse();
            return (BuyPolicyResponse)serializer.Deserialize(responseReader);
        }
    
    }

}