﻿using System;
using System.Net;
using System.Net.Mail;

namespace TravelApi
{
    public class AutoEmailer
    {
        public string From = string.Empty;
        public string To = string.Empty;
        public string Subject = string.Empty;
        public string Body = string.Empty;
        public string CC = string.Empty;
        public bool IsHtml = false;


        public AutoEmailer() { }

        public void SendEmail()
        {
            // new Thread(new ThreadStart(SendMessage)).Start();
            SendMessage();
        }
        /// <summary>
        /// Send Email Message method.
        /// </summary>
        private void SendMessage()
        {
           // Pop3Client email = new Pop3Client("Chandra.Chandrabalan", "chandra1978", "EXCH-01.rock.local");
            try
            {
                MailMessage oMessage = new MailMessage();
                SmtpClient smtpClient = new SmtpClient();

                oMessage.From = new System.Net.Mail.MailAddress(From);
                oMessage.To.Add(To);
                oMessage.Subject = Subject;
                oMessage.IsBodyHtml = IsHtml;
                oMessage.Body = Body;

                if (CC != string.Empty)
                    oMessage.CC.Add(CC);

                try
                {
                    // Deliver the message    
                    smtpClient.Send(oMessage);
                }

                catch (Exception ex)
                {
                    ex.ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

    }
}