﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TravelApi
{
    [System.Serializable()]
    public class cPolicyHolderData
    {

        private string titleField;

        private string firstNameField;

        private string lastNameField;

        private System.DateTime dateOfBirthField;

        private bool dateOfBirthFieldSpecified;

        private int ageField;

        private bool ageFieldSpecified;

        private string address1Field;

        private string address2Field;

        private string address3Field;

        private string address4Field;

        private string postcodeField;

        private string countryField;

        private string phoneDaytimeField;

        private string phoneEveningField;

        private string emailField;

        private bool wantsEmailUpdatesField;

        private bool wantsEmailUpdatesFieldSpecified;

        /// <remarks/>
        public string Title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string LastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DateOfBirthSpecified
        {
            get
            {
                return this.dateOfBirthFieldSpecified;
            }
            set
            {
                this.dateOfBirthFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Age
        {
            get
            {
                return this.ageField;
            }
            set
            {
                this.ageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AgeSpecified
        {
            get
            {
                return this.ageFieldSpecified;
            }
            set
            {
                this.ageFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Address1
        {
            get
            {
                return this.address1Field;
            }
            set
            {
                this.address1Field = value;
            }
        }

        /// <remarks/>
        public string Address2
        {
            get
            {
                return this.address2Field;
            }
            set
            {
                this.address2Field = value;
            }
        }

        /// <remarks/>
        public string Address3
        {
            get
            {
                return this.address3Field;
            }
            set
            {
                this.address3Field = value;
            }
        }

        /// <remarks/>
        public string Address4
        {
            get
            {
                return this.address4Field;
            }
            set
            {
                this.address4Field = value;
            }
        }

        /// <remarks/>
        public string Postcode
        {
            get
            {
                return this.postcodeField;
            }
            set
            {
                this.postcodeField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public string PhoneDaytime
        {
            get
            {
                return this.phoneDaytimeField;
            }
            set
            {
                this.phoneDaytimeField = value;
            }
        }

        /// <remarks/>
        public string PhoneEvening
        {
            get
            {
                return this.phoneEveningField;
            }
            set
            {
                this.phoneEveningField = value;
            }
        }

        /// <remarks/>
        public string Email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        public bool WantsEmailUpdates
        {
            get
            {
                return this.wantsEmailUpdatesField;
            }
            set
            {
                this.wantsEmailUpdatesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool WantsEmailUpdatesSpecified
        {
            get
            {
                return this.wantsEmailUpdatesFieldSpecified;
            }
            set
            {
                this.wantsEmailUpdatesFieldSpecified = value;
            }
        }
    }
}
