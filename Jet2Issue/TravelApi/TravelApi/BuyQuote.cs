﻿using System.Xml.Serialization;
using System.IO;
using System.Text;

namespace TravelApi
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.insurefor.com/ws2/0")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.insurefor.com/ws2/0", IsNullable = false)]
    public partial class BuyQuote
    {

        private cLoginData loginDataField;

        private string policyHolderIdField;

        private string quoteIdField;

        private bool agreeTermsField;

        private bool agreePolicyField;

        private bool agreeDisclosureField;

        private bool wantsEmailCertificateField;

        private cSurcharge[] surchargesField;

        private cTraveller[] travellersField;

        private decimal paymentTotalPriceField;

        private bool paymentOutstandingField;
        
        private string paymentReferenceField;

        private cPolicy[] linkedPoliciesField;

        /// <remarks/>
        public cLoginData LoginData
        {
            get
            {
                return this.loginDataField;
            }
            set
            {
                this.loginDataField = value;
            }
        }

        /// <remarks/>
        public string PolicyHolderId
        {
            get
            {
                return this.policyHolderIdField;
            }
            set
            {
                this.policyHolderIdField = value;
            }
        }

        /// <remarks/>
        public string QuoteId
        {
            get
            {
                return this.quoteIdField;
            }
            set
            {
                this.quoteIdField = value;
            }
        }

        /// <remarks/>
        public bool AgreeTerms
        {
            get
            {
                return this.agreeTermsField;
            }
            set
            {
                this.agreeTermsField = value;
            }
        }


        /// <remarks/>
        public bool AgreePolicy
        {
            get
            {
                return this.agreePolicyField;
            }
            set
            {
                this.agreePolicyField = value;
            }
        }

        /// <remarks/>
        public bool AgreeDisclosure
        {
            get
            {
                return this.agreeDisclosureField;
            }
            set
            {
                this.agreeDisclosureField = value;
            }
        }

        /// <remarks/>
        public bool WantsEmailCertificate
        {
            get
            {
                return this.wantsEmailCertificateField;
            }
            set
            {
                this.wantsEmailCertificateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Surcharge", IsNullable = false)]
        public cSurcharge[] Surcharges
        {
            get
            {
                return this.surchargesField;
            }
            set
            {
                this.surchargesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Traveller", IsNullable = false)]
        public cTraveller[] Travellers
        {
            get
            {
                return this.travellersField;
            }
            set
            {
                this.travellersField = value;
            }
        }

        /// <remarks/>
        public decimal PaymentTotalPrice
        {
            get
            {
                return this.paymentTotalPriceField;
            }
            set
            {
                this.paymentTotalPriceField = value;
            }
        }
       
        /// <remarks/>
        public bool PaymentOutstanding
        {
            get
            {
                return this.paymentOutstandingField;
            }
            set
            {
                this.paymentOutstandingField = value;
            }
        }

        /// <remarks/>
        public string PaymentReference
        {
            get
            {
                return this.paymentReferenceField;
            }
            set
            {
                this.paymentReferenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Policy", IsNullable = false)]
        public cPolicy[] LinkedPolicies
        {
            get
            {
                return this.linkedPoliciesField;
            }
            set
            {
                this.linkedPoliciesField = value;
            }
        }


        [XmlIgnore()]
        public string Xml
        {
            get
            {
                // Prepare XML serializer
                XmlSerializer serializer = new XmlSerializer(typeof(BuyQuote));

                // Serialize into Stringbuilder
                StringBuilder bodyBuilder = new StringBuilder();
                StringWriter bodyWriter = new StringWriter(bodyBuilder);
                serializer.Serialize(bodyWriter, this);
                // Replace UTF-16 encoding with UTF-8 encoding
                string body = bodyBuilder.ToString().Replace("utf-16", "utf-8");
                return body;
            }
        }
    }
}