﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TravelApi
{
    public class cPolicy
    {

        private int policyIdField;

        private bool policyIdFieldSpecified;

        private string policyNumberField;

        /// <remarks/>
        public int PolicyId
        {
            get
            {
                return this.policyIdField;
            }
            set
            {
                this.policyIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PolicyIdSpecified
        {
            get
            {
                return this.policyIdFieldSpecified;
            }
            set
            {
                this.policyIdFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string PolicyNumber
        {
            get
            {
                return this.policyNumberField;
            }
            set
            {
                this.policyNumberField = value;
            }
        }
    }
}
