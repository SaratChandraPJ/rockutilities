﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TravelApi
{
    public class Person
    {

        private string _title;
        private string _firstName;
        private string _lastName;
        private MailAddress _mailaddress;
        private string _telephone;
        private string _mobile;
        private string _emailAddress;
        private System.DateTime _dateofBirth;
        private string _occupation;
        private string _maritalStatus;
        private int _age;
        private string _status;
        private int _id;
        private bool _student;
        private bool _mailmarketing;

        public string Title
        {
            get { return _title.Trim(); }
            set { _title = value.Trim(); }
        }

        public string FirstName
        {
            get { return _firstName.Trim(); }
            set
            {
                if (value != null)
                {
                    _firstName = value.Trim();
                }
            }
        }

        public string Lastname
        {
            get { return _lastName.Trim(); }
            set
            {
                if (value != null)
                {
                    _lastName = value.Trim();
                }
            }
        }

        public MailAddress MailAddress
        {
            get { return _mailaddress; }
            set
            {
                if (value != null)
                {
                    _mailaddress = value;
                }
            }
        }

        public string Telephone
        {
            get { return _telephone.Trim(); }
            set
            {
                if (value != null)
                {
                    _telephone = value.Trim();
                }
            }
        }

        public string Mobile
        {
            get { return _mobile.Trim(); }
            set
            {
                if (value != null)
                {
                    _mobile = value.Trim();
                }
            }
        }

        public string EmailAddress
        {
            get { return _emailAddress.Trim(); }
            set
            {
                if (value != null)
                {
                    _emailAddress = value.Trim();
                }
            }
        }

        public string Occupation
        {
            get { return _occupation; }
            set
            {
                if (value != null)
                {
                    _occupation = value.Trim();
                }
            }
        }

        public string MaritalStatus
        {
            get { return _maritalStatus; }
            set
            {
                if (value != null)
                {
                    _maritalStatus = value.Trim();
                }
            }
        }

        public bool Student
        {
            get { return _student; }
            set { _student = value; }
        }

        public System.DateTime DateofBirth
        {
            get { return _dateofBirth; }
            set { _dateofBirth = value; }
        }

        public bool MailMarketing
        {
            get { return _mailmarketing; }
            set { _mailmarketing = value; }
        }

        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public Person()
        {

            _title = "";
            _firstName = "";
            _lastName = "";
            _mailaddress = new MailAddress();
            _telephone = "";
            _mobile = "";
            _emailAddress = "";
            _dateofBirth = System.DateTime.MinValue;
            _occupation = "";
            _maritalStatus = "";
            _age = 0;
            _status = "";

            _mailmarketing = false;
        }

    }

    [Serializable()]
    public class MailAddress
    {

        private int _addressId;
        private string _address1;
        private string _address2;
        private string _address3;
        private string _town;
        private string _county;
        private string _postcode;
        private string _country;

        public int ID
        {
            get { return _addressId; }
            set { _addressId = value; }
        }

        public string Address1
        {
            get { return _address1.Trim(); }
            set
            {
                if (value != null)
                {
                    _address1 = value.Trim();
                }
            }
        }

        public string Address2
        {
            get { return _address2.Trim(); }
            set
            {
                if (value != null)
                {
                    _address2 = value.Trim();
                }
            }
        }

        public string Address3
        {
            get { return _address3.Trim(); }
            set
            {
                if (value != null)
                {
                    _address3 = value.Trim();
                }
            }
        }

        public string City
        {
            get { return _town.Trim(); }
            set
            {
                if (value != null)
                {
                    _town = value.Trim();
                }
            }
        }

        public string Region
        {
            get { return _county.Trim(); }
            set
            {
                if (value != null)
                {
                    _county = value.Trim();
                }
            }
        }

        public string PostalCode
        {
            get { return _postcode.Trim(); }
            set
            {
                if (value != null)
                {
                    _postcode = value.Trim();
                }
            }
        }

        public string Country
        {
            get { return _country.Trim(); }
            set
            {
                if (value != null)
                {
                    _country = value.Trim();
                }
            }
        }

        public MailAddress()
        {
            _address1 = "";
            _address2 = "";
            _address3 = "";
            _town = "";
            _county = "";
            _postcode = "";
            _country = "";
        }

    }

    [Serializable()]
    public class PersonCollection : System.Collections.CollectionBase
    {

        public Person this[int index]
        {
            get
            {
                return ((Person)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }
        //Item(index)

        public int Add(Person value)
        {
            return List.Add(value);
        }
        //Add

        public int IndexOf(Person value)
        {
            return List.IndexOf(value);
        }
        //IndexOf

        public void Insert(int index, Person value)
        {
            List.Insert(index, value);
        }
        //Insert

        public void Remove(Person value)
        {
            List.Remove(value);
        }
        //Remove

        public new int Count()
        {
            return List.Count;
        }
        //Contains

        public bool Contains(Person value)
        {
            // If value is not of type Person, this will return false.
            return List.Contains(value);
        }
        //Contains

        public int MaximumAge()
        {

            // Get the maximum age of the people in the collection
            int maxAge = int.MinValue;
            foreach (Person p in this.List)
            {
                if (p.Age == 0)
                {
                    p.Age = AgeFromDateOfBirth(p.DateofBirth, DateTime.Now);
                }
                if (p.Age > maxAge) maxAge = p.Age;
            }

            return maxAge;
        }
        //MaximumAge

        public int GetQuantityWithinAgeBrackets(int minAge, int maxAge, System.DateTime policyStartDate)
        {

            int quantity = 0;

            foreach (Person p in this.List)
            {
                p.Age = AgeFromDateOfBirth(p.DateofBirth, policyStartDate);
                if (p.Age >= minAge & p.Age <= maxAge)
                {
                    quantity = quantity + 1;
                }
            }


            return quantity;
        }
        // GetQuantityWithinAgeBrackets

        public Person GetOldestPerson()
        {

            Person oldestperson = new Person();
            foreach (Person p in this.List)
            {
                if (p.Age == 0)
                {
                    p.Age = AgeFromDateOfBirth(p.DateofBirth, DateTime.Now);
                }
                if (p.Age > oldestperson.Age) oldestperson = p;
            }

            return oldestperson;
        }
        //GetOldestPerson

        public Person GetYoungestPerson()
        {

            Person youngestPerson = new Person();
            youngestPerson.Age = int.MaxValue;
            foreach (Person p in this.List)
            {
                if (p.Age == 0)
                {
                    p.Age = AgeFromDateOfBirth(p.DateofBirth, DateTime.Now);
                }
                if (p.Age < youngestPerson.Age) youngestPerson = p;
            }

            return youngestPerson;
        }
        //GetYoungestPerson

        public void SortByAge()
        {
            System.Collections.IComparer sorter = new AgeSortHelper();
            InnerList.Sort(sorter);
        }

        private class AgeSortHelper : System.Collections.IComparer
        {

            public int Compare(object x, object y)
            {

                Person p1 = (Person)x;

                Person p2 = (Person)y;

                if (p1.Age == int.MinValue | p1.Age == 0)
                {
                    p1.Age = AgeFromDateOfBirth(p1.DateofBirth, DateTime.Now);
                }

                if (p2.Age == int.MinValue | p2.Age == 0)
                {
                    p2.Age = AgeFromDateOfBirth(p2.DateofBirth, DateTime.Now);
                }

                if (p1.Age > p2.Age)
                {
                    return 1;
                }

                if (p1.Age < p2.Age)
                {
                    return -1;
                }


                return 0;
            }

            private int AgeFromDateOfBirth(DateTime birthday, DateTime referenceDate)
            {
                DateTime now = DateTime.Today;
                int age = now.Year - birthday.Year;
                if (birthday > now.AddYears(-age)) age--;
                return age;
            }

        }


        private int AgeFromDateOfBirth(DateTime birthday, DateTime referenceDate)
        {
            DateTime now = DateTime.Today;
            int age = now.Year - birthday.Year;
            if (birthday > now.AddYears(-age)) age--;
            return age;
        }
    }
}