﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Xml.Serialization;
using System.Xml;
using System.Configuration;

namespace TravelApi
{
    public class ApiCall
    {
        public int UserID { get { return _userId; } set { _userId = value; } }
        public string UserName { get { return _userName; } set { _userName = value; } }
        public string Password { get { return _password; } set { _password = value; } }

        public bool Debug { get { return _debug; } set { _debug = value; } }
        public bool IsLocal { get { return _isLocal; } set { _isLocal = value; } }
        public string DebugPath { get { return _debugPath; } set { _debugPath = value; } }
        public string ClientIp { get { return _clientIp; } set { _clientIp = value; } }

        public int ProductID { get { return _productID; } set { _productID = value; } }
        public enum TripType { SingleTrip = 1, Annual, LongStay, Ski };
        public enum DestinationType { HomeCountry = 1, Europe, Worldwide, ANZO, WorldwideUSA };
        public enum GroupType { Individual = 1, Couple, Family, SingleParentFamily, GroupOfPeople };

        public string[] Products { get { return _products; } set { _products = value; } }
        public string Product { get { return _product; } set { _product = value; } }
        public TripType Trip { get { return _trip; } set { _trip = value; } }
        public DestinationType Destination { get { return _destination; } set { _destination = value; } }
        public GroupType Group { get { return _groupType; } set { _groupType = value; } }
        public DateTime StartDate { get { return _startDate; } set { _startDate = value; } }
        public DateTime EndDate { get { return _endDate; } set { _endDate = value; } }

        public string[] Endorsements { get { return _endorsements; } set { _endorsements = value; } }

        public decimal QuotePrice { get { return _quotePrice; } set { _quotePrice = value; } }

        public int TotalTravellers { get { return _totaltravellers; } set { _totaltravellers = value; } }

        public cTraveller[] Travellers { get { return _travellers; } set { _travellers = value; } }

        public bool PostDocuments { get { return _postDocuments; } set { _postDocuments = value; } }

        public string Reference { get { return _reference; } set { _reference = value; } }

        public string PromoCode { get; set; }

        public string[] LinkedPolicies { get { return _linkedPolicies; } set { _linkedPolicies = value; } }

        private int _userId;
        private string _userName;
        private string _password;
        private int _productID;
        private decimal _quotePrice;
        private TripType _trip;
        private DestinationType _destination;
        private GroupType _groupType;
        private DateTime _startDate;
        private DateTime _endDate;
        private int _totaltravellers;
        private string _product;
        private string[] _products;
        private string[] _endorsements;
        private cTraveller[] _travellers;
        private string _reference;

        private string _debugPath;
        private bool _debug;
        private bool _isLocal;
        private bool _postDocuments;
        private string _clientIp;
        private string fileName;

        private string[] _linkedPolicies;

        // const string apiUrl = "http://84.45.46.46/ws2.asmx";
        string apiUrl = ConfigurationManager.AppSettings["apiUrl"];
        //const string apiUrl = "http://www.qwerfg.com/ws2.asmx";

        public GetQuoteResponse GetQuote(Boolean storeQuote, out String quoteId, out string gqRequest, out string gqResponse)
        {
            quoteId = null;
            String xmlRequest = null;
            String xmlResponse = null;
            gqRequest = string.Empty;
            gqResponse = string.Empty;

            GetQuote gq = new GetQuote();

            // Create login Data
            cLoginData ld = new cLoginData();
            ld.AgentId = _userId;
            ld.LoginName = _userName;
            ld.Password = _password;

            if (_products == null)
                return null;

            gq.QuoteApplications = new cQuoteApplication[_products.Length];

            int application = 0;

            foreach (string product in _products)
            {

                // Create Main Data
                cMainData md = new cMainData();
                //Need Confirmation whether these are the same as while creating the Quote for the first time
                switch (_trip)
                {
                    case TripType.SingleTrip:
                        md.TripType = eTripType.SingleTrip;
                        break;
                    case TripType.Annual:
                        md.TripType = eTripType.Annual;
                        break;
                    case TripType.LongStay:
                        md.TripType = eTripType.LongStay;
                        break;
                    case TripType.Ski:
                        break;
                }

                switch (_destination)
                {
                    case DestinationType.HomeCountry:
                        md.Area = eArea.UK;
                        break;
                    case DestinationType.Europe:
                        md.Area = eArea.Europe;
                        break;
                    case DestinationType.Worldwide:
                        md.Area = eArea.Worldwide;
                        break;
                    case DestinationType.ANZO:
                        md.Area = eArea.ANZO;
                        break;
                    case DestinationType.WorldwideUSA:
                        md.Area = eArea.USA;
                        break;
                }

                switch (_groupType)
                {
                    case GroupType.Individual:
                        md.Relationship = eRelationship.Individual;
                        break;
                    case GroupType.Couple:
                        md.Relationship = eRelationship.Couple;
                        break;
                    case GroupType.Family:
                        md.Relationship = eRelationship.Family;
                        break;
                    case GroupType.SingleParentFamily:
                        md.Relationship = eRelationship.SingleParentFamily;
                        break;
                    case GroupType.GroupOfPeople:
                        md.Relationship = eRelationship.Individual;
                        break;
                }

                md.StartDate = _startDate;
                md.ReturnDate = _endDate;

                md.Travellers = _travellers;

                // Create Product data

                cProduct pd = new cProduct();
                pd.ProductId = Convert.ToInt16(product);
                md.Product = pd;


                // Create Promo Code
                cPromotion promo = new cPromotion();
                promo.PromotionCode = PromoCode;
                md.Promotion = promo;

                // Create Endorsement data

                if (_endorsements != null)
                {
                    int endorsementCount = 0;
                    md.Endorsements = new cEndorsement[_endorsements.Length];
                    foreach (string endorsement in _endorsements)
                    {
                        md.Endorsements[endorsementCount] = new cEndorsement();
                        md.Endorsements[endorsementCount].EndorsementCode = endorsement;
                        md.Endorsements[endorsementCount].Selected = true;
                        endorsementCount = endorsementCount + 1;
                    }
                }

                // Add External Refernce
                md.Reference = "autorenew";

                // Create Quote Application
                cQuoteApplication Appli = new cQuoteApplication();
                Appli.StoreQuote = storeQuote;
                Appli.MainData = md;
                gq.QuoteApplications[application] = Appli;
                application = application + 1;
            }

            gq.LoginData = ld;

            TravelApi.RockApiService.ws2 newServ = new TravelApi.RockApiService.ws2();
            newServ.Url = apiUrl;

            xmlRequest = gq.Xml;
            gqRequest = xmlRequest;
            if (_debug)
            {
                if (!Directory.Exists(_debugPath))
                    Directory.CreateDirectory(_debugPath);

                int iExecThread = System.Threading.Thread.CurrentThread.ManagedThreadId;
                bool fileExists = File.Exists(_debugPath + "\\GetQuote-Request-" + iExecThread.ToString() + ".log");

                if (!string.IsNullOrEmpty(_clientIp))
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + _clientIp + "-" + "GetQuote-Request" + ".xml";
                else
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + "GetQuote-Request" + ".xml";

                using (StreamWriter writer = new StreamWriter(_debugPath + "\\" + fileName, true))
                {
                    writer.WriteLine(xmlRequest);
                }
            }


            try
            {
                xmlResponse = newServ.Insurance("2.2.2", "GetQuote", xmlRequest);
                gqResponse = xmlResponse;
                //var te = Serialize_Object(xmlResponse);
                //var temp1 = te.Replace("&lt;", "<");
                //var temp2 = temp1.Replace("&gt;", ">");
                //var response = temp2;

            }
            catch (Exception e)
            {
                StringBuilder error = new StringBuilder();
                error.AppendLine("URL of End Point:");
                error.AppendLine(newServ.Url).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                error.AppendLine("Exception:");
                error.AppendLine(e.Message).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                if (e.InnerException != null)
                {
                    error.AppendLine("Inner Exception:");
                    error.AppendLine(e.InnerException.Message).AppendLine();
                    error.AppendLine("------------------------------------").AppendLine();
                }
                error.AppendLine("Stack Trace:");
                error.AppendLine(e.StackTrace).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                error.AppendLine("Request XML:");
                error.AppendLine(xmlRequest);
                SendEmail(error.ToString(), "Error transacting with Red2 Service. Agent Id: " + _userId, true);
            }


            if (_debug)
            {
                if (!Directory.Exists(_debugPath))
                    Directory.CreateDirectory(_debugPath);

                int iExecThread = System.Threading.Thread.CurrentThread.ManagedThreadId;
                bool fileExists = File.Exists(_debugPath + "\\GetQuote-Response-" + iExecThread.ToString() + ".log");

                if (!string.IsNullOrEmpty(_clientIp))
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + _clientIp + "-" + "GetQuote-Response" + ".xml";
                else
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + "GetQuote-Response" + ".xml";

                using (StreamWriter writer = new StreamWriter(_debugPath + "\\" + fileName, true))
                {
                    writer.WriteLine(xmlResponse);
                }
            }

            GetQuoteResponse quoteResponse = new GetQuoteResponse();
            try
            {
                quoteResponse = quoteResponse.GetResponse(xmlResponse);
                quoteId = quoteResponse.QuoteResults[0].QuoteId;
                return quoteResponse;
            }
            catch
            {
                cQuoteResult[] results = new cQuoteResult[0];
                quoteResponse.QuoteResults = results;
                return quoteResponse;
            }
        }

        public GetQuoteResponse GetQuoteMSM(Boolean storeQuote, out String quoteId, out string gqRequest, out string gqResponse, string reference)
        {
            quoteId = null;
            String xmlRequest = null;
            String xmlResponse = null;
            gqRequest = string.Empty;
            gqResponse = string.Empty;

            GetQuote gq = new GetQuote();

            // Create login Data
            cLoginData ld = new cLoginData();
            ld.AgentId = _userId;
            ld.LoginName = _userName;
            ld.Password = _password;

            if (_products == null)
                return null;

            gq.QuoteApplications = new cQuoteApplication[_products.Length];

            int application = 0;

            foreach (string product in _products)
            {

                // Create Main Data
                cMainData md = new cMainData();

                switch (_trip)
                {
                    case TripType.SingleTrip:
                        md.TripType = eTripType.SingleTrip;
                        break;
                    case TripType.Annual:
                        md.TripType = eTripType.Annual;
                        break;
                    case TripType.LongStay:
                        md.TripType = eTripType.LongStay;
                        break;
                    case TripType.Ski:
                        break;
                }

                switch (_destination)
                {
                    case DestinationType.HomeCountry:
                        md.Area = eArea.UK;
                        break;
                    case DestinationType.Europe:
                        md.Area = eArea.Europe;
                        break;
                    case DestinationType.Worldwide:
                        md.Area = eArea.Worldwide;
                        break;
                    case DestinationType.ANZO:
                        md.Area = eArea.ANZO;
                        break;
                    case DestinationType.WorldwideUSA:
                        md.Area = eArea.USA;
                        break;
                }

                switch (_groupType)
                {
                    case GroupType.Individual:
                        md.Relationship = eRelationship.Individual;
                        break;
                    case GroupType.Couple:
                        md.Relationship = eRelationship.Couple;
                        break;
                    case GroupType.Family:
                        md.Relationship = eRelationship.Family;
                        break;
                    case GroupType.SingleParentFamily:
                        md.Relationship = eRelationship.SingleParentFamily;
                        break;
                    case GroupType.GroupOfPeople:
                        md.Relationship = eRelationship.Individual;
                        break;
                }

                md.StartDate = _startDate;
                md.ReturnDate = _endDate;

                md.Travellers = _travellers;

                // Create Product data

                cProduct pd = new cProduct();
                pd.ProductId = Convert.ToInt16(product);
                md.Product = pd;


                // Create Promo Code
                cPromotion promo = new cPromotion();
                promo.PromotionCode = PromoCode;
                md.Promotion = promo;

                // Create Endorsement data

                if (_endorsements != null)
                {
                    int endorsementCount = 0;
                    md.Endorsements = new cEndorsement[_endorsements.Length];
                    foreach (string endorsement in _endorsements)
                    {
                        md.Endorsements[endorsementCount] = new cEndorsement();
                        md.Endorsements[endorsementCount].EndorsementCode = endorsement;
                        md.Endorsements[endorsementCount].Selected = true;
                        endorsementCount = endorsementCount + 1;
                    }
                }

                // Add External Refernce
                md.Reference = reference;

                // Create Quote Application
                cQuoteApplication Appli = new cQuoteApplication();
                Appli.StoreQuote = storeQuote;
                Appli.MainData = md;
                gq.QuoteApplications[application] = Appli;
                application = application + 1;
            }

            gq.LoginData = ld;

            TravelApi.RockApiService.ws2 newServ = new TravelApi.RockApiService.ws2();
            newServ.Url = apiUrl;

            xmlRequest = gq.Xml;
            gqRequest = xmlRequest;
            if (_debug)
            {
                if (!Directory.Exists(_debugPath))
                    Directory.CreateDirectory(_debugPath);

                int iExecThread = System.Threading.Thread.CurrentThread.ManagedThreadId;
                bool fileExists = File.Exists(_debugPath + "\\GetQuote-Request-" + iExecThread.ToString() + ".log");

                if (!string.IsNullOrEmpty(_clientIp))
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + _clientIp + "-" + "GetQuote-Request" + ".xml";
                else
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + "GetQuote-Request" + ".xml";

                using (StreamWriter writer = new StreamWriter(_debugPath + "\\" + fileName, true))
                {
                    writer.WriteLine(xmlRequest);
                }
            }


            try
            {
                xmlResponse = newServ.Insurance("2.2.2", "GetQuote", xmlRequest);
                gqResponse = xmlResponse;
                //var te = Serialize_Object(xmlResponse);
                //var temp1 = te.Replace("&lt;", "<");
                //var temp2 = temp1.Replace("&gt;", ">");
                //var response = temp2;

            }
            catch (Exception e)
            {
                StringBuilder error = new StringBuilder();
                error.AppendLine("URL of End Point:");
                error.AppendLine(newServ.Url).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                error.AppendLine("Exception:");
                error.AppendLine(e.Message).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                if (e.InnerException != null)
                {
                    error.AppendLine("Inner Exception:");
                    error.AppendLine(e.InnerException.Message).AppendLine();
                    error.AppendLine("------------------------------------").AppendLine();
                }
                error.AppendLine("Stack Trace:");
                error.AppendLine(e.StackTrace).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                error.AppendLine("Request XML:");
                error.AppendLine(xmlRequest);
                SendEmail(error.ToString(), "Error transacting with Red2 Service. Agent Id: " + _userId, true);
            }


            if (_debug)
            {
                if (!Directory.Exists(_debugPath))
                    Directory.CreateDirectory(_debugPath);

                int iExecThread = System.Threading.Thread.CurrentThread.ManagedThreadId;
                bool fileExists = File.Exists(_debugPath + "\\GetQuote-Response-" + iExecThread.ToString() + ".log");

                if (!string.IsNullOrEmpty(_clientIp))
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + _clientIp + "-" + "GetQuote-Response" + ".xml";
                else
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + "GetQuote-Response" + ".xml";

                using (StreamWriter writer = new StreamWriter(_debugPath + "\\" + fileName, true))
                {
                    writer.WriteLine(xmlResponse);
                }
            }

            GetQuoteResponse quoteResponse = new GetQuoteResponse();
            try
            {
                quoteResponse = quoteResponse.GetResponse(xmlResponse);
                quoteId = quoteResponse.QuoteResults[0].QuoteId;
                return quoteResponse;
            }
            catch
            {
                cQuoteResult[] results = new cQuoteResult[0];
                quoteResponse.QuoteResults = results;
                return quoteResponse;
            }
        }

        public GetQuoteResponse GetQuote(Boolean storeQuote, out String quoteId)
        {
            quoteId = null;
            String xmlRequest = null;
            String xmlResponse = null;
            //gqRequest = string.Empty;
            // gqResponse = string.Empty;

            GetQuote gq = new GetQuote();

            // Create login Data
            cLoginData ld = new cLoginData();
            ld.AgentId = _userId;
            ld.LoginName = _userName;
            ld.Password = _password;

            if (_products == null)
                return null;

            gq.QuoteApplications = new cQuoteApplication[_products.Length];

            int application = 0;

            foreach (string product in _products)
            {

                // Create Main Data
                cMainData md = new cMainData();

                switch (_trip)
                {
                    case TripType.SingleTrip:
                        md.TripType = eTripType.SingleTrip;
                        break;
                    case TripType.Annual:
                        md.TripType = eTripType.Annual;
                        break;
                    case TripType.LongStay:
                        md.TripType = eTripType.LongStay;
                        break;
                    case TripType.Ski:
                        break;
                }

                switch (_destination)
                {
                    case DestinationType.HomeCountry:
                        md.Area = eArea.UK;
                        break;
                    case DestinationType.Europe:
                        md.Area = eArea.Europe;
                        break;
                    case DestinationType.Worldwide:
                        md.Area = eArea.Worldwide;
                        break;
                    case DestinationType.ANZO:
                        md.Area = eArea.ANZO;
                        break;
                    case DestinationType.WorldwideUSA:
                        md.Area = eArea.USA;
                        break;
                }

                switch (_groupType)
                {
                    case GroupType.Individual:
                        md.Relationship = eRelationship.Individual;
                        break;
                    case GroupType.Couple:
                        md.Relationship = eRelationship.Couple;
                        break;
                    case GroupType.Family:
                        md.Relationship = eRelationship.Family;
                        break;
                    case GroupType.SingleParentFamily:
                        md.Relationship = eRelationship.SingleParentFamily;
                        break;
                    case GroupType.GroupOfPeople:
                        md.Relationship = eRelationship.Individual;
                        break;
                }

                md.StartDate = _startDate;
                md.ReturnDate = _endDate;

                md.Travellers = _travellers;

                // Create Product data

                cProduct pd = new cProduct();
                pd.ProductId = Convert.ToInt16(product);
                md.Product = pd;


                // Create Promo Code
                cPromotion promo = new cPromotion();
                promo.PromotionCode = PromoCode;
                md.Promotion = promo;

                // Create Endorsement data

                if (_endorsements != null)
                {
                    int endorsementCount = 0;
                    md.Endorsements = new cEndorsement[_endorsements.Length];
                    foreach (string endorsement in _endorsements)
                    {
                        md.Endorsements[endorsementCount] = new cEndorsement();
                        md.Endorsements[endorsementCount].EndorsementCode = endorsement;
                        md.Endorsements[endorsementCount].Selected = true;
                        endorsementCount = endorsementCount + 1;
                    }
                }

                // Add External Refernce
                md.Reference = "autorenew";

                // Create Quote Application
                cQuoteApplication Appli = new cQuoteApplication();
                Appli.StoreQuote = storeQuote;
                Appli.MainData = md;
                gq.QuoteApplications[application] = Appli;
                application = application + 1;
            }

            gq.LoginData = ld;

            TravelApi.RockApiService.ws2 newServ = new TravelApi.RockApiService.ws2();
            newServ.Url = apiUrl;

            xmlRequest = gq.Xml;
            // gqRequest = xmlRequest;
            if (_debug)
            {
                if (!Directory.Exists(_debugPath))
                    Directory.CreateDirectory(_debugPath);

                int iExecThread = System.Threading.Thread.CurrentThread.ManagedThreadId;
                bool fileExists = File.Exists(_debugPath + "\\GetQuote-Request-" + iExecThread.ToString() + ".log");

                if (!string.IsNullOrEmpty(_clientIp))
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + _clientIp + "-" + "GetQuote-Request" + ".xml";
                else
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + "GetQuote-Request" + ".xml";

                using (StreamWriter writer = new StreamWriter(_debugPath + "\\" + fileName, true))
                {
                    writer.WriteLine(xmlRequest);
                }
            }


            try
            {
                xmlResponse = newServ.Insurance("2.2.2", "GetQuote", xmlRequest);
                // gqResponse = xmlResponse;
                //var te = Serialize_Object(xmlResponse);
                //var temp1 = te.Replace("&lt;", "<");
                //var temp2 = temp1.Replace("&gt;", ">");
                //var response = temp2;

            }
            catch (Exception e)
            {
                StringBuilder error = new StringBuilder();
                error.AppendLine("URL of End Point:");
                error.AppendLine(newServ.Url).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                error.AppendLine("Exception:");
                error.AppendLine(e.Message).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                if (e.InnerException != null)
                {
                    error.AppendLine("Inner Exception:");
                    error.AppendLine(e.InnerException.Message).AppendLine();
                    error.AppendLine("------------------------------------").AppendLine();
                }
                error.AppendLine("Stack Trace:");
                error.AppendLine(e.StackTrace).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                error.AppendLine("Request XML:");
                error.AppendLine(xmlRequest);
                SendEmail(error.ToString(), "Error transacting with Red2 Service. Agent Id: " + _userId, true);
            }


            if (_debug)
            {
                if (!Directory.Exists(_debugPath))
                    Directory.CreateDirectory(_debugPath);

                int iExecThread = System.Threading.Thread.CurrentThread.ManagedThreadId;
                bool fileExists = File.Exists(_debugPath + "\\GetQuote-Response-" + iExecThread.ToString() + ".log");

                if (!string.IsNullOrEmpty(_clientIp))
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + _clientIp + "-" + "GetQuote-Response" + ".xml";
                else
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + "GetQuote-Response" + ".xml";

                using (StreamWriter writer = new StreamWriter(_debugPath + "\\" + fileName, true))
                {
                    writer.WriteLine(xmlResponse);
                }
            }

            GetQuoteResponse quoteResponse = new GetQuoteResponse();
            try
            {
                quoteResponse = quoteResponse.GetResponse(xmlResponse);
                quoteId = quoteResponse.QuoteResults[0].QuoteId;
                return quoteResponse;
            }
            catch
            {
                cQuoteResult[] results = new cQuoteResult[0];
                quoteResponse.QuoteResults = results;
                return quoteResponse;
            }
        }

        public string CreatePolicyholder(cPolicyHolderData policyHolderData, out string cHolderRequest, out string cHolderResponse)
        {
            String customerId = null;
            String xmlRequest = null;
            String xmlResponse = null;
            cHolderRequest = string.Empty;
            cHolderResponse = string.Empty;

            CreatePolicyHolder policyHolder = new CreatePolicyHolder();

            // Create login Data
            cLoginData ld = new cLoginData();
            ld.AgentId = _userId;
            ld.LoginName = _userName;
            ld.Password = _password;

            policyHolderData.WantsEmailUpdatesSpecified = true;
            policyHolderData.WantsEmailUpdates = true;

            policyHolder.LoginData = ld;
            policyHolder.PolicyHolderData = policyHolderData;

            TravelApi.RockApiService.ws2 newServ = new TravelApi.RockApiService.ws2();
            newServ.Url = apiUrl;

            xmlRequest = policyHolder.Xml;

            if (_debug)
            {
                if (!Directory.Exists(_debugPath))
                    Directory.CreateDirectory(_debugPath);

                if (!string.IsNullOrEmpty(_clientIp))
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + _clientIp + "-" + "CreatePolicyholder-Request" + ".xml";
                else
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + "CreatePolicyholder-Request" + ".xml";

                TextWriter tw = new StreamWriter(_debugPath + "\\" + fileName);
                tw.WriteLine(xmlRequest);
                tw.Close();
            }

            try
            {
                cHolderRequest = xmlRequest;
                xmlResponse = newServ.Insurance("2.2.2", "CreatePolicyHolder", xmlRequest);
                cHolderResponse = xmlResponse;
            }
            catch (Exception e)
            {
                StringBuilder error = new StringBuilder();
                error.AppendLine("URL of End Point:");
                error.AppendLine(newServ.Url).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                error.AppendLine("Exception:");
                error.AppendLine(e.Message).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                if (e.InnerException != null)
                {
                    error.AppendLine("Inner Exception:");
                    error.AppendLine(e.InnerException.Message).AppendLine();
                    error.AppendLine("------------------------------------").AppendLine();
                }
                error.AppendLine("Stack Trace:");
                error.AppendLine(e.StackTrace).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                error.AppendLine("Request XML:");
                error.AppendLine(xmlRequest);
                SendEmail(error.ToString(), "Error transacting with Red2 Service. Agent Id: " + _userId, true);
                throw e;
            }

            if (_debug)
            {
                if (!Directory.Exists(_debugPath))
                    Directory.CreateDirectory(_debugPath);

                if (!string.IsNullOrEmpty(_clientIp))
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + _clientIp + "-" + "CreatePolicyholder-Response" + ".xml";
                else
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + "CreatePolicyholder-Response" + ".xml";

                TextWriter tw = new StreamWriter(_debugPath + "\\" + fileName);
                tw.WriteLine(xmlResponse);
                tw.Close();
            }

            CreatePolicyHolderResponse cphResponse = new CreatePolicyHolderResponse();

            try
            {
                cphResponse = cphResponse.GetResponse(xmlResponse);
                customerId = cphResponse.PolicyHolderId;
            }
            catch (Exception e)
            {
                throw e;
            }
            return customerId;
        }

        public string CreatePolicyholder(cPolicyHolderData policyHolderData)
        {
            String customerId = null;
            String xmlRequest = null;
            String xmlResponse = null;
            //  cHolderRequest = string.Empty;
            //    cHolderResponse = string.Empty;

            CreatePolicyHolder policyHolder = new CreatePolicyHolder();

            // Create login Data
            cLoginData ld = new cLoginData();
            ld.AgentId = _userId;
            ld.LoginName = _userName;
            ld.Password = _password;

            policyHolderData.WantsEmailUpdatesSpecified = true;
            policyHolderData.WantsEmailUpdates = true;

            policyHolder.LoginData = ld;
            policyHolder.PolicyHolderData = policyHolderData;

            TravelApi.RockApiService.ws2 newServ = new TravelApi.RockApiService.ws2();
            newServ.Url = apiUrl;

            xmlRequest = policyHolder.Xml;

            if (_debug)
            {
                if (!Directory.Exists(_debugPath))
                    Directory.CreateDirectory(_debugPath);

                if (!string.IsNullOrEmpty(_clientIp))
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + _clientIp + "-" + "CreatePolicyholder-Request" + ".xml";
                else
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + "CreatePolicyholder-Request" + ".xml";

                TextWriter tw = new StreamWriter(_debugPath + "\\" + fileName);
                tw.WriteLine(xmlRequest);
                tw.Close();
            }

            try
            {
                //  cHolderRequest = xmlRequest;
                xmlResponse = newServ.Insurance("2.2.2", "CreatePolicyHolder", xmlRequest);
                //  cHolderResponse = xmlResponse;
            }
            catch (Exception e)
            {
                StringBuilder error = new StringBuilder();
                error.AppendLine("URL of End Point:");
                error.AppendLine(newServ.Url).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                error.AppendLine("Exception:");
                error.AppendLine(e.Message).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                if (e.InnerException != null)
                {
                    error.AppendLine("Inner Exception:");
                    error.AppendLine(e.InnerException.Message).AppendLine();
                    error.AppendLine("------------------------------------").AppendLine();
                }
                error.AppendLine("Stack Trace:");
                error.AppendLine(e.StackTrace).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                error.AppendLine("Request XML:");
                error.AppendLine(xmlRequest);
                SendEmail(error.ToString(), "Error transacting with Red2 Service. Agent Id: " + _userId, true);
                throw e;
            }

            if (_debug)
            {
                if (!Directory.Exists(_debugPath))
                    Directory.CreateDirectory(_debugPath);

                if (!string.IsNullOrEmpty(_clientIp))
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + _clientIp + "-" + "CreatePolicyholder-Response" + ".xml";
                else
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + "CreatePolicyholder-Response" + ".xml";

                TextWriter tw = new StreamWriter(_debugPath + "\\" + fileName);
                tw.WriteLine(xmlResponse);
                tw.Close();
            }

            CreatePolicyHolderResponse cphResponse = new CreatePolicyHolderResponse();

            try
            {
                cphResponse = cphResponse.GetResponse(xmlResponse);
                customerId = cphResponse.PolicyHolderId;
            }
            catch (Exception e)
            {
                throw e;
            }
            return customerId;
        }

        public string BuyQuote(String quoteId, String customerId, Boolean sendEmailCertificate, Decimal grossPrice, string paymentReference, out string bQuoteRequest, out string bQuoteResponse, bool IsPaid)
        {
            String policyId = null;
            String xmlRequest = null;
            String xmlResponse = null;
            String emailBody = null;
            bQuoteRequest = string.Empty;
            bQuoteResponse = string.Empty;

            BuyQuote bQuote = new BuyQuote();
            // Create login Data
            cLoginData ld = new cLoginData();
            ld.AgentId = _userId;
            ld.LoginName = _userName;
            ld.Password = _password;

            bQuote.LoginData = ld;
            bQuote.QuoteId = quoteId;
            bQuote.PolicyHolderId = customerId;


            bQuote.AgreeTerms = true;
            bQuote.AgreePolicy = true;
            bQuote.AgreeDisclosure = true;
            bQuote.PaymentOutstanding = IsPaid;

            if (sendEmailCertificate)
                bQuote.WantsEmailCertificate = true;
            else
                bQuote.WantsEmailCertificate = false;

            bQuote.PaymentTotalPrice = grossPrice;
            bQuote.PaymentReference = paymentReference;

            if (_linkedPolicies != null)
            {
                bQuote.LinkedPolicies = new cPolicy[_linkedPolicies.Length];
                for (int i = 0; i < _linkedPolicies.Length; i++)
                {
                    cPolicy linkedPolicy = new cPolicy();
                    linkedPolicy.PolicyNumber = _linkedPolicies[i];
                    bQuote.LinkedPolicies[i] = linkedPolicy;
                }
            }

            TravelApi.RockApiService.ws2 newServ = new TravelApi.RockApiService.ws2();
            newServ.Url = apiUrl;

            xmlRequest = bQuote.Xml;
            bQuoteRequest = xmlRequest;

            if (_debug)
            {
                if (!Directory.Exists(_debugPath))
                    Directory.CreateDirectory(_debugPath);

                if (!string.IsNullOrEmpty(_clientIp))
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + _clientIp + "-" + "BuyQuote-Request" + ".xml";
                else
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + "BuyQuote-Request" + ".xml";

                TextWriter tw = new StreamWriter(_debugPath + "\\" + fileName);
                tw.WriteLine(xmlRequest);
                tw.Close();
            }

            try
            {
                xmlResponse = newServ.Insurance("2.2.2", "BuyQuote", xmlRequest);
                bQuoteResponse = xmlResponse;
            }
            catch (Exception e)
            {
                StringBuilder error = new StringBuilder();
                error.AppendLine("URL of End Point:");
                error.AppendLine(newServ.Url).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                error.AppendLine("Exception:");
                error.AppendLine(e.Message).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                if (e.InnerException != null)
                {
                    error.AppendLine("Inner Exception:");
                    error.AppendLine(e.InnerException.Message).AppendLine();
                    error.AppendLine("------------------------------------").AppendLine();
                }
                error.AppendLine("Stack Trace:");
                error.AppendLine(e.StackTrace).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                error.AppendLine("Request XML:");
                error.AppendLine(xmlRequest);
                SendEmail(error.ToString(), "Error transacting with Red2 Service. Agent Id: " + _userId, true);
                throw e;
            }

            if (_debug)
            {
                if (!Directory.Exists(_debugPath))
                    Directory.CreateDirectory(_debugPath);

                if (!string.IsNullOrEmpty(_clientIp))
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + _clientIp + "-" + "BuyQuote-Response" + ".xml";
                else
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + "BuyQuote-Response" + ".xml";

                TextWriter tw = new StreamWriter(_debugPath + "\\" + fileName);
                tw.WriteLine(xmlResponse);
                tw.Close();
            }

            BuyQuoteResponse bqResponse = new BuyQuoteResponse();
            bqResponse = bqResponse.GetResponse(xmlResponse);
            policyId = bqResponse.PolicyNumber;

            if (bqResponse.OperationResult.OverallResultStatus == eResultStatus.Error)
            {
                emailBody = "Service Response: " + Environment.NewLine;
                emailBody += xmlResponse + Environment.NewLine + Environment.NewLine + Environment.NewLine;
                emailBody += "Service Request: " + Environment.NewLine;
                emailBody += xmlRequest + Environment.NewLine;
                SendEmail(emailBody, "Buy Quote Error - Quote Id: " + quoteId + ". Agent Id: " + _userId, true);
                System.Threading.Thread.Sleep(5000);
            }

            return policyId;
        }

        public string BuyQuote(String quoteId, String customerId, Boolean sendEmailCertificate, Decimal grossPrice, string paymentReference)
        {
            String policyId = null;
            String xmlRequest = null;
            String xmlResponse = null;
            String emailBody = null;
            //bQuoteRequest = string.Empty;
            //  bQuoteResponse = string.Empty;

            BuyQuote bQuote = new BuyQuote();
            // Create login Data
            cLoginData ld = new cLoginData();
            ld.AgentId = _userId;
            ld.LoginName = _userName;
            ld.Password = _password;

            bQuote.LoginData = ld;
            bQuote.QuoteId = quoteId;
            bQuote.PolicyHolderId = customerId;


            bQuote.AgreeTerms = true;
            bQuote.AgreePolicy = true;
            bQuote.AgreeDisclosure = true;
            // bQuote.PaymentOutstanding = IsPaid;

            if (sendEmailCertificate)
                bQuote.WantsEmailCertificate = true;
            else
                bQuote.WantsEmailCertificate = false;

            bQuote.PaymentTotalPrice = grossPrice;
            bQuote.PaymentReference = paymentReference;

            if (_linkedPolicies != null)
            {
                bQuote.LinkedPolicies = new cPolicy[_linkedPolicies.Length];
                for (int i = 0; i < _linkedPolicies.Length; i++)
                {
                    cPolicy linkedPolicy = new cPolicy();
                    linkedPolicy.PolicyNumber = _linkedPolicies[i];
                    bQuote.LinkedPolicies[i] = linkedPolicy;
                }
            }

            TravelApi.RockApiService.ws2 newServ = new TravelApi.RockApiService.ws2();
            newServ.Url = apiUrl;

            xmlRequest = bQuote.Xml;
            // bQuoteRequest = xmlRequest;

            if (_debug)
            {
                if (!Directory.Exists(_debugPath))
                    Directory.CreateDirectory(_debugPath);

                if (!string.IsNullOrEmpty(_clientIp))
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + _clientIp + "-" + "BuyQuote-Request" + ".xml";
                else
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + "BuyQuote-Request" + ".xml";

                TextWriter tw = new StreamWriter(_debugPath + "\\" + fileName);
                tw.WriteLine(xmlRequest);
                tw.Close();
            }

            try
            {
                xmlResponse = newServ.Insurance("2.2.2", "BuyQuote", xmlRequest);
                //  bQuoteResponse = xmlResponse;
            }
            catch (Exception e)
            {
                StringBuilder error = new StringBuilder();
                error.AppendLine("URL of End Point:");
                error.AppendLine(newServ.Url).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                error.AppendLine("Exception:");
                error.AppendLine(e.Message).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                if (e.InnerException != null)
                {
                    error.AppendLine("Inner Exception:");
                    error.AppendLine(e.InnerException.Message).AppendLine();
                    error.AppendLine("------------------------------------").AppendLine();
                }
                error.AppendLine("Stack Trace:");
                error.AppendLine(e.StackTrace).AppendLine();
                error.AppendLine("------------------------------------").AppendLine();
                error.AppendLine("Request XML:");
                error.AppendLine(xmlRequest);
                SendEmail(error.ToString(), "Error transacting with Red2 Service. Agent Id: " + _userId, true);
                throw e;
            }

            if (_debug)
            {
                if (!Directory.Exists(_debugPath))
                    Directory.CreateDirectory(_debugPath);

                if (!string.IsNullOrEmpty(_clientIp))
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + _clientIp + "-" + "BuyQuote-Response" + ".xml";
                else
                    fileName = String.Format("{0:yyyyMMddHHmmssff}", DateTime.Now) + "-" + Guid.NewGuid() + "-" + "BuyQuote-Response" + ".xml";

                TextWriter tw = new StreamWriter(_debugPath + "\\" + fileName);
                tw.WriteLine(xmlResponse);
                tw.Close();
            }

            BuyQuoteResponse bqResponse = new BuyQuoteResponse();
            bqResponse = bqResponse.GetResponse(xmlResponse);
            policyId = bqResponse.PolicyNumber;

            if (bqResponse.OperationResult.OverallResultStatus == eResultStatus.Error)
            {
                emailBody = "Service Response: " + Environment.NewLine;
                emailBody += xmlResponse + Environment.NewLine + Environment.NewLine + Environment.NewLine;
                emailBody += "Service Request: " + Environment.NewLine;
                emailBody += xmlRequest + Environment.NewLine;
                SendEmail(emailBody, "Buy Quote Error - Quote Id: " + quoteId + ". Agent Id: " + _userId, true);
                System.Threading.Thread.Sleep(5000);
            }

            return policyId;
        }

        private void SendEmail(string body, string subject, bool sendMike)
        {
            using (var mailMessage = new MailMessage())
            {
                mailMessage.To.Add(new System.Net.Mail.MailAddress("saratchandra.pappu@rockinsurance.com"));
                if (sendMike == true)
                {
                    mailMessage.CC.Add(new System.Net.Mail.MailAddress("saratchandra.pappu@rockinsurance.com"));
                }
                mailMessage.From = new System.Net.Mail.MailAddress("webserver@mrlgroup.com");
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = false;
                using (var smtpClient = new SmtpClient())
                {
                    if (_isLocal == true)
                        smtpClient.Host = "89.234.12.108";
                    else
                        smtpClient.Host = "192.168.100.20";

                    smtpClient.Port = 25;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new System.Net.NetworkCredential("server@Rock", "Andr0meda");
                    try
                    {
                        smtpClient.Send(mailMessage);
                    }
                    catch (Exception e)
                    {
                    }
                }
            }
        }

        public string BuyPolicy(ref String xmlRequest, ref String xmlResponse, cPolicyHolderData policyHolderData,string Reference)
        {

            String policyID = "";

            BuyPolicy buyP = new BuyPolicy();

            // Create login Data
            cLoginData ld = new cLoginData();
            ld.AgentId = _userId;
            ld.LoginName = _userName;
            ld.Password = _password;

            buyP.QuoteApplication = new cQuoteApplication();

            int application = 0;

            // Create Main Data
            cMainData md = new cMainData();

            switch (_trip)
            {
                case TripType.SingleTrip:
                    md.TripType = eTripType.SingleTrip;
                    break;
                case TripType.Annual:
                    md.TripType = eTripType.Annual;
                    break;
                case TripType.LongStay:
                    md.TripType = eTripType.LongStay;
                    break;
                case TripType.Ski:
                    break;
            }

            switch (_destination)
            {
                case DestinationType.HomeCountry:
                    md.Area = eArea.UK;
                    break;
                case DestinationType.Europe:
                    md.Area = eArea.Europe;
                    break;
                case DestinationType.Worldwide:
                    md.Area = eArea.Worldwide;
                    break;
                case DestinationType.ANZO:
                    md.Area = eArea.ANZO;
                    break;
                case DestinationType.WorldwideUSA:
                    md.Area = eArea.USA;
                    break;
            }

            switch (_groupType)
            {
                case GroupType.Individual:
                    md.Relationship = eRelationship.Individual;
                    break;
                case GroupType.Couple:
                    md.Relationship = eRelationship.Couple;
                    break;
                case GroupType.Family:
                    md.Relationship = eRelationship.Family;
                    break;
                case GroupType.SingleParentFamily:
                    md.Relationship = eRelationship.SingleParentFamily;
                    break;
                case GroupType.GroupOfPeople:
                    md.Relationship = eRelationship.Individual;
                    break;
            }

            md.StartDate = _startDate;
            md.ReturnDate = _endDate;
            md.Travellers = _travellers;
           // md.Reference = Reference;
            

            // Create Product data
            cProduct pd = new cProduct();
            pd.ProductId = _productID;
            md.Product = pd;

            // Create Endorsement data
            if (_endorsements != null)
            {
                int endorsementCount = 0;
                md.Endorsements = new cEndorsement[_endorsements.Length];
                foreach (string endorsement in _endorsements)
                {
                    md.Endorsements[endorsementCount] = new cEndorsement();
                    md.Endorsements[endorsementCount].EndorsementCode = endorsement;
                    md.Endorsements[endorsementCount].Selected = true;
                    endorsementCount = endorsementCount + 1;
                }
            }

            // Create Quote Application
            cQuoteApplication Appli = new cQuoteApplication();
            Appli.MainData = md;

            buyP.QuoteApplication = Appli;
            application = application + 1;

            // Create Customer data
            cPolicyHolderData policyHolder = policyHolderData;
            buyP.PolicyHolderData = policyHolder;
            buyP.WantsEmailCertificate = _postDocuments;
            buyP.WantsEmailCertificateSpecified = true;

            buyP.AgreeTerms = true;
            buyP.AgreeTermsSpecified = true;
            buyP.AgreeDisclosure = true;
            buyP.AgreeDisclosureSpecified = true;
            buyP.AgreePolicy = true;
            buyP.AgreePolicySpecified = true;
            buyP.PaymentTotalPriceSpecified = true;
            buyP.PaymentTotalPrice = _quotePrice;
            buyP.Surcharges = new cSurcharge[] { };
            buyP.LoginData = ld;
            buyP.Reference = Reference;
            
            

            // TravelApi.RockApiStage.ws2 newServ = new TravelApi.RockApiStage.ws2();
            TravelApi.RockApiService.ws2 newServ = new TravelApi.RockApiService.ws2();
            //newServ.Url = ConfigurationManager.AppSettings["RockService"].ToString();
            newServ.Url = "http://iat.api.rock.perfectchannel.com/ws2.asmx";
            // TravelApi.NewRockApiService.ws2 newServ = new TravelApi.NewRockApiService.ws2();

            xmlRequest = buyP.Xml;
            xmlResponse = newServ.Insurance("2.2.2", "BuyPolicy", xmlRequest);

            BuyPolicyResponse buyResponse = new BuyPolicyResponse();
            buyResponse = buyResponse.GetResponse(xmlResponse);

            policyID = buyResponse.PolicyNumber;

            return policyID;
        }

        public string Serialize_Object(object oContentObject)
        {
            if (oContentObject == null)
                return "<NULL />";
            string sOutputXml;
            Encoding utf8 = new UTF8Encoding();
            XmlTypeAttribute xmlatt = (XmlTypeAttribute)Attribute.GetCustomAttribute(oContentObject.GetType(), typeof(XmlTypeAttribute));

            using (MemoryStream mStream = new MemoryStream())
            {
                XmlTextWriter xmlTextWriter = new XmlTextWriter(mStream, utf8);
                xmlTextWriter.Formatting = Formatting.Indented;
                XmlSerializer xmlSerializer = xmlatt == null ? new XmlSerializer(oContentObject.GetType()) : new XmlSerializer(oContentObject.GetType(), xmlatt.Namespace);
                xmlSerializer.Serialize(xmlTextWriter, oContentObject);
                sOutputXml = utf8.GetString(mStream.GetBuffer(), 0, (int)mStream.Length);
                xmlTextWriter.Close();
                mStream.Close();
            }

            return sOutputXml;
        }

        #region Legacy Methods

        // Methods for the Rock Legacy System

        /* public string GetLegacyQuote()
        {

            TravelApi.RockLegacy.ws1 webServ = new TravelApi.RockLegacy.ws1();
            TravelApi.RockLegacy.CLoginData loginData = new TravelApi.RockLegacy.CLoginData();
            TravelApi.RockLegacy.CQuoteInputData inputData = new TravelApi.RockLegacy.CQuoteInputData();
            TravelApi.RockLegacy.QuoteResponse rockResponse = new TravelApi.RockLegacy.QuoteResponse();

            // Login Data
            loginData.LoginName = _userName;
            loginData.Password = _password;

            // Quote Data
            inputData.StartDate = _startDate;
            inputData.ReturnDate = _endDate;

            inputData.NumPassengers = _totaltravellers;

            int i = 0;
            inputData.Travellers = new TravelApi.RockLegacy.TravellerData[_totaltravellers];
            foreach (Person p in PolicyMembers)
            {
                inputData.Travellers[i] = new TravelApi.RockLegacy.TravellerData();
                inputData.Travellers[i].Age = p.Age;
                i = i + 1;
            }

            switch (_product)
            {
                case "1":
                    inputData.Scheme = TravelApi.RockLegacy.eSchemeType.Standard;
                    break;
                case "2":
                    inputData.Scheme = TravelApi.RockLegacy.eSchemeType.Premier;
                    break;
                case "3":
                    inputData.Scheme = TravelApi.RockLegacy.eSchemeType.Over65;
                    break;
            }

            switch (_trip)
            {
                case TripType.SingleTrip:
                    inputData.TripType = TravelApi.RockLegacy.eTripType.SingleTrip;
                    break;
                case TripType.Annual:
                    inputData.TripType = TravelApi.RockLegacy.eTripType.Annual;
                    break;
                case TripType.LongStay:
                    inputData.TripType = TravelApi.RockLegacy.eTripType.LongStay;
                    break;
                case TripType.Ski:
                    inputData.TripType = TravelApi.RockLegacy.eTripType.Ski;
                    break;
            }

            switch (_destination)
            {
                case DestinationType.HomeCountry:
                    inputData.Destination = TravelApi.RockLegacy.eDestination.UK;
                    break;
                case DestinationType.Europe:
                    inputData.Destination = TravelApi.RockLegacy.eDestination.Europe;
                    break;
                case DestinationType.Worldwide:
                    inputData.Destination = TravelApi.RockLegacy.eDestination.Worldwide;
                    break;
                case DestinationType.ANZO:
                    inputData.Destination = TravelApi.RockLegacy.eDestination.ANZO;
                    break;
                case DestinationType.WorldwideUSA:
                    inputData.Destination = TravelApi.RockLegacy.eDestination.USA;
                    break;
            }

            switch (_groupType)
            {
                case GroupType.Individual:
                    inputData.RelationShip = TravelApi.RockLegacy.eRelationship.SinglePerson;
                    break;
                case GroupType.Couple:
                    inputData.RelationShip = TravelApi.RockLegacy.eRelationship.Couple;
                    break;
                case GroupType.Family:
                    inputData.RelationShip = TravelApi.RockLegacy.eRelationship.Family;
                    break;
                case GroupType.SingleParentFamily:
                    inputData.RelationShip = TravelApi.RockLegacy.eRelationship.SingleParent;
                    break;
                case GroupType.GroupOfPeople:
                    inputData.RelationShip = TravelApi.RockLegacy.eRelationship.GroupOfPeople;
                    break;
            }

            // Create Endorsement data
            if (_endorsements != null)
            {
                inputData.Endorsements = _endorsements;
            }

            // Get the quote price back
            rockResponse = webServ.GetInstantQuote(loginData, inputData);

            _quotePrice = rockResponse.Price;

            return rockResponse.ToString();
        }

        public bool AddLegacyPolicy(TravelInsurancePolicy policy, ref string messageOut)
        {
            // Create the elements of the call
            TravelApi.RockLegacy.ws1 webServ = new TravelApi.RockLegacy.ws1();
            TravelApi.RockLegacy.CLoginData loginData = new TravelApi.RockLegacy.CLoginData();
            TravelApi.RockLegacy.CPurchaseInputData inputData = new TravelApi.RockLegacy.CPurchaseInputData();
            TravelApi.RockLegacy.PurchaseResponse rockResponse = new TravelApi.RockLegacy.PurchaseResponse();

            // Login Data
            loginData.LoginName = _userName;
            loginData.Password = _password;

            // Quote Data
            inputData.StartDate = _startDate;
            inputData.ReturnDate = _endDate;

            switch (_product)
            {
                case "1":
                    inputData.Scheme = TravelApi.RockLegacy.eSchemeType.Standard;
                    break;
                case "2":
                    inputData.Scheme = TravelApi.RockLegacy.eSchemeType.Premier;
                    break;
                case "3":
                    inputData.Scheme = TravelApi.RockLegacy.eSchemeType.Over65;
                    break;
            }

            switch (_trip)
            {
                case TripType.SingleTrip:
                    inputData.TripType = TravelApi.RockLegacy.eTripType.SingleTrip;
                    break;
                case TripType.Annual:
                    inputData.TripType = TravelApi.RockLegacy.eTripType.Annual;
                    break;
                case TripType.LongStay:
                    inputData.TripType = TravelApi.RockLegacy.eTripType.LongStay;
                    break;
                case TripType.Ski:
                    inputData.TripType = TravelApi.RockLegacy.eTripType.Ski;
                    break;
            }

            switch (_destination)
            {
                case DestinationType.HomeCountry:
                    inputData.Destination = TravelApi.RockLegacy.eDestination.UK;
                    break;
                case DestinationType.Europe:
                    inputData.Destination = TravelApi.RockLegacy.eDestination.Europe;
                    break;
                case DestinationType.Worldwide:
                    inputData.Destination = TravelApi.RockLegacy.eDestination.Worldwide;
                    break;
                case DestinationType.ANZO:
                    inputData.Destination = TravelApi.RockLegacy.eDestination.ANZO;
                    break;
                case DestinationType.WorldwideUSA:
                    inputData.Destination = TravelApi.RockLegacy.eDestination.USA;
                    break;
            }

            switch (_groupType)
            {
                case GroupType.Individual:
                    inputData.RelationShip = TravelApi.RockLegacy.eRelationship.SinglePerson;
                    break;
                case GroupType.Couple:
                    inputData.RelationShip = TravelApi.RockLegacy.eRelationship.Couple;
                    break;
                case GroupType.Family:
                    inputData.RelationShip = TravelApi.RockLegacy.eRelationship.Family;
                    break;
                case GroupType.SingleParentFamily:
                    inputData.RelationShip = TravelApi.RockLegacy.eRelationship.SingleParent;
                    break;
                case GroupType.GroupOfPeople:
                    inputData.RelationShip = TravelApi.RockLegacy.eRelationship.GroupOfPeople;
                    break;
            }

            // Create Endorsement data
            if (_endorsements != null)
            {
                inputData.Endorsements = _endorsements;
            }
            else
            {
                string[] endorsements = new string[1];
                endorsements[0] = null;
                inputData.Endorsements = endorsements;
            }

            // Create Traveller data
            inputData.NumPassengers = _totaltravellers;
            int i = 0;
            inputData.Travellers = new TravelApi.RockLegacy.TravellerData[_totaltravellers];
            foreach (Person p in PolicyMembers)
            {
                inputData.Travellers[i] = new TravelApi.RockLegacy.TravellerData();
                inputData.Travellers[i].Age = p.Age;
                inputData.Travellers[i].Title = policy.PolicyMembers[i].Title;
                inputData.Travellers[i].FirstName = policy.PolicyMembers[i].FirstName;
                inputData.Travellers[i].LastName = policy.PolicyMembers[i].Lastname;
                inputData.Travellers[i].Age = Mrl.Utilities.Tools.CreateAgeIntegerFromDOB(policy.PolicyMembers[i].DateofBirth, policy.EntryDate);
                i = i + 1;
            }
            inputData.Address1 = "";
            inputData.Address2 = "";
            inputData.Address3 = "";
            inputData.Address4 = "";
            inputData.Address1 = policy.PolicyMembers[0].MailAddress.Address1;
            if ((policy.PolicyMembers[0].MailAddress.Address2 != ""))
            {
                inputData.Address2 = policy.PolicyMembers[0].MailAddress.Address2;
                inputData.Address3 = policy.PolicyMembers[0].MailAddress.City;
                if ((policy.PolicyMembers[0].MailAddress.Region != ""))
                {
                    inputData.Address4 = policy.PolicyMembers[0].MailAddress.Region;
                }
            }
            else
            {
                inputData.Address2 = policy.PolicyMembers[0].MailAddress.City;
                if ((policy.PolicyMembers[0].MailAddress.Region != ""))
                {
                    inputData.Address3 = policy.PolicyMembers[0].MailAddress.Region;
                }
            }
            inputData.Postcode = policy.PolicyMembers[0].MailAddress.PostalCode;

            //  Contact details of main insured
            inputData.Email = policy.PolicyMembers[0].EmailAddress;
            inputData.PhoneDay = Mrl.Utilities.Tools.TrimToDigits(policy.PolicyMembers[0].Telephone);
            inputData.PhoneEvening = Mrl.Utilities.Tools.TrimToDigits(policy.PolicyMembers[0].Telephone);
            inputData.AgreeTerms = true;
            inputData.AgreeTerm2 = true;
            inputData.WantsEmailCertificate = true;
            inputData.WantsPostDocuments = policy.PostDocuments;
            inputData.WantsEmailUpdates = policy.PolicyMembers[0].MailMarketing;

            //  Policy price
            inputData.CalculatedPrice = Math.Round(policy.GrossPrice, 2);

            rockResponse = webServ.BuyQuote(loginData, inputData);
            if (rockResponse.OperationResult.CompletedOk)
            {
                messageOut = rockResponse.PolicyNumber;
                return true;
            }
            else
            {
                messageOut = rockResponse.OperationResult.ErrorNum.ToString() + " " + rockResponse.OperationResult.ErrMsg;
                return false;
            }
        }
         
         */

        #endregion
    }
}