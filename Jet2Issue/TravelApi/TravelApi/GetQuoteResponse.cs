﻿using System.Xml.Serialization;
using System.IO;
using System.Text;

namespace TravelApi
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.insurefor.com/ws2/0")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.insurefor.com/ws2/0", IsNullable = false)]
    public class GetQuoteResponse
    {

        private cOperationResult operationResultField;

        private cQuoteResult[] quoteResultsField;

        /// <remarks/>
        public cOperationResult OperationResult
        {
            get
            {
                return this.operationResultField;
            }
            set
            {
                this.operationResultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("QuoteResult", IsNullable = false)]
        public cQuoteResult[] QuoteResults
        {
            get
            {
                return this.quoteResultsField;
            }
            set
            {
                this.quoteResultsField = value;
            }
        }

        public GetQuoteResponse GetResponse(string xmlResponse)
        {
           XmlSerializer serializer = new XmlSerializer(typeof(GetQuoteResponse));
           StringReader responseReader = new StringReader(xmlResponse);
           GetQuoteResponse quoteResponse = new GetQuoteResponse();
           return (GetQuoteResponse)serializer.Deserialize(responseReader);
        }
    }

    [System.Serializable()]
    public partial class cOperationResult
    {

        private eResultStatus overallResultStatusField;

        private bool overallResultStatusFieldSpecified;

        private cResult[] resultsField;

        /// <remarks/>
        public eResultStatus OverallResultStatus
        {
            get
            {
                return this.overallResultStatusField;
            }
            set
            {
                this.overallResultStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OverallResultStatusSpecified
        {
            get
            {
                return this.overallResultStatusFieldSpecified;
            }
            set
            {
                this.overallResultStatusFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Result", IsNullable = false)]
        public cResult[] Results
        {
            get
            {
                return this.resultsField;
            }
            set
            {
                this.resultsField = value;
            }
        }
    }

    [System.Serializable()]
    public enum eResultStatus
    {

        /// <remarks/>
        Information,

        /// <remarks/>
        Warning,

        /// <remarks/>
        Error,
    }

    [System.Serializable()]
    public partial class cResult
    {

        private eResultStatus resultStatusField;

        private bool resultStatusFieldSpecified;

        private int resultCodeField;

        private bool resultCodeFieldSpecified;

        private string resultMessageField;

        /// <remarks/>
        public eResultStatus ResultStatus
        {
            get
            {
                return this.resultStatusField;
            }
            set
            {
                this.resultStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ResultStatusSpecified
        {
            get
            {
                return this.resultStatusFieldSpecified;
            }
            set
            {
                this.resultStatusFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int ResultCode
        {
            get
            {
                return this.resultCodeField;
            }
            set
            {
                this.resultCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ResultCodeSpecified
        {
            get
            {
                return this.resultCodeFieldSpecified;
            }
            set
            {
                this.resultCodeFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string ResultMessage
        {
            get
            {
                return this.resultMessageField;
            }
            set
            {
                this.resultMessageField = value;
            }
        }
    }

    [System.Serializable()]
    public partial class cSurcharge
    {

        private string surchargeCodeField;

        private bool selectedField;

        private bool selectedFieldSpecified;

        private string nameField;

        private decimal priceField;

        private bool priceFieldSpecified;

        /// <remarks/>
        public string SurchargeCode
        {
            get
            {
                return this.surchargeCodeField;
            }
            set
            {
                this.surchargeCodeField = value;
            }
        }

        /// <remarks/>
        public bool Selected
        {
            get
            {
                return this.selectedField;
            }
            set
            {
                this.selectedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SelectedSpecified
        {
            get
            {
                return this.selectedFieldSpecified;
            }
            set
            {
                this.selectedFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public decimal Price
        {
            get
            {
                return this.priceField;
            }
            set
            {
                this.priceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PriceSpecified
        {
            get
            {
                return this.priceFieldSpecified;
            }
            set
            {
                this.priceFieldSpecified = value;
            }
        }
    }

    [System.Serializable()]
    public partial class cPrice
    {
        private decimal netToUnderwriterField;

        private bool netToUnderwriterFieldSpecified;

        private decimal netToParentAgentField;

        private bool netToParentAgentFieldSpecified;

        private decimal netPriceField;

        private bool netPriceFieldSpecified;

        private decimal taxField;

        private bool taxFieldSpecified;

        private cSurcharge[] surchargesField;

        private decimal grossPriceWithAllSurchargesAndMedicalScreeningsField;

        private bool grossPriceWithAllSurchargesAndMedicalScreeningsFieldSpecified;

        private decimal grossPriceWithoutSurchargesField;

        private bool grossPriceWithoutSurchargesFieldSpecified;

        private decimal grossPriceWithAllSurchargesField;

        private bool grossPriceWithAllSurchargesFieldSpecified;

        /// <remarks/>
        public decimal NetToUnderwriter
        {
            get
            {
                return this.netToUnderwriterField;
            }
            set
            {
                this.netToUnderwriterField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NetToUnderwriterSpecified
        {
            get
            {
                return this.netToUnderwriterFieldSpecified;
            }
            set
            {
                this.netToUnderwriterFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal NetToParentAgent
        {
            get
            {
                return this.netToParentAgentField;
            }
            set
            {
                this.netToParentAgentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NetToParentAgentSpecified
        {
            get
            {
                return this.netToParentAgentFieldSpecified;
            }
            set
            {
                this.netToParentAgentFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal NetPrice
        {
            get
            {
                return this.netPriceField;
            }
            set
            {
                this.netPriceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NetPriceSpecified
        {
            get
            {
                return this.netPriceFieldSpecified;
            }
            set
            {
                this.netPriceFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal Tax
        {
            get
            {
                return this.taxField;
            }
            set
            {
                this.taxField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TaxSpecified
        {
            get
            {
                return this.taxFieldSpecified;
            }
            set
            {
                this.taxFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Surcharge", IsNullable = false)]
        public cSurcharge[] Surcharges
        {
            get
            {
                return this.surchargesField;
            }
            set
            {
                this.surchargesField = value;
            }
        }

        /// <remarks/>
        public decimal GrossPriceWithAllSurchargesAndMedicalScreenings
        {
            get
            {
                return this.grossPriceWithAllSurchargesAndMedicalScreeningsField;
            }
            set
            {
                this.grossPriceWithAllSurchargesAndMedicalScreeningsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool GrossPriceWithAllSurchargesAndMedicalScreeningsSpecified
        {
            get
            {
                return this.grossPriceWithAllSurchargesAndMedicalScreeningsFieldSpecified;
            }
            set
            {
                this.grossPriceWithAllSurchargesAndMedicalScreeningsFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal GrossPriceWithoutSurcharges
        {
            get
            {
                return this.grossPriceWithoutSurchargesField;
            }
            set
            {
                this.grossPriceWithoutSurchargesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool GrossPriceWithoutSurchargesSpecified
        {
            get
            {
                return this.grossPriceWithoutSurchargesFieldSpecified;
            }
            set
            {
                this.grossPriceWithoutSurchargesFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal GrossPriceWithAllSurcharges
        {
            get
            {
                return this.grossPriceWithAllSurchargesField;
            }
            set
            {
                this.grossPriceWithAllSurchargesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool GrossPriceWithAllSurchargesSpecified
        {
            get
            {
                return this.grossPriceWithAllSurchargesFieldSpecified;
            }
            set
            {
                this.grossPriceWithAllSurchargesFieldSpecified = value;
            }
        }
    }

    [System.Serializable()]
    public partial class cQuoteResult
    {

        private cOperationResult operationResultField;

        private cMainData mainDataField;

        private cPrice priceField;

        private string quoteIdField;

        private string quoteGuidField;

        private System.DateTime quoteExpiryDateField;

        private bool quoteExpiryDateFieldSpecified;

        private string summaryOfCoverField;

        /// <remarks/>
        public cOperationResult OperationResult
        {
            get
            {
                return this.operationResultField;
            }
            set
            {
                this.operationResultField = value;
            }
        }

        /// <remarks/>
        public cMainData MainData
        {
            get
            {
                return this.mainDataField;
            }
            set
            {
                this.mainDataField = value;
            }
        }

        /// <remarks/>
        public cPrice Price
        {
            get
            {
                return this.priceField;
            }
            set
            {
                this.priceField = value;
            }
        }

        /// <remarks/>
        public string QuoteId
        {
            get
            {
                return this.quoteIdField;
            }
            set
            {
                this.quoteIdField = value;
            }
        }

        /// <remarks/>
        public string QuoteGuid
        {
            get
            {
                return this.quoteGuidField;
            }
            set
            {
                this.quoteGuidField = value;
            }
        }

        /// <remarks/>
        public System.DateTime QuoteExpiryDate
        {
            get
            {
                return this.quoteExpiryDateField;
            }
            set
            {
                this.quoteExpiryDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuoteExpiryDateSpecified
        {
            get
            {
                return this.quoteExpiryDateFieldSpecified;
            }
            set
            {
                this.quoteExpiryDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string SummaryOfCover
        {
            get
            {
                return this.summaryOfCoverField;
            }
            set
            {
                this.summaryOfCoverField = value;
            }
        }
    }
}