﻿using System.Configuration;
namespace TravelApi
{
    public class SendEmail
    {
        public void send(string Body, string subject)
        {
            AutoEmailer me = new AutoEmailer();
            me.From = "saratchandra.pappu@rockinsurance.com";
            me.To = ConfigurationManager.AppSettings["ErrorEmailTo"];
            me.Subject = subject;
            me.Body = Body;
            me.SendEmail();
        }
    }
}
