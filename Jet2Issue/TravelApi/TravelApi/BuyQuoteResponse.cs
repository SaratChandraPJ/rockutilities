﻿using System.Xml.Serialization;
using System.IO;
using System.Text;

namespace TravelApi
{

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.insurefor.com/ws2/0")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.insurefor.com/ws2/0", IsNullable = false)]
    public partial class BuyQuoteResponse
    {

        private cOperationResult operationResultField;

        private cPrice priceField;

        private string policyIdField;

        private string policyNumberField;

        /// <remarks/>
        public cOperationResult OperationResult
        {
            get
            {
                return this.operationResultField;
            }
            set
            {
                this.operationResultField = value;
            }
        }

        /// <remarks/>
        public cPrice Price
        {
            get
            {
                return this.priceField;
            }
            set
            {
                this.priceField = value;
            }
        }

        /// <remarks/>
        public string PolicyId
        {
            get
            {
                return this.policyIdField;
            }
            set
            {
                this.policyIdField = value;
            }
        }

        /// <remarks/>
        public string PolicyNumber
        {
            get
            {
                return this.policyNumberField;
            }
            set
            {
                this.policyNumberField = value;
            }
        }

        public BuyQuoteResponse GetResponse(string xmlResponse)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(BuyQuoteResponse));
            StringReader responseReader = new StringReader(xmlResponse);
            BuyQuoteResponse response = new BuyQuoteResponse();
            return (BuyQuoteResponse)serializer.Deserialize(responseReader);
        }
    }
}