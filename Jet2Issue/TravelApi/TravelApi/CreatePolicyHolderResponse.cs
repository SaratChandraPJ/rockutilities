﻿using System.Xml.Serialization;
using System.IO;
using System.Text;

namespace TravelApi
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.insurefor.com/ws2/0")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.insurefor.com/ws2/0", IsNullable = false)]
    public partial class CreatePolicyHolderResponse
    {

        private cOperationResult operationResultField;

        private string policyHolderIdField;

        /// <remarks/>
        public cOperationResult OperationResult
        {
            get
            {
                return this.operationResultField;
            }
            set
            {
                this.operationResultField = value;
            }
        }

        /// <remarks/>
        public string PolicyHolderId
        {
            get
            {
                return this.policyHolderIdField;
            }
            set
            {
                this.policyHolderIdField = value;
            }
        }

        public CreatePolicyHolderResponse GetResponse(string xmlResponse)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CreatePolicyHolderResponse));
            StringReader responseReader = new StringReader(xmlResponse);
            CreatePolicyHolderResponse response = new CreatePolicyHolderResponse();
            return (CreatePolicyHolderResponse)serializer.Deserialize(responseReader);
        }
    }
}