﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TravelApi;
using TravelApi.Red2Data;

namespace Jet2Issue
{
    public class Program
    {
        public static void Main(string[] args)
        {
            PolicyProvider P = new PolicyProvider();
            var mDb = new RenewalPolicyProvider();
            //var policy = P.getPolicyByPolicyNumber("JEA13963883");
            var rPolicies = mDb.getRenewalPolicies();
            string PolicyNumber = string.Empty;

            try
            {
                foreach (var c in rPolicies)
                {


                    #region Creating Quote
                    var quoteRequest = new ApiCall();
                    quoteRequest.UserID = (int)c.UserId;
                    quoteRequest.UserName = c.UserName;
                    quoteRequest.Password = c.Password;
                    quoteRequest.EndDate = (DateTime)c.PolicyEndDate;
                    quoteRequest.StartDate = (DateTime)c.PolicyStartDate;
                    quoteRequest.PromoCode = c.PromoCode;
                    quoteRequest.TotalTravellers = c.TotalTravellers;
                    quoteRequest.Travellers = c.Travellers;
                    quoteRequest.Trip = c.TripType;
                    quoteRequest.Destination = c.DestinationType;
                    quoteRequest.Endorsements = c.Endorsements;
                    quoteRequest.Group = c.Group;
                    quoteRequest.Products = c.Products;
                    if (!(buyQuote( ref quoteRequest,c.Policyid,out PolicyNumber)))
                        continue;
                    #endregion
                    Console.WriteLine(PolicyNumber);
                }
            }
            catch (Exception e)
            {

            }
            Console.ReadLine();
        }

        private static bool buyQuote(ref ApiCall quoteRequest,int policyId,out string PolicyNumber)
        {
            try
            {
                PolicyNumber = String.Empty;
                PolicyProvider P = new PolicyProvider();
                string quoteId;
                string gRequest = string.Empty, gResponse = string.Empty;
                cPolicyHolderData cp = new cPolicyHolderData();
                cp =P.getPolicyHolderByPolicyId(policyId);
                
                #region Generating Quote Id
                // TravelApi.GetQuoteResponse quoteResponse = quoteRequest.GetQuote(true, out quoteId, out gRequest, out gResponse);
                PolicyNumber = quoteRequest.BuyPolicy(ref gRequest,ref gResponse,cp,string.Empty);
                #endregion


            }
            catch (Exception ex)
            {
                PolicyNumber = string.Empty;
                return false;
            }
            
            return true;
        }
    }
}
